<?php

/**
 * Template Name: CPT
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<section id="page-blog">
  <div class="container">
    <h2 class="section-title">Custom Post Type Loop</h2>
 	<hr class="underline">
    <!-- Marketing Icons Section -->
    <div class="row">
      <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $posts_per_page = 3;
          $args = array( 
            'post_type' => 'our_team', 
            'posts_per_page' => $posts_per_page,
            'paginate' => 'yes',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'paged' => $paged,
            );

          $loop = new WP_Query( $args );
      if ( $loop->have_posts() ) :  while ( $loop->have_posts() ) :  $loop->the_post(); ?>

          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <div class="card-img">
              <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
              <?php } else { ?>
                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
              <?php } ?>
              </div>
              <h4 class="card-header">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h4>
              <div class="card-body">
                <p class="card-text">
                  <div class="entry-content">
                    <?php the_excerpt(); ?>
                  </div></p>
                </div>
                <div class="card-footer">
                  <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
                </div>
              </div>
            </div><!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
          <div class="pagination-box">
         <?php
         $big = 999999999;
         echo paginate_links( array(
          'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
          'format' => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $loop->max_num_pages,
          'prev_text' => '&laquo;',
          'next_text' => '&raquo;'
             ) );
        ?>

          </div>
        <?php endif; ?>
      </div>
      <!-- /.row -->
  </div>
</section>
<?php get_footer(); ?>