<?php

/**
 * The Blog template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<section id="page-blog">
  <div class="container">
    <h2 class="section-title">Blog</h2>
 	<hr class="underline">
    <!-- Marketing Icons Section -->
    <div class="row">


      <?php if ( have_posts() ) :  while ( have_posts() ) :  the_post(); ?>

          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <div class="card-img">
              <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
              <?php } else { ?>
                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
              <?php } ?>
              </div>
              <h4 class="card-header">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h4>
              <div class="card-body">
                <p class="card-text">
                  <div class="entry-content">
                    <?php the_excerpt(); ?>
                  </div></p>
                </div>
                <div class="card-footer">
                  <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
                </div>
              </div>
            </div><!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
          <div class="pagination-box">
          <?php 
          the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'cm' ),
            'next_text'          => __( 'Next page', 'cm' ),
          ) ); ?>
          </div>
        <?php endif; ?>
      </div>
      <!-- /.row -->
  </div>
</section>
<?php get_footer(); ?>