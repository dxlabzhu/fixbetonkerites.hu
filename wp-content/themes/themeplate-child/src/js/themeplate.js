if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  executeDOMScripts();
} else {
  document.addEventListener("DOMContentLoaded", executeDOMScripts);
}

function executeDOMScripts() {
	WebFont.load({
		google: {
			families: ['Open Sans Condensed:300,700']
		}
	});

	lazyLoadStyles();
}

function lazyLoadStyles() {
	let documentFragment = document.createDocumentFragment();

	documentFragment.appendChild(constructElement('link', {
		rel: 'stylesheet',
		href: pluginsUrl + '/gdpr-cookie-compliance/dist/styles/gdpr-main.css'
	}));

	document.body.appendChild(documentFragment);
}
