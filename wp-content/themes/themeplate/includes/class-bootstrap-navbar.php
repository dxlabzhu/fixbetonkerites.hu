<?php

/**
 * Bootstrap NavBar Walker
 *
 * @package ThemePlate
 * @since 0.1.0
 */

if ( ! class_exists( 'Bootstrap_NavBar' ) ) {
	if ( class_exists( 'ThemePlate_NavWalker' ) ) {
		class Bootstrap_NavBar extends ThemePlate_NavWalker {
			public $class = array(
				'sub-menu' => 'dropdown-menu',
				'has-sub'  => 'dropdown',
				'active'   => 'active',
				);

			public function attributes( $item, $args ) {
				$atts = array();

				if ( $args->walker->has_children ) {
					$atts['href']          = '#';
					$atts['data-toggle']   = 'dropdown';
					$atts['class']         = 'dropdown-toggle';
					$atts['aria-haspopup'] = 'true';
				}

				return $atts;
			}

			// Displays start of an element. E.g '<li> Item Name'
    // @see Walker::start_el()
			function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
				$object = $item->object;
				$type = $item->type;
				$title = $item->title;
				$description = $item->description;
				$permalink = $item->url;

				$output .= "<li class='nav-item " .  implode(" ", $item->classes) . "'>";

      			//Add SPAN if no Permalink
				if( $permalink && $permalink != '#' ) {
					$output .= '<a class="nav-link" href="' . $permalink . '">';
				} else {
					$output .= '<span>';
				}

				$output .= $title;

				if( $description != '' && $depth == 0 ) {
					$output .= '<small class="description">' . $description . '</small>';
				}

				if( $permalink && $permalink != '#' ) {
					$output .= '</a>';
				} else {
					$output .= '</span>';
				}
			}
		}
	} else {
		class Bootstrap_NavBar extends Walker_Nav_Menu {}
	}
}
