<?php
/**
 * Viewport dependant image source generation through picture tag
 *
 * @package ThemePlate
 * @since 0.1.0
 */

function picture($base_filename, $filetype, $alt, $lazy, $class) {
  $filepath = get_stylesheet_directory_uri() . '/assets/images/' . $base_filename;
  $srcset_attr = $lazy ? 'data-srcset' : 'srcset';
	//$filepath = 'assets/images/' . $base_filename;

	echo '<picture>
		<source media="(min-width: 1200px)" ' . $srcset_attr . '="' . $filepath . '-desktop.webp" type="image/webp" />
		<source media="(min-width: 768px)" ' . $srcset_attr . '="' . $filepath . '-tablet.webp" type="image/webp" />
		<source media="(max-width: 767px)" ' . $srcset_attr . '="' . $filepath . '-2x.webp 2x, ' . $filepath . '.webp" type="image/webp" />
		<source media="(min-width: 1200px)" ' . $srcset_attr . '="' . $filepath . '-desktop.' . $filetype . '" />
		<source media="(min-width: 768px)" ' . $srcset_attr . '="' . $filepath . '-tablet.' . $filetype . '" />
		<source media="(max-width: 767px)" ' . $srcset_attr . '="' . $filepath . '-2x.' . $filetype . ' 2x, ' . $filepath . '.' . $filetype . '" />
		<img ' . ($lazy ? 'data-src' : 'src') . '="' . $filepath . '.' . $filetype . '" alt="'. $alt . '" class="' . ($lazy ? 'lazyload' : '') . ($class ? ' ' . $class : '') . '" />
	</picture>';
}
?>