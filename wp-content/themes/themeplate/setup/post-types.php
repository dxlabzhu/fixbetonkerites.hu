<?php

/**
 * Register custom post types
 *
 * @package ThemePlate
 * @since 0.1.0
 */

ThemePlate()->post_type( array(
	'name'        => 'portfolio',
	'plural'      => __( 'Portfolio', 'themeplate' ),
	'singular'    => __( 'Portfolio', 'themeplate' ),
	'description' => __( 'Portfolio', 'themeplate' ),
	'args'        => array(
		'public'          => true,
		// 'exclude_from_search' => false,
		// 'publicly_queryable'  => true,
		// 'show_ui'             => true,
		// 'show_in_nav_menus'   => true,
		// 'show_in_menu'        => true,
		// 'show_in_admin_bar'   => true,
		'menu_position'   => 5,
		'menu_icon'       => 'dashicons-media-document',
		'capability_type' => 'post',
		'hierarchical'    => false,
		'supports'        => array( 'title', 'editor', 'thumbnail' ),
		// 'taxonomies'          => array( 'category', 'post_tag' ),
		'has_archive'     => true,
		'rewrite'         => array(
			'slug'       => 'portfolio',
			'with_front' => false,
		),
	),
) );

ThemePlate()->taxonomy( array(
	'name'        => 'portfolio-cat',
	'plural'      => __( 'Categories', 'themeplate' ),
	'singular'    => __( 'Category', 'themeplate' ),
	'description' => __( 'Portfolio Category', 'themeplate' ),
	'type'        => 'portfolio',
	'args'        => array(
		'public'            => true,
		// 'show_ui'            => true,
		// 'show_in_menu'       => true,
		// 'show_in_nav_menus'  => true,
		// 'show_tagcloud'      => true,
		// 'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'rewrite'           => array( 'slug' => 'portfolio-cat' ),
	),
) );

ThemePlate()->taxonomy( array(
	'name'        => 'portfolio-tag',
	'plural'      => __( 'Tags', 'themeplate' ),
	'singular'    => __( 'Tag', 'themeplate' ),
	'description' => __( 'Portfolio Tag', 'themeplate' ),
	'type'        => 'portfolio',
	'args'        => array(
		'public'            => true,
		// 'show_ui'            => true,
		// 'show_in_menu'       => true,
		// 'show_in_nav_menus'  => true,
		// 'show_tagcloud'      => true,
		// 'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical'      => false,
		'rewrite'           => array( 'slug' => 'portfolio-tag' ),
	),
) );
