<?php

/**
 * Register settings
 *
 * @package ThemePlate
 * @since 0.1.0
 */

ThemePlate()->settings( array(
	'id'          => 'google',
	'title'       => __( 'Google Codes', 'themeplate' ),
	'description' => __( 'Enter the tracking IDs to use in site.', 'themeplate' ),
	'context'     => 'side',
	'fields'      => array(
		'analytics'  => array(
			'title'       => __( 'Analytics', 'themeplate' ),
			'description' => __( 'UA-XXXXX-Y', 'themeplate' ),
			'type'        => 'text',
		),
		'tagmanager' => array(
			'title'       => __( 'Tag Manager', 'themeplate' ),
			'description' => __( 'GTM-XXXX', 'themeplate' ),
			'type'        => 'text',
		),
	),
) );

ThemePlate()->settings( array(
	'id'          => 'social',
	'title'       => __( 'Social Profiles', 'themeplate' ),
	'description' => __( 'Enter the links to the associated service.', 'themeplate' ),
	'context'     => 'normal',
	'fields'      => array(
		'profiles' => array(
			'title'      => __( 'Service', 'themeplate' ),
			'type'       => 'group',
			'repeatable' => true,
			'fields'     => array(
				'provider' => array(
					'title'   => __( 'Provider', 'themeplate' ),
					'type'    => 'select',
					'options' => array(
						'facebook'    => 'Facebook',
						'twitter'     => 'Twitter',
						'instagram'   => 'Instagram',
						'linkedin'    => 'LinkedIn',
						'youtube'     => 'Youtube',
						'pinterest'   => 'Pinterest',
						'google-plus' => 'Google+',
					),
				),
				'link'     => array(
					'title' => __( 'Link', 'themeplate' ),
					'type'  => 'url',
				),
			),
		),
	),
) );
