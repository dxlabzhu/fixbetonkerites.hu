<?php

/**
 * Register navigation menus
 *
 * @package ThemePlate
 * @since 0.1.0
 */

if ( ! function_exists( 'themeplate_navigations' ) ) {
	function themeplate_navigations() {
		// Bootstrap Nav Walker
		require_once THEME_INC . 'class-bootstrap-navbar.php';
		
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'themeplate' ),
			'footer'  => __( 'Footer Menu', 'themeplate' ),
		) );
	}
	add_action( 'after_setup_theme', 'themeplate_navigations' );
}

// Primary Menu
if ( ! function_exists( 'themeplate_primary_menu' ) ) {
	function themeplate_primary_menu() {
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'menu_class'     => 'ml-auto navbar-nav',
			'walker'         => new Bootstrap_NavBar(),
			'depth'          => 0,
		) );
	}
}

// Footer Menu
if ( ! function_exists( 'themeplate_footer_menu' ) ) {
	function themeplate_footer_menu() {
		wp_nav_menu( array(
			'theme_location' => 'footer',
			'depth'          => 1,
		) );
	}
}
