-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Okt 22. 13:04
-- Kiszolgáló verziója: 10.4.8-MariaDB
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `fixbeton`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-10-31 08:28:29', '2017-10-31 08:28:29', 'Üdvözlet! Ez egy hozzászólás.\nA hozzászólások moderálásához, szerkesztéséhez és törléséhez látogassunk el a honlapunk vezérlőpultjának\nHozzászólások menüpontjához.\nA hozzászólok avatarját a <a href=\"https://gravatar.com\">Gravatar</a> biztosítja.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_db7_forms`
--

CREATE TABLE `wp_db7_forms` (
  `form_id` bigint(20) NOT NULL,
  `form_post_id` bigint(20) NOT NULL,
  `form_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_email_log`
--

CREATE TABLE `wp_email_log` (
  `id` mediumint(9) NOT NULL,
  `to_email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `attachment_name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result` tinyint(1) DEFAULT NULL,
  `error_message` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter`
--

CREATE TABLE `wp_newsletter` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'S',
  `id` int(11) NOT NULL,
  `profile` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` int(11) NOT NULL DEFAULT 0,
  `last_activity` int(11) NOT NULL DEFAULT 0,
  `followup_step` tinyint(4) NOT NULL DEFAULT 0,
  `followup_time` bigint(20) NOT NULL DEFAULT 0,
  `followup` tinyint(4) NOT NULL DEFAULT 0,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n',
  `feed_time` bigint(20) NOT NULL DEFAULT 0,
  `feed` tinyint(4) NOT NULL DEFAULT 0,
  `referrer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `wp_user_id` int(11) NOT NULL DEFAULT 0,
  `http_referer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `geo` tinyint(4) NOT NULL DEFAULT 0,
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `region` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_time` int(11) NOT NULL DEFAULT 0,
  `unsub_email_id` int(11) NOT NULL DEFAULT 0,
  `unsub_time` int(11) NOT NULL DEFAULT 0,
  `list_1` tinyint(4) NOT NULL DEFAULT 0,
  `list_2` tinyint(4) NOT NULL DEFAULT 0,
  `list_3` tinyint(4) NOT NULL DEFAULT 0,
  `list_4` tinyint(4) NOT NULL DEFAULT 0,
  `list_5` tinyint(4) NOT NULL DEFAULT 0,
  `list_6` tinyint(4) NOT NULL DEFAULT 0,
  `list_7` tinyint(4) NOT NULL DEFAULT 0,
  `list_8` tinyint(4) NOT NULL DEFAULT 0,
  `list_9` tinyint(4) NOT NULL DEFAULT 0,
  `list_10` tinyint(4) NOT NULL DEFAULT 0,
  `list_11` tinyint(4) NOT NULL DEFAULT 0,
  `list_12` tinyint(4) NOT NULL DEFAULT 0,
  `list_13` tinyint(4) NOT NULL DEFAULT 0,
  `list_14` tinyint(4) NOT NULL DEFAULT 0,
  `list_15` tinyint(4) NOT NULL DEFAULT 0,
  `list_16` tinyint(4) NOT NULL DEFAULT 0,
  `list_17` tinyint(4) NOT NULL DEFAULT 0,
  `list_18` tinyint(4) NOT NULL DEFAULT 0,
  `list_19` tinyint(4) NOT NULL DEFAULT 0,
  `list_20` tinyint(4) NOT NULL DEFAULT 0,
  `list_21` tinyint(4) NOT NULL DEFAULT 0,
  `list_22` tinyint(4) NOT NULL DEFAULT 0,
  `list_23` tinyint(4) NOT NULL DEFAULT 0,
  `list_24` tinyint(4) NOT NULL DEFAULT 0,
  `list_25` tinyint(4) NOT NULL DEFAULT 0,
  `list_26` tinyint(4) NOT NULL DEFAULT 0,
  `list_27` tinyint(4) NOT NULL DEFAULT 0,
  `list_28` tinyint(4) NOT NULL DEFAULT 0,
  `list_29` tinyint(4) NOT NULL DEFAULT 0,
  `list_30` tinyint(4) NOT NULL DEFAULT 0,
  `list_31` tinyint(4) NOT NULL DEFAULT 0,
  `list_32` tinyint(4) NOT NULL DEFAULT 0,
  `list_33` tinyint(4) NOT NULL DEFAULT 0,
  `list_34` tinyint(4) NOT NULL DEFAULT 0,
  `list_35` tinyint(4) NOT NULL DEFAULT 0,
  `list_36` tinyint(4) NOT NULL DEFAULT 0,
  `list_37` tinyint(4) NOT NULL DEFAULT 0,
  `list_38` tinyint(4) NOT NULL DEFAULT 0,
  `list_39` tinyint(4) NOT NULL DEFAULT 0,
  `list_40` tinyint(4) NOT NULL DEFAULT 0,
  `profile_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_7` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_8` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_9` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_11` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_13` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_14` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_15` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_16` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_17` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_18` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_19` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_20` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `test` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_newsletter`
--

INSERT INTO `wp_newsletter` (`name`, `email`, `token`, `language`, `status`, `id`, `profile`, `created`, `updated`, `last_activity`, `followup_step`, `followup_time`, `followup`, `surname`, `sex`, `feed_time`, `feed`, `referrer`, `ip`, `wp_user_id`, `http_referer`, `geo`, `country`, `region`, `city`, `bounce_type`, `bounce_time`, `unsub_email_id`, `unsub_time`, `list_1`, `list_2`, `list_3`, `list_4`, `list_5`, `list_6`, `list_7`, `list_8`, `list_9`, `list_10`, `list_11`, `list_12`, `list_13`, `list_14`, `list_15`, `list_16`, `list_17`, `list_18`, `list_19`, `list_20`, `list_21`, `list_22`, `list_23`, `list_24`, `list_25`, `list_26`, `list_27`, `list_28`, `list_29`, `list_30`, `list_31`, `list_32`, `list_33`, `list_34`, `list_35`, `list_36`, `list_37`, `list_38`, `list_39`, `list_40`, `profile_1`, `profile_2`, `profile_3`, `profile_4`, `profile_5`, `profile_6`, `profile_7`, `profile_8`, `profile_9`, `profile_10`, `profile_11`, `profile_12`, `profile_13`, `profile_14`, `profile_15`, `profile_16`, `profile_17`, `profile_18`, `profile_19`, `profile_20`, `test`) VALUES
('', 'doxa84@hotmail.com', 'b95d6c98a7', '', 'C', 1, NULL, '2020-03-10 09:54:34', 0, 0, 0, 0, 0, '', 'n', 0, 0, '', '', 0, '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_emails`
--

CREATE TABLE `wp_newsletter_emails` (
  `id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('new','sending','sent','paused') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `total` int(11) NOT NULL DEFAULT 0,
  `last_id` int(11) NOT NULL DEFAULT 0,
  `sent` int(11) NOT NULL DEFAULT 0,
  `track` int(11) NOT NULL DEFAULT 0,
  `list` int(11) NOT NULL DEFAULT 0,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `query` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` tinyint(4) NOT NULL DEFAULT 0,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `theme` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferences` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_on` int(11) NOT NULL DEFAULT 0,
  `token` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT 0,
  `click_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `open_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `unsub_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `error_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `stats_time` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_sent`
--

CREATE TABLE `wp_newsletter_sent` (
  `email_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `open` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `error` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_stats`
--

CREATE TABLE `wp_newsletter_stats` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_user_logs`
--

CREATE TABLE `wp_newsletter_user_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `source` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/fixbetonkerites.hu', 'yes'),
(2, 'home', 'http://localhost/fixbetonkerites.hu', 'yes'),
(3, 'blogname', 'Fix Beton Kerítés', 'yes'),
(4, 'blogdescription', 'Biztonságos, Gyors, Esztétikus megoldások Olcsón, a Legmodernebb Technológiákkal', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'doxa84@hotmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '3', 'yes'),
(23, 'date_format', 'Y-m-d', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'Y-m-d H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:20:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";i:4;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:5;s:23:\"debug-bar/debug-bar.php\";i:6;s:37:\"disable-comments/disable-comments.php\";i:7;s:32:\"duplicate-page/duplicatepage.php\";i:8;s:23:\"email-log/email-log.php\";i:9;s:45:\"enable-media-replace/enable-media-replace.php\";i:10;s:29:\"health-check/health-check.php\";i:12;s:77:\"jquery-validation-for-contact-form-7/jquery-validation-for-contact-form-7.php\";i:14;s:37:\"rocket-lazy-load/rocket-lazy-load.php\";i:15;s:47:\"show-current-template/show-current-template.php\";i:16;s:35:\"simply-show-ids/simply-show-ids.php\";i:17;s:27:\"svg-support/svg-support.php\";i:18;s:27:\"theme-check/theme-check.php\";i:19;s:24:\"wordpress-seo/wp-seo.php\";i:20;s:31:\"wp-migrate-db/wp-migrate-db.php\";i:21;s:23:\"wp-smushit/wp-smush.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'themeplate', 'yes'),
(41, 'stylesheet', 'themeplate-child', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '48748', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:4:{s:39:\"svg-vector-icon-plugin/wp-svg-icons.php\";s:22:\"uninstall_wp_svg_icons\";s:27:\"wp-asset-clean-up/wpacu.php\";a:2:{i:0;s:8:\"Freemius\";i:1;s:22:\"_uninstall_plugin_hook\";}s:28:\"fast-velocity-minify/fvm.php\";s:29:\"fastvelocity_plugin_uninstall\";s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', 'Europe/Budapest', 'yes'),
(83, 'page_for_posts', '42', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '89', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:64:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:17:\"manage_email_logs\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'hu_HU', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'cron', 'a:12:{i:1603359601;a:1:{s:25:\"wpseo_ping_search_engines\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1603359605;a:2:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603370371;a:3:{s:13:\"wpseo-reindex\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:31:\"wpseo_permalink_structure_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:20:\"wpseo_home_url_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603396614;a:1:{s:17:\"wmac_cachechecker\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603398510;a:3:{s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1603440801;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603441729;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603443751;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603445676;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603799171;a:1:{s:40:\"health-check-scheduled-site-status-check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1603884585;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438587;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(144, 'current_theme', 'ThemePlate Child', 'yes'),
(145, 'theme_mods_understrap-master', 'a:5:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438639;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"statichero\";N;s:10:\"footerfull\";N;}}}', 'yes'),
(146, 'theme_switched', '', 'yes'),
(147, 'theme_mods_twentyfifteen', 'a:2:{i:0;b:0;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438657;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(148, 'theme_mods_understrap', 'a:5:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438669;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";N;s:4:\"hero\";N;s:10:\"statichero\";N;s:10:\"footerfull\";N;}}}', 'yes'),
(149, 'theme_mods_understrap-child', 'a:7:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548498991;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"statichero\";a:0:{}s:10:\"footerfull\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(181, 'recently_activated', 'a:2:{s:21:\"newsletter/plugin.php\";i:1603358057;s:22:\"honeypot/wp-armour.php\";i:1603200263;}', 'yes'),
(186, 'wpseo', 'a:36:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";s:1:\"2\";s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:1;s:29:\"indexation_warning_hide_until\";b:0;s:19:\"indexing_first_time\";b:1;s:18:\"indexation_started\";b:0;s:15:\"indexing_reason\";s:23:\"home_url_option_changed\";s:31:\"indexables_indexation_completed\";b:0;s:7:\"version\";s:6:\"15.1.1\";s:16:\"previous_version\";s:4:\"15.0\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:0;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1527930151;s:13:\"myyoast-oauth\";b:0;s:26:\"semrush_integration_active\";b:1;s:14:\"semrush_tokens\";a:0:{}s:20:\"semrush_country_code\";s:2:\"us\";s:19:\"permalink_structure\";s:12:\"/%postname%/\";s:8:\"home_url\";s:36:\"https://localhost/fixbetonkerites.hu\";s:18:\"dynamic_permalinks\";b:0;}', 'yes'),
(187, 'wpseo_titles', 'a:101:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:83:\"Keresési eredmény a következőre: %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:45:\"Az oldal nem található %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:1;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:1;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:42:\"HIBA 404: A keresett oldal nem található\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:1;s:25:\"breadcrumbs-archiveprefix\";s:0:\"\";s:18:\"breadcrumbs-enable\";b:1;s:16:\"breadcrumbs-home\";s:8:\"Főoldal\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:35:\"Keresési találat a következőre:\";s:15:\"breadcrumbs-sep\";s:2:\"»\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";s:1:\"0\";s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:18:\"title-tax-category\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:14:\"title-our_team\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-our_team\";s:0:\"\";s:16:\"noindex-our_team\";b:0;s:27:\"display-metabox-pt-our_team\";b:1;s:27:\"post_types-our_team-maintax\";s:1:\"0\";s:25:\"schema-page-type-our_team\";s:7:\"WebPage\";s:28:\"schema-article-type-our_team\";s:4:\"None\";s:24:\"title-ptarchive-our_team\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-our_team\";s:0:\"\";s:26:\"bctitle-ptarchive-our_team\";s:0:\"\";s:26:\"noindex-ptarchive-our_team\";b:0;s:17:\"title-testimonial\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:20:\"metadesc-testimonial\";s:0:\"\";s:19:\"noindex-testimonial\";b:0;s:30:\"display-metabox-pt-testimonial\";b:1;s:30:\"post_types-testimonial-maintax\";s:1:\"0\";s:28:\"schema-page-type-testimonial\";s:7:\"WebPage\";s:31:\"schema-article-type-testimonial\";s:4:\"None\";s:13:\"title-partner\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-partner\";s:0:\"\";s:15:\"noindex-partner\";b:0;s:26:\"display-metabox-pt-partner\";b:1;s:26:\"post_types-partner-maintax\";s:1:\"0\";s:24:\"schema-page-type-partner\";s:7:\"WebPage\";s:27:\"schema-article-type-partner\";s:4:\"None\";s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}', 'yes'),
(188, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(189, 'wpseo_flush_rewrite', '1', 'yes'),
(198, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:23:\"wp-smushit/wp-smush.php\";i:1527930198;}s:5:\"queue\";a:1:{s:32:\"fc50097023d0d34c5a66f6cddcf77694\";a:4:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1602070986;s:6:\"sticky\";b:1;}}s:4:\"done\";a:1:{i:0;a:6:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1527930198;s:5:\"state\";s:6:\"ignore\";s:4:\"hash\";s:32:\"fc50097023d0d34c5a66f6cddcf77694\";s:10:\"handled_at\";i:1601984597;}}}', 'no'),
(199, 'wp-smush-install-type', 'existing', 'no'),
(200, 'wp-smush-version', '3.7.1', 'no'),
(201, 'wp-smush-skip-redirect', '1', 'no'),
(203, 'smush_global_stats', 'a:9:{s:11:\"size_before\";i:433734;s:10:\"size_after\";i:388571;s:7:\"percent\";d:10.4;s:5:\"human\";s:7:\"44,1 KB\";s:5:\"bytes\";i:45163;s:12:\"total_images\";i:16;s:12:\"resize_count\";i:0;s:14:\"resize_savings\";i:0;s:18:\"conversion_savings\";i:0;}', 'no'),
(208, 'dir_smush_stats', 'a:2:{s:9:\"dir_smush\";a:2:{s:5:\"total\";i:0;s:9:\"optimised\";i:0;}s:14:\"combined_stats\";a:0:{}}', 'no'),
(209, 'skip-smush-setup', '1', 'no'),
(212, 'wpmdb_settings', 'a:13:{s:3:\"key\";s:40:\"YKzrgMpkTQ45FVfxnL2vd5Qdz6m1N59NeWokLPTA\";s:10:\"allow_pull\";b:0;s:10:\"allow_push\";b:0;s:8:\"profiles\";a:0:{}s:7:\"licence\";s:0:\"\";s:10:\"verify_ssl\";b:0;s:17:\"whitelist_plugins\";a:0:{}s:11:\"max_request\";i:1048576;s:22:\"delay_between_requests\";i:0;s:18:\"prog_tables_hidden\";b:1;s:21:\"pause_before_finalize\";b:0;s:14:\"allow_tracking\";N;s:28:\"compatibility_plugin_version\";s:3:\"1.2\";}', 'no'),
(213, 'wpmdb_schema_version', '2', 'no'),
(222, 'a8c_developer', 'a:1:{s:12:\"project_type\";s:11:\"wporg-theme\";}', 'yes'),
(257, 'wpcf7', 'a:2:{s:7:\"version\";s:3:\"5.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1527930759;s:7:\"version\";s:5:\"5.0.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(263, 'cptui_new_install', 'false', 'yes'),
(282, 'sm_options', 'a:52:{s:18:\"sm_b_prio_provider\";s:41:\"GoogleSitemapGeneratorPrioByCountProvider\";s:9:\"sm_b_ping\";b:1;s:10:\"sm_b_stats\";b:0;s:12:\"sm_b_pingmsn\";b:1;s:12:\"sm_b_autozip\";b:1;s:11:\"sm_b_memory\";s:0:\"\";s:9:\"sm_b_time\";i:-1;s:18:\"sm_b_style_default\";b:1;s:10:\"sm_b_style\";s:0:\"\";s:12:\"sm_b_baseurl\";s:0:\"\";s:11:\"sm_b_robots\";b:1;s:9:\"sm_b_html\";b:1;s:12:\"sm_b_exclude\";a:0:{}s:17:\"sm_b_exclude_cats\";a:0:{}s:10:\"sm_in_home\";b:1;s:11:\"sm_in_posts\";b:1;s:15:\"sm_in_posts_sub\";b:0;s:11:\"sm_in_pages\";b:1;s:10:\"sm_in_cats\";b:0;s:10:\"sm_in_arch\";b:0;s:10:\"sm_in_auth\";b:0;s:10:\"sm_in_tags\";b:0;s:9:\"sm_in_tax\";a:0:{}s:17:\"sm_in_customtypes\";a:0:{}s:13:\"sm_in_lastmod\";b:1;s:10:\"sm_cf_home\";s:5:\"daily\";s:11:\"sm_cf_posts\";s:7:\"monthly\";s:11:\"sm_cf_pages\";s:6:\"weekly\";s:10:\"sm_cf_cats\";s:6:\"weekly\";s:10:\"sm_cf_auth\";s:6:\"weekly\";s:15:\"sm_cf_arch_curr\";s:5:\"daily\";s:14:\"sm_cf_arch_old\";s:6:\"yearly\";s:10:\"sm_cf_tags\";s:6:\"weekly\";s:10:\"sm_pr_home\";d:1;s:11:\"sm_pr_posts\";d:0.6;s:15:\"sm_pr_posts_min\";d:0.2;s:11:\"sm_pr_pages\";d:0.6;s:10:\"sm_pr_cats\";d:0.3;s:10:\"sm_pr_arch\";d:0.3;s:10:\"sm_pr_auth\";d:0.3;s:10:\"sm_pr_tags\";d:0.3;s:12:\"sm_i_donated\";b:0;s:17:\"sm_i_hide_donated\";b:0;s:17:\"sm_i_install_date\";i:1527932310;s:16:\"sm_i_hide_survey\";b:0;s:14:\"sm_i_hide_note\";b:0;s:15:\"sm_i_hide_works\";b:0;s:16:\"sm_i_hide_donors\";b:0;s:9:\"sm_i_hash\";s:20:\"9da2b23434bcc2725652\";s:13:\"sm_i_lastping\";i:1548579132;s:16:\"sm_i_supportfeed\";b:1;s:22:\"sm_i_supportfeed_cache\";i:1583827338;}', 'yes'),
(286, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(290, 'disable_comments_options', 'a:5:{s:19:\"disabled_post_types\";a:0:{}s:17:\"remove_everywhere\";b:0;s:9:\"permanent\";b:0;s:16:\"extra_post_types\";b:0;s:10:\"db_version\";i:6;}', 'yes'),
(291, 'duplicate_page_options', 'a:3:{s:21:\"duplicate_post_status\";s:5:\"draft\";s:23:\"duplicate_post_redirect\";s:7:\"to_list\";s:21:\"duplicate_post_suffix\";s:0:\"\";}', 'yes'),
(295, '_transient_understrap_categories', '1', 'yes'),
(325, 'theme_mods_themeplate', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548620296;s:4:\"data\";a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}}}}', 'yes'),
(326, 'widget_themeplate_about', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(327, 'widget_themeplate_contact', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(403, 'wp-smush-settings', 'a:15:{s:11:\"networkwide\";b:0;s:4:\"auto\";b:1;s:5:\"lossy\";b:0;s:10:\"strip_exif\";b:1;s:6:\"resize\";b:0;s:9:\"detection\";b:0;s:8:\"original\";b:0;s:6:\"backup\";b:0;s:10:\"png_to_jpg\";b:0;s:7:\"nextgen\";b:0;s:2:\"s3\";b:0;s:9:\"gutenberg\";b:0;s:3:\"cdn\";b:0;s:11:\"auto_resize\";b:0;s:4:\"webp\";b:1;}', 'yes'),
(427, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(429, 'sm_status', 'O:28:\"GoogleSitemapGeneratorStatus\":4:{s:39:\"\0GoogleSitemapGeneratorStatus\0startTime\";d:1548579130.359318;s:37:\"\0GoogleSitemapGeneratorStatus\0endTime\";d:1548579131.346869;s:41:\"\0GoogleSitemapGeneratorStatus\0pingResults\";a:2:{s:6:\"google\";a:5:{s:9:\"startTime\";d:1548579130.368825;s:7:\"endTime\";d:1548579130.7352221;s:7:\"success\";b:0;s:3:\"url\";s:128:\"http://www.google.com/webmasters/sitemaps/ping?sitemap=https%3A%2F%2Flocalhost%2Fwordpress-bootstrap-4-boilerplate%2Fsitemap.xml\";s:4:\"name\";s:6:\"Google\";}s:4:\"bing\";a:5:{s:9:\"startTime\";d:1548579130.7504101;s:7:\"endTime\";d:1548579131.3391559;s:7:\"success\";b:1;s:3:\"url\";s:121:\"http://www.bing.com/webmaster/ping.aspx?siteMap=https%3A%2F%2Flocalhost%2Fwordpress-bootstrap-4-boilerplate%2Fsitemap.xml\";s:4:\"name\";s:4:\"Bing\";}}s:38:\"\0GoogleSitemapGeneratorStatus\0autoSave\";b:1;}', 'no'),
(439, 'category_children', 'a:0:{}', 'yes'),
(460, 'theme_mods_themeplate-child', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(787, 'factory_plugin_versions', 'a:2:{s:13:\"wbcr_gonzales\";s:10:\"free-1.0.7\";s:12:\"wbcr_clearfy\";s:10:\"free-1.5.0\";}', 'yes'),
(798, 'wbcr_clearfy_deactive_preinstall_components', 'a:1:{i:0;s:9:\"cyrlitera\";}', 'yes'),
(799, 'wbcr_clearfy_plugin_activated', '1554148614', 'yes'),
(837, 'fs_active_plugins', 'O:8:\"stdClass\":0:{}', 'yes'),
(838, 'fs_debug_mode', '', 'yes'),
(839, 'fs_accounts', 'a:6:{s:21:\"id_slug_type_path_map\";a:1:{i:2951;a:2:{s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:4:\"type\";s:6:\"plugin\";}}s:11:\"plugin_data\";a:1:{s:17:\"wp-asset-clean-up\";a:15:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:9:\"prev_path\";s:27:\"wp-asset-clean-up/wpacu.php\";}s:17:\"install_timestamp\";i:1554149200;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.2.4\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:7:\"1.3.2.5\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"was_plugin_loaded\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:9:\"localhost\";s:9:\"server_ip\";s:3:\"::1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1554149203;s:7:\"version\";s:7:\"1.3.2.5\";}s:15:\"prev_is_premium\";b:0;s:16:\"uninstall_reason\";O:8:\"stdClass\":3:{s:2:\"id\";s:2:\"15\";s:4:\"info\";s:0:\"\";s:12:\"is_anonymous\";b:0;}}}s:13:\"file_slug_map\";a:1:{s:27:\"wp-asset-clean-up/wpacu.php\";s:17:\"wp-asset-clean-up\";}s:7:\"plugins\";a:1:{s:17:\"wp-asset-clean-up\";O:9:\"FS_Plugin\":20:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:33:\"Asset CleanUp: Page Speed Booster\";s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:12:\"premium_slug\";s:25:\"wp-asset-clean-up-premium\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:4:\"file\";s:27:\"wp-asset-clean-up/wpacu.php\";s:7:\"version\";s:7:\"1.3.2.5\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:14:\"premium_suffix\";s:9:\"(Premium)\";s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_70ecc6600cb03b5168150b4c99257\";s:10:\"secret_key\";N;s:2:\"id\";s:4:\"2951\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"51fa79bb6ef83171be728be84b413a81\";s:13:\"admin_notices\";a:1:{s:17:\"wp-asset-clean-up\";a:0:{}}}', 'yes'),
(840, 'fs_api_cache', 'a:0:{}', 'yes'),
(841, 'wpassetcleanup_do_activation_redirect_first_time', '1', 'yes'),
(844, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:1:{s:8:\"required\";b:0;}}', 'yes'),
(894, 'aqm-dequeued', 'a:2:{s:6:\"styles\";a:2:{s:22:\"current-template-style\";a:4:{s:6:\"handle\";s:22:\"current-template-style\";s:3:\"src\";s:106:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/show-current-template/css/style.css\";s:3:\"ver\";s:5:\"false\";s:4:\"args\";s:3:\"all\";}s:14:\"contact-form-7\";a:4:{s:6:\"handle\";s:14:\"contact-form-7\";s:3:\"src\";s:109:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/contact-form-7/includes/css/styles.css\";s:3:\"ver\";s:5:\"5.1.1\";s:4:\"args\";s:3:\"all\";}}s:7:\"scripts\";a:1:{s:14:\"contact-form-7\";a:6:{s:6:\"handle\";s:14:\"contact-form-7\";s:3:\"src\";s:108:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/contact-form-7/includes/js/scripts.js\";s:4:\"deps\";a:1:{i:0;s:6:\"jquery\";}s:3:\"ver\";s:5:\"5.1.1\";s:4:\"args\";s:0:\"\";s:5:\"extra\";a:2:{s:5:\"group\";s:1:\"1\";s:4:\"data\";s:173:\"var wpcf7 = {\\\"apiSettings\\\":{\\\"root\\\":\\\"https:\\\\/\\\\/localhost\\\\/wordpress-bootstrap-4-boilerplate\\\\/wp-json\\\\/contact-form-7\\\\/v1\\\",\\\"namespace\\\":\\\"contact-form-7\\\\/v1\\\"}};\";}}}}', 'yes'),
(919, '_transient_timeout_wpseo_link_table_inaccessible', '1615363338', 'no'),
(920, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(921, '_transient_timeout_wpseo_meta_table_inaccessible', '1615363338', 'no'),
(922, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(1007, 'cfdb7_view_install_date', '2020-03-10 8:10:09', 'yes'),
(1020, 'fvm-last-cache-update', '1583836194', 'yes'),
(1021, 'fastvelocity_min_fvm_fix_editor', '1', 'yes'),
(1022, 'fastvelocity_min_remove_print_mediatypes', '1', 'yes'),
(1023, 'fastvelocity_fvm_clean_header_one', '1', 'yes'),
(1024, 'fastvelocity_min_skip_google_fonts', '1', 'yes'),
(1025, 'fastvelocity_min_force_inline_css_footer', '1', 'yes'),
(1026, 'fastvelocity_min_skip_cssorder', '1', 'yes'),
(1027, 'fastvelocity_gfonts_method', '1', 'yes'),
(1028, 'fastvelocity_fontawesome_method', '1', 'yes'),
(1029, 'fastvelocity_min_disable_css_inline_merge', '1', 'yes'),
(1030, 'fastvelocity_min_blacklist', '/html5shiv.js\r\n/html5shiv-printshiv.min.js\r\n/excanvas.js\r\n/avada-ie9.js\r\n/respond.js\r\n/respond.min.js\r\n/selectivizr.js\r\n/Avada/assets/css/ie.css\r\n/html5.js\r\n/IE9.js\r\n/fusion-ie9.js\r\n/vc_lte_ie9.min.css\r\n/old-ie.css\r\n/ie.css\r\n/vc-ie8.min.css\r\n/mailchimp-for-wp/assets/js/third-party/placeholders.min.js\r\n/assets/js/plugins/wp-enqueue/min/webfontloader.js\r\n/a.optnmstr.com/app/js/api.min.js\r\n/pixelyoursite/js/public.js\r\n/assets/js/wcdrip-drip.js', 'yes'),
(1031, 'fastvelocity_min_ignorelist', '/Avada/assets/js/main.min.js\r\n/woocommerce-product-search/js/product-search.js\r\n/includes/builder/scripts/frontend-builder-scripts.js\r\n/assets/js/jquery.themepunch.tools.min.js\r\n/js/TweenMax.min.js\r\n/jupiter/assets/js/min/full-scripts\r\n/wp-content/themes/Divi/core/admin/js/react-dom.production.min.js\r\n/LayerSlider/static/layerslider/js/greensock.js\r\n/themes/kalium/assets/js/main.min.js\r\n/elementor/assets/js/common.min.js\r\n/elementor/assets/js/frontend.min.js\r\n/elementor-pro/assets/js/frontend.min.js\r\n/Divi/core/admin/js/react-dom.production.min.js\r\n/kalium/assets/js/main.min.js', 'yes'),
(1032, 'fastvelocity_plugin_version', '2.7.9', 'yes'),
(1033, 'fastvelocity_preserve_settings_on_uninstall', '1', 'yes'),
(1038, 'recovery_keys', 'a:0:{}', 'yes'),
(1039, 'wp_page_for_privacy_policy', '0', 'yes'),
(1040, 'show_comments_cookies_opt_in', '1', 'yes'),
(1041, 'admin_email_lifespan', '1617535340', 'yes'),
(1042, 'db_upgraded', '', 'yes'),
(1074, 'newsletter_logger_secret', '290f647a', 'yes'),
(1076, 'newsletter_main_first_install_time', '1583834072', 'no'),
(1077, 'newsletter_main', 'a:31:{s:11:\"return_path\";s:0:\"\";s:8:\"reply_to\";s:0:\"\";s:12:\"sender_email\";s:20:\"newsletter@localhost\";s:11:\"sender_name\";s:14:\"WP Bootstrap 4\";s:6:\"editor\";i:0;s:13:\"scheduler_max\";i:100;s:9:\"phpmailer\";i:0;s:5:\"debug\";i:0;s:5:\"track\";i:1;s:3:\"css\";s:0:\"\";s:12:\"css_disabled\";i:0;s:2:\"ip\";s:0:\"\";s:4:\"page\";i:9;s:19:\"disable_cron_notice\";i:0;s:13:\"do_shortcodes\";i:0;s:11:\"header_logo\";s:0:\"\";s:12:\"header_title\";s:14:\"WP Bootstrap 4\";s:10:\"header_sub\";s:34:\"... egy újabb WordPress honlap...\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(1078, 'newsletter_main_info', 'a:16:{s:11:\"header_logo\";a:1:{s:2:\"id\";i:0;}s:12:\"header_title\";s:14:\"WP Bootstrap 4\";s:10:\"header_sub\";s:34:\"... egy újabb WordPress honlap...\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(1079, 'newsletter_main_smtp', 'a:7:{s:7:\"enabled\";i:0;s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:4:\"pass\";s:0:\"\";s:4:\"port\";i:25;s:6:\"secure\";s:0:\"\";s:12:\"ssl_insecure\";i:0;}', 'yes'),
(1080, 'newsletter_main_version', '1.6.3', 'yes'),
(1081, 'newsletter_subscription_first_install_time', '1583834072', 'no'),
(1082, 'newsletter_subscription_antibot', 'a:6:{s:12:\"ip_blacklist\";a:0:{}s:17:\"address_blacklist\";a:0:{}s:9:\"antiflood\";i:60;s:7:\"akismet\";i:0;s:7:\"captcha\";i:0;s:8:\"disabled\";i:0;}', 'yes'),
(1083, 'newsletter', 'a:14:{s:14:\"noconfirmation\";i:1;s:12:\"notify_email\";s:18:\"doxa84@hotmail.com\";s:8:\"multiple\";i:1;s:6:\"notify\";i:0;s:10:\"error_text\";s:102:\"<p>You cannot subscribe with the email address you entered, please contact the site administrator.</p>\";s:17:\"subscription_text\";s:19:\"{subscription_form}\";s:17:\"confirmation_text\";s:104:\"<p>A confirmation email is on the way. Follow the instructions and check the spam folder. Thank you.</p>\";s:20:\"confirmation_subject\";s:32:\"Please confirm your subscription\";s:21:\"confirmation_tracking\";s:0:\"\";s:20:\"confirmation_message\";s:94:\"<p>Please confirm your subscription <a href=\"{subscription_confirm_url}\">clicking here</a></p>\";s:14:\"confirmed_text\";s:43:\"<p>Your subscription has been confirmed</p>\";s:17:\"confirmed_subject\";s:7:\"Welcome\";s:17:\"confirmed_message\";s:130:\"<p>This message confirms your subscription to our newsletter. Thank you!</p><hr><p><a href=\"{profile_url}\">Change your profile</p>\";s:18:\"confirmed_tracking\";s:0:\"\";}', 'yes'),
(1084, 'newsletter_subscription_lists', 'a:160:{s:6:\"list_1\";s:0:\"\";s:13:\"list_1_status\";i:0;s:14:\"list_1_checked\";i:0;s:13:\"list_1_forced\";i:0;s:6:\"list_2\";s:0:\"\";s:13:\"list_2_status\";i:0;s:14:\"list_2_checked\";i:0;s:13:\"list_2_forced\";i:0;s:6:\"list_3\";s:0:\"\";s:13:\"list_3_status\";i:0;s:14:\"list_3_checked\";i:0;s:13:\"list_3_forced\";i:0;s:6:\"list_4\";s:0:\"\";s:13:\"list_4_status\";i:0;s:14:\"list_4_checked\";i:0;s:13:\"list_4_forced\";i:0;s:6:\"list_5\";s:0:\"\";s:13:\"list_5_status\";i:0;s:14:\"list_5_checked\";i:0;s:13:\"list_5_forced\";i:0;s:6:\"list_6\";s:0:\"\";s:13:\"list_6_status\";i:0;s:14:\"list_6_checked\";i:0;s:13:\"list_6_forced\";i:0;s:6:\"list_7\";s:0:\"\";s:13:\"list_7_status\";i:0;s:14:\"list_7_checked\";i:0;s:13:\"list_7_forced\";i:0;s:6:\"list_8\";s:0:\"\";s:13:\"list_8_status\";i:0;s:14:\"list_8_checked\";i:0;s:13:\"list_8_forced\";i:0;s:6:\"list_9\";s:0:\"\";s:13:\"list_9_status\";i:0;s:14:\"list_9_checked\";i:0;s:13:\"list_9_forced\";i:0;s:7:\"list_10\";s:0:\"\";s:14:\"list_10_status\";i:0;s:15:\"list_10_checked\";i:0;s:14:\"list_10_forced\";i:0;s:7:\"list_11\";s:0:\"\";s:14:\"list_11_status\";i:0;s:15:\"list_11_checked\";i:0;s:14:\"list_11_forced\";i:0;s:7:\"list_12\";s:0:\"\";s:14:\"list_12_status\";i:0;s:15:\"list_12_checked\";i:0;s:14:\"list_12_forced\";i:0;s:7:\"list_13\";s:0:\"\";s:14:\"list_13_status\";i:0;s:15:\"list_13_checked\";i:0;s:14:\"list_13_forced\";i:0;s:7:\"list_14\";s:0:\"\";s:14:\"list_14_status\";i:0;s:15:\"list_14_checked\";i:0;s:14:\"list_14_forced\";i:0;s:7:\"list_15\";s:0:\"\";s:14:\"list_15_status\";i:0;s:15:\"list_15_checked\";i:0;s:14:\"list_15_forced\";i:0;s:7:\"list_16\";s:0:\"\";s:14:\"list_16_status\";i:0;s:15:\"list_16_checked\";i:0;s:14:\"list_16_forced\";i:0;s:7:\"list_17\";s:0:\"\";s:14:\"list_17_status\";i:0;s:15:\"list_17_checked\";i:0;s:14:\"list_17_forced\";i:0;s:7:\"list_18\";s:0:\"\";s:14:\"list_18_status\";i:0;s:15:\"list_18_checked\";i:0;s:14:\"list_18_forced\";i:0;s:7:\"list_19\";s:0:\"\";s:14:\"list_19_status\";i:0;s:15:\"list_19_checked\";i:0;s:14:\"list_19_forced\";i:0;s:7:\"list_20\";s:0:\"\";s:14:\"list_20_status\";i:0;s:15:\"list_20_checked\";i:0;s:14:\"list_20_forced\";i:0;s:7:\"list_21\";s:0:\"\";s:14:\"list_21_status\";i:0;s:15:\"list_21_checked\";i:0;s:14:\"list_21_forced\";i:0;s:7:\"list_22\";s:0:\"\";s:14:\"list_22_status\";i:0;s:15:\"list_22_checked\";i:0;s:14:\"list_22_forced\";i:0;s:7:\"list_23\";s:0:\"\";s:14:\"list_23_status\";i:0;s:15:\"list_23_checked\";i:0;s:14:\"list_23_forced\";i:0;s:7:\"list_24\";s:0:\"\";s:14:\"list_24_status\";i:0;s:15:\"list_24_checked\";i:0;s:14:\"list_24_forced\";i:0;s:7:\"list_25\";s:0:\"\";s:14:\"list_25_status\";i:0;s:15:\"list_25_checked\";i:0;s:14:\"list_25_forced\";i:0;s:7:\"list_26\";s:0:\"\";s:14:\"list_26_status\";i:0;s:15:\"list_26_checked\";i:0;s:14:\"list_26_forced\";i:0;s:7:\"list_27\";s:0:\"\";s:14:\"list_27_status\";i:0;s:15:\"list_27_checked\";i:0;s:14:\"list_27_forced\";i:0;s:7:\"list_28\";s:0:\"\";s:14:\"list_28_status\";i:0;s:15:\"list_28_checked\";i:0;s:14:\"list_28_forced\";i:0;s:7:\"list_29\";s:0:\"\";s:14:\"list_29_status\";i:0;s:15:\"list_29_checked\";i:0;s:14:\"list_29_forced\";i:0;s:7:\"list_30\";s:0:\"\";s:14:\"list_30_status\";i:0;s:15:\"list_30_checked\";i:0;s:14:\"list_30_forced\";i:0;s:7:\"list_31\";s:0:\"\";s:14:\"list_31_status\";i:0;s:15:\"list_31_checked\";i:0;s:14:\"list_31_forced\";i:0;s:7:\"list_32\";s:0:\"\";s:14:\"list_32_status\";i:0;s:15:\"list_32_checked\";i:0;s:14:\"list_32_forced\";i:0;s:7:\"list_33\";s:0:\"\";s:14:\"list_33_status\";i:0;s:15:\"list_33_checked\";i:0;s:14:\"list_33_forced\";i:0;s:7:\"list_34\";s:0:\"\";s:14:\"list_34_status\";i:0;s:15:\"list_34_checked\";i:0;s:14:\"list_34_forced\";i:0;s:7:\"list_35\";s:0:\"\";s:14:\"list_35_status\";i:0;s:15:\"list_35_checked\";i:0;s:14:\"list_35_forced\";i:0;s:7:\"list_36\";s:0:\"\";s:14:\"list_36_status\";i:0;s:15:\"list_36_checked\";i:0;s:14:\"list_36_forced\";i:0;s:7:\"list_37\";s:0:\"\";s:14:\"list_37_status\";i:0;s:15:\"list_37_checked\";i:0;s:14:\"list_37_forced\";i:0;s:7:\"list_38\";s:0:\"\";s:14:\"list_38_status\";i:0;s:15:\"list_38_checked\";i:0;s:14:\"list_38_forced\";i:0;s:7:\"list_39\";s:0:\"\";s:14:\"list_39_status\";i:0;s:15:\"list_39_checked\";i:0;s:14:\"list_39_forced\";i:0;s:7:\"list_40\";s:0:\"\";s:14:\"list_40_status\";i:0;s:15:\"list_40_checked\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(1085, 'newsletter_subscription_template', 'a:1:{s:8:\"template\";s:2365:\"<!DOCTYPE html>\n<html>\n    <head>\n        <!-- General styles, not used by all email clients -->\n        <style type=\"text/css\" media=\"all\">\n            a {\n                text-decoration: none;\n                color: #0088cc;\n            }\n            hr {\n                border-top: 1px solid #999;\n            }\n        </style>\n    </head>\n\n    <!-- KEEP THE LAYOUT SIMPLE: THOSE ARE SERVICE MESSAGES. -->\n    <body style=\"margin: 0; padding: 0;\">\n\n        <!-- Top title with dark background -->\n        <table style=\"background-color: #444; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\">\n            <tr>\n                <td style=\"padding: 20px; text-align: center; font-family: sans-serif; color: #fff; font-size: 28px\">\n                    {blog_title}\n                </td>\n            </tr>\n        </table>\n\n        <!-- Main table 100% wide with background color #eee -->    \n        <table style=\"background-color: #eee; width: 100%;\">\n            <tr>\n                <td align=\"center\" style=\"padding: 15px;\">\n\n                    <!-- Content table with backgdound color #fff, width 500px -->\n                    <table style=\"background-color: #fff; max-width: 600px; width: 100%; border: 1px solid #ddd;\">\n                        <tr>\n                            <td style=\"padding: 15px; color: #333; font-size: 16px; font-family: sans-serif\">\n                                <!-- The \"message\" tag below is replaced with one of confirmation, welcome or goodbye messages -->\n                                <!-- Messages content can be configured on Newsletter List Building panels --> \n\n                                {message}\n\n                                <hr>\n                                <!-- Signature if not already added to single messages (surround with <p>) -->\n                                <p>\n                                    <small>\n                                        <a href=\"{blog_url}\">{blog_url}</a><br>\n                                        {company_name}<br>\n                                        {company_address}\n                                    </small>\n                                </p>\n                                \n\n                            </td>\n                        </tr>\n                    </table>\n\n                </td>\n            </tr>\n        </table>\n\n    </body>\n</html>\";}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1086, 'newsletter_profile', 'a:184:{s:5:\"email\";s:5:\"Email\";s:11:\"email_error\";s:28:\"Email address is not correct\";s:4:\"name\";s:23:\"First name or full name\";s:10:\"name_error\";s:16:\"Name is required\";s:11:\"name_status\";i:0;s:10:\"name_rules\";i:0;s:7:\"surname\";s:9:\"Last name\";s:13:\"surname_error\";s:21:\"Last name is required\";s:14:\"surname_status\";i:0;s:10:\"sex_status\";i:0;s:3:\"sex\";s:3:\"I\'m\";s:7:\"privacy\";s:44:\"By continuing, you accept the privacy policy\";s:13:\"privacy_error\";s:34:\"You must accept the privacy policy\";s:14:\"privacy_status\";i:0;s:11:\"privacy_url\";s:0:\"\";s:18:\"privacy_use_wp_url\";i:0;s:9:\"subscribe\";s:9:\"Subscribe\";s:12:\"title_female\";s:3:\"Ms.\";s:10:\"title_male\";s:3:\"Mr.\";s:10:\"title_none\";s:4:\"Dear\";s:8:\"sex_male\";s:3:\"Man\";s:10:\"sex_female\";s:5:\"Woman\";s:8:\"sex_none\";s:13:\"Not specified\";s:13:\"profile_error\";s:34:\"A mandatory field is not filled in\";s:16:\"profile_1_status\";i:0;s:9:\"profile_1\";s:0:\"\";s:14:\"profile_1_type\";s:4:\"text\";s:21:\"profile_1_placeholder\";s:0:\"\";s:15:\"profile_1_rules\";i:0;s:17:\"profile_1_options\";s:0:\"\";s:16:\"profile_2_status\";i:0;s:9:\"profile_2\";s:0:\"\";s:14:\"profile_2_type\";s:4:\"text\";s:21:\"profile_2_placeholder\";s:0:\"\";s:15:\"profile_2_rules\";i:0;s:17:\"profile_2_options\";s:0:\"\";s:16:\"profile_3_status\";i:0;s:9:\"profile_3\";s:0:\"\";s:14:\"profile_3_type\";s:4:\"text\";s:21:\"profile_3_placeholder\";s:0:\"\";s:15:\"profile_3_rules\";i:0;s:17:\"profile_3_options\";s:0:\"\";s:16:\"profile_4_status\";i:0;s:9:\"profile_4\";s:0:\"\";s:14:\"profile_4_type\";s:4:\"text\";s:21:\"profile_4_placeholder\";s:0:\"\";s:15:\"profile_4_rules\";i:0;s:17:\"profile_4_options\";s:0:\"\";s:16:\"profile_5_status\";i:0;s:9:\"profile_5\";s:0:\"\";s:14:\"profile_5_type\";s:4:\"text\";s:21:\"profile_5_placeholder\";s:0:\"\";s:15:\"profile_5_rules\";i:0;s:17:\"profile_5_options\";s:0:\"\";s:16:\"profile_6_status\";i:0;s:9:\"profile_6\";s:0:\"\";s:14:\"profile_6_type\";s:4:\"text\";s:21:\"profile_6_placeholder\";s:0:\"\";s:15:\"profile_6_rules\";i:0;s:17:\"profile_6_options\";s:0:\"\";s:16:\"profile_7_status\";i:0;s:9:\"profile_7\";s:0:\"\";s:14:\"profile_7_type\";s:4:\"text\";s:21:\"profile_7_placeholder\";s:0:\"\";s:15:\"profile_7_rules\";i:0;s:17:\"profile_7_options\";s:0:\"\";s:16:\"profile_8_status\";i:0;s:9:\"profile_8\";s:0:\"\";s:14:\"profile_8_type\";s:4:\"text\";s:21:\"profile_8_placeholder\";s:0:\"\";s:15:\"profile_8_rules\";i:0;s:17:\"profile_8_options\";s:0:\"\";s:16:\"profile_9_status\";i:0;s:9:\"profile_9\";s:0:\"\";s:14:\"profile_9_type\";s:4:\"text\";s:21:\"profile_9_placeholder\";s:0:\"\";s:15:\"profile_9_rules\";i:0;s:17:\"profile_9_options\";s:0:\"\";s:17:\"profile_10_status\";i:0;s:10:\"profile_10\";s:0:\"\";s:15:\"profile_10_type\";s:4:\"text\";s:22:\"profile_10_placeholder\";s:0:\"\";s:16:\"profile_10_rules\";i:0;s:18:\"profile_10_options\";s:0:\"\";s:17:\"profile_11_status\";i:0;s:10:\"profile_11\";s:0:\"\";s:15:\"profile_11_type\";s:4:\"text\";s:22:\"profile_11_placeholder\";s:0:\"\";s:16:\"profile_11_rules\";i:0;s:18:\"profile_11_options\";s:0:\"\";s:17:\"profile_12_status\";i:0;s:10:\"profile_12\";s:0:\"\";s:15:\"profile_12_type\";s:4:\"text\";s:22:\"profile_12_placeholder\";s:0:\"\";s:16:\"profile_12_rules\";i:0;s:18:\"profile_12_options\";s:0:\"\";s:17:\"profile_13_status\";i:0;s:10:\"profile_13\";s:0:\"\";s:15:\"profile_13_type\";s:4:\"text\";s:22:\"profile_13_placeholder\";s:0:\"\";s:16:\"profile_13_rules\";i:0;s:18:\"profile_13_options\";s:0:\"\";s:17:\"profile_14_status\";i:0;s:10:\"profile_14\";s:0:\"\";s:15:\"profile_14_type\";s:4:\"text\";s:22:\"profile_14_placeholder\";s:0:\"\";s:16:\"profile_14_rules\";i:0;s:18:\"profile_14_options\";s:0:\"\";s:17:\"profile_15_status\";i:0;s:10:\"profile_15\";s:0:\"\";s:15:\"profile_15_type\";s:4:\"text\";s:22:\"profile_15_placeholder\";s:0:\"\";s:16:\"profile_15_rules\";i:0;s:18:\"profile_15_options\";s:0:\"\";s:17:\"profile_16_status\";i:0;s:10:\"profile_16\";s:0:\"\";s:15:\"profile_16_type\";s:4:\"text\";s:22:\"profile_16_placeholder\";s:0:\"\";s:16:\"profile_16_rules\";i:0;s:18:\"profile_16_options\";s:0:\"\";s:17:\"profile_17_status\";i:0;s:10:\"profile_17\";s:0:\"\";s:15:\"profile_17_type\";s:4:\"text\";s:22:\"profile_17_placeholder\";s:0:\"\";s:16:\"profile_17_rules\";i:0;s:18:\"profile_17_options\";s:0:\"\";s:17:\"profile_18_status\";i:0;s:10:\"profile_18\";s:0:\"\";s:15:\"profile_18_type\";s:4:\"text\";s:22:\"profile_18_placeholder\";s:0:\"\";s:16:\"profile_18_rules\";i:0;s:18:\"profile_18_options\";s:0:\"\";s:17:\"profile_19_status\";i:0;s:10:\"profile_19\";s:0:\"\";s:15:\"profile_19_type\";s:4:\"text\";s:22:\"profile_19_placeholder\";s:0:\"\";s:16:\"profile_19_rules\";i:0;s:18:\"profile_19_options\";s:0:\"\";s:17:\"profile_20_status\";i:0;s:10:\"profile_20\";s:0:\"\";s:15:\"profile_20_type\";s:4:\"text\";s:22:\"profile_20_placeholder\";s:0:\"\";s:16:\"profile_20_rules\";i:0;s:18:\"profile_20_options\";s:0:\"\";s:13:\"list_1_forced\";i:0;s:13:\"list_2_forced\";i:0;s:13:\"list_3_forced\";i:0;s:13:\"list_4_forced\";i:0;s:13:\"list_5_forced\";i:0;s:13:\"list_6_forced\";i:0;s:13:\"list_7_forced\";i:0;s:13:\"list_8_forced\";i:0;s:13:\"list_9_forced\";i:0;s:14:\"list_10_forced\";i:0;s:14:\"list_11_forced\";i:0;s:14:\"list_12_forced\";i:0;s:14:\"list_13_forced\";i:0;s:14:\"list_14_forced\";i:0;s:14:\"list_15_forced\";i:0;s:14:\"list_16_forced\";i:0;s:14:\"list_17_forced\";i:0;s:14:\"list_18_forced\";i:0;s:14:\"list_19_forced\";i:0;s:14:\"list_20_forced\";i:0;s:14:\"list_21_forced\";i:0;s:14:\"list_22_forced\";i:0;s:14:\"list_23_forced\";i:0;s:14:\"list_24_forced\";i:0;s:14:\"list_25_forced\";i:0;s:14:\"list_26_forced\";i:0;s:14:\"list_27_forced\";i:0;s:14:\"list_28_forced\";i:0;s:14:\"list_29_forced\";i:0;s:14:\"list_30_forced\";i:0;s:14:\"list_31_forced\";i:0;s:14:\"list_32_forced\";i:0;s:14:\"list_33_forced\";i:0;s:14:\"list_34_forced\";i:0;s:14:\"list_35_forced\";i:0;s:14:\"list_36_forced\";i:0;s:14:\"list_37_forced\";i:0;s:14:\"list_38_forced\";i:0;s:14:\"list_39_forced\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(1087, 'newsletter_subscription_version', '2.2.7', 'yes'),
(1088, 'newsletter_unsubscription_first_install_time', '1583834072', 'no'),
(1089, 'newsletter_unsubscription', 'a:6:{s:16:\"unsubscribe_text\";s:103:\"<p>Please confirm you want to unsubscribe <a href=\"{unsubscription_confirm_url}\">clicking here</a>.</p>\";s:10:\"error_text\";s:99:\"<p>Subscriber not found, it probably has already been removed. No further actions are required.</p>\";s:17:\"unsubscribed_text\";s:124:\"<p>Your subscription has been deleted. If that was an error you can <a href=\"{reactivate_url}\">subscribe again here</a>.</p>\";s:20:\"unsubscribed_subject\";s:7:\"Goodbye\";s:20:\"unsubscribed_message\";s:87:\"<p>This message confirms that you have unsubscribed from our newsletter. Thank you.</p>\";s:16:\"reactivated_text\";s:46:\"<p>Your subscription has been reactivated.</p>\";}', 'yes'),
(1090, 'newsletter_unsubscription_version', '1.0.3', 'yes'),
(1091, 'newsletter_profile_first_install_time', '1583834072', 'no'),
(1092, 'newsletter_profile_main', 'a:8:{s:4:\"text\";s:188:\"{profile_form}\n    <p>If you change your email address, a confirmation email will be sent to activate it.</p>\n    <p><a href=\"{unsubscription_confirm_url}\">Cancel your subscription</a></p>\";s:13:\"email_changed\";s:81:\"Your email has been changed, an activation email has been sent with instructions.\";s:5:\"error\";s:42:\"Your email is not valid or already in use.\";s:10:\"save_label\";s:4:\"Save\";s:13:\"privacy_label\";s:21:\"Read our privacy note\";s:5:\"saved\";s:14:\"Profile saved.\";s:18:\"export_newsletters\";i:0;s:3:\"url\";s:0:\"\";}', 'yes'),
(1093, 'newsletter_profile_version', '1.1.0', 'yes'),
(1094, 'newsletter_emails_first_install_time', '1583834072', 'no'),
(1095, 'newsletter_emails', 'a:1:{s:5:\"theme\";s:7:\"default\";}', 'yes'),
(1096, 'newsletter_emails_theme_default', 'a:0:{}', 'no'),
(1097, 'newsletter_emails_version', '1.1.5', 'yes'),
(1098, 'newsletter_users_first_install_time', '1583834072', 'no'),
(1099, 'newsletter_users', 'a:0:{}', 'yes'),
(1100, 'newsletter_users_version', '1.3.0', 'yes'),
(1101, 'newsletter_statistics_first_install_time', '1583834072', 'no'),
(1102, 'newsletter_statistics', 'a:1:{s:3:\"key\";s:32:\"5785cca00b51a12adef903c5b8e9fdf8\";}', 'yes'),
(1103, 'newsletter_statistics_version', '1.2.7', 'yes'),
(1104, 'newsletter_install_time', '1583834072', 'no'),
(1105, 'widget_newsletterwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1106, 'widget_newsletterwidgetminimal', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1109, 'newsletter_page', '9', 'no'),
(1112, 'newsletter_diagnostic_cron_calls', 'a:100:{i:0;i:1601991868;i:1;i:1602000438;i:2;i:1602000674;i:3;i:1602000916;i:4;i:1602052423;i:5;i:1602052533;i:6;i:1602052775;i:7;i:1602052896;i:8;i:1602053138;i:9;i:1602053380;i:10;i:1602053743;i:11;i:1602053985;i:12;i:1602054347;i:13;i:1602054589;i:14;i:1602054952;i:15;i:1602055194;i:16;i:1602055557;i:17;i:1602055799;i:18;i:1602057656;i:19;i:1602057892;i:20;i:1602058255;i:21;i:1602058497;i:22;i:1602058800;i:23;i:1602058862;i:24;i:1602058880;i:25;i:1602058886;i:26;i:1602058897;i:27;i:1602058900;i:28;i:1602058904;i:29;i:1602058914;i:30;i:1602059003;i:31;i:1602059095;i:32;i:1602059335;i:33;i:1602059366;i:34;i:1602059696;i:35;i:1602060020;i:36;i:1602060297;i:37;i:1602060625;i:38;i:1602060867;i:39;i:1602061230;i:40;i:1602061381;i:41;i:1602061472;i:42;i:1602061835;i:43;i:1602062077;i:44;i:1602062440;i:45;i:1602062682;i:46;i:1602063044;i:47;i:1602063286;i:48;i:1602063649;i:49;i:1602063891;i:50;i:1602064254;i:51;i:1602064496;i:52;i:1602064859;i:53;i:1602065101;i:54;i:1602065463;i:55;i:1602065705;i:56;i:1602066068;i:57;i:1602066310;i:58;i:1602068498;i:59;i:1602068734;i:60;i:1602068975;i:61;i:1602069308;i:62;i:1602072209;i:63;i:1602072299;i:64;i:1602077471;i:65;i:1602077784;i:66;i:1603197305;i:67;i:1603197430;i:68;i:1603197445;i:69;i:1603197571;i:70;i:1603197581;i:71;i:1603197674;i:72;i:1603199770;i:73;i:1603199792;i:74;i:1603199856;i:75;i:1603199869;i:76;i:1603199986;i:77;i:1603200250;i:78;i:1603200262;i:79;i:1603200295;i:80;i:1603200365;i:81;i:1603200432;i:82;i:1603200538;i:83;i:1603200596;i:84;i:1603200837;i:85;i:1603200842;i:86;i:1603200900;i:87;i:1603201027;i:88;i:1603201074;i:89;i:1603201136;i:90;i:1603201159;i:91;i:1603201220;i:92;i:1603201222;i:93;i:1603201422;i:94;i:1603201452;i:95;i:1603201508;i:96;i:1603201513;i:97;i:1603357938;i:98;i:1603357992;i:99;i:1603358057;}', 'no'),
(1113, 'newsletter_diagnostic_cron_data', 'a:3:{s:4:\"mean\";d:13660.89;s:3:\"max\";i:1119521;s:3:\"min\";i:2;}', 'no'),
(1138, '_transient_wpassetcleanup_do_activation_redirect_first_time', '1', 'yes'),
(1141, 'wpassetcleanup_first_usage', '1583836277', 'no'),
(1142, 'wpassetcleanup_tracking_notice', '1', 'yes'),
(1147, 'wpassetcleanup_global_data', '{\"styles\":{\"assets_info\":{\"wp-block-library\":{\"src\":\"\\/wp-includes\\/css\\/dist\\/block-library\\/style.min.css\",\"extra\":{\"rtl\":\"replace\",\"suffix\":\".min\"}},\"contact-form-7\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/contact-form-7\\/includes\\/css\\/styles.css\",\"ver\":\"5.1.7\",\"args\":\"all\"},\"newsletter\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/newsletter\\/style.css\",\"ver\":\"6.5.4\",\"args\":\"all\"},\"current-template-style\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/show-current-template\\/css\\/style.css\",\"args\":\"all\"},\"moove_gdpr_frontend\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/gdpr-cookie-compliance\\/dist\\/styles\\/gdpr-main.css\",\"ver\":\"4.1.6\",\"args\":\"all\"}}},\"scripts\":{\"assets_info\":{\"jquery-core\":{\"src\":\"\\/wp-includes\\/js\\/jquery\\/jquery.js\",\"ver\":\"1.12.4-wp\"},\"jquery-migrate\":{\"src\":\"\\/wp-includes\\/js\\/jquery\\/jquery-migrate.min.js\",\"ver\":\"1.4.1\"},\"jquery\":{\"deps\":[\"jquery-core\",\"jquery-migrate\"],\"ver\":\"1.12.4-wp\"},\"contact-form-7\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/contact-form-7\\/includes\\/js\\/scripts.js\",\"deps\":[\"jquery\"],\"ver\":\"5.1.7\",\"extra\":{\"group\":1,\"data\":\"var wpcf7 = {\\\"apiSettings\\\":{\\\"root\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-json\\\\\\/contact-form-7\\\\\\/v1\\\",\\\"namespace\\\":\\\"contact-form-7\\\\\\/v1\\\"}};\"}},\"newsletter-subscription\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/newsletter\\/subscription\\/validate.js\",\"ver\":\"6.5.4\",\"extra\":{\"group\":1,\"data\":\"var newsletter = {\\\"messages\\\":{\\\"email_error\\\":\\\"Email address is not correct\\\",\\\"name_error\\\":\\\"Name is required\\\",\\\"surname_error\\\":\\\"Last name is required\\\",\\\"profile_error\\\":\\\"A mandatory field is not filled in\\\",\\\"privacy_error\\\":\\\"You must accept the privacy policy\\\"},\\\"profile_max\\\":\\\"20\\\"};\"}},\"themeplate-script\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/themes\\/themeplate-child\\/assets\\/js\\/themeplate.js\",\"ver\":\"0.1.0\",\"extra\":{\"group\":1,\"data\":\"var themeplate_options = {\\\"ajaxurl\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-admin\\\\\\/admin-ajax.php\\\"};\"}},\"moove_gdpr_frontend\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/gdpr-cookie-compliance\\/dist\\/scripts\\/main.js\",\"deps\":[\"jquery\"],\"ver\":\"4.1.6\",\"extra\":{\"group\":1,\"data\":\"var moove_frontend_gdpr_scripts = {\\\"ajaxurl\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-admin\\\\\\/admin-ajax.php\\\",\\\"post_id\\\":\\\"2\\\",\\\"plugin_dir\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-content\\\\\\/plugins\\\\\\/gdpr-cookie-compliance\\\",\\\"is_page\\\":\\\"1\\\",\\\"strict_init\\\":\\\"1\\\",\\\"enabled_default\\\":{\\\"third_party\\\":0,\\\"advanced\\\":0},\\\"geo_location\\\":\\\"false\\\",\\\"force_reload\\\":\\\"false\\\",\\\"is_single\\\":\\\"\\\",\\\"current_user\\\":\\\"1\\\",\\\"load_lity\\\":\\\"true\\\",\\\"cookie_expiration\\\":\\\"365\\\"};\"}}},\"preloads\":{\"contact-form-7\":\"basic\"}}}', 'no'),
(1150, 'wpassetcleanup_settings', '{\"dashboard_show\":\"1\",\"dom_get_type\":\"direct\",\"frontend_show_exceptions\":\"et_fb=1\\nct_builder=true\\nvc_editable=true\\npreview_nonce=\\n\",\"assets_list_layout\":\"by-location\",\"assets_list_layout_areas_status\":\"expanded\",\"assets_list_layout_plugin_area_status\":\"expanded\",\"assets_list_inline_code_status\":\"contracted\",\"minify_loaded_css_exceptions\":\"(.*?)\\\\.min.css\\n\\/plugins\\/wd-instagram-feed\\/(.*?).css\",\"minify_loaded_js_exceptions\":\"(.*?)\\\\.min.js\\n\\/plugins\\/wd-instagram-feed\\/(.*?).js\",\"inline_css_files_below_size\":\"1\",\"inline_css_files_below_size_input\":\"3\",\"inline_js_files_below_size_input\":\"3\",\"move_scripts_to_body_exceptions\":\"\\/\\/cdn.ampproject.org\\/\",\"combine_loaded_css_exceptions\":\"\\/plugins\\/wd-instagram-feed\\/(.*?).css\",\"combine_loaded_css_append_handle_extra\":\"1\",\"combine_loaded_js_exceptions\":\"\\/plugins\\/wd-instagram-feed\\/(.*?).js\",\"combine_loaded_js_append_handle_extra\":\"1\",\"defer_css_loaded_body\":\"moved\",\"cache_dynamic_loaded_css\":1,\"cache_dynamic_loaded_js\":1,\"input_style\":\"enhanced\",\"hide_core_files\":\"1\",\"fetch_cached_files_details_from\":\"disk\",\"clear_cached_files_after\":\"10\"}', 'no'),
(1151, 'wpassetcleanup_global_unload', '{\"styles\":[\"contact-form-7\",\"moove_gdpr_frontend\"]}', 'no'),
(1152, 'wpassetcleanup_bulk_unload', '[]', 'no'),
(1207, 'new_admin_email', 'doxa84@hotmail.com', 'yes'),
(1429, 'acf_version', '5.9.1', 'yes'),
(1432, 'cptui_post_types', 'a:4:{s:8:\"our_team\";a:30:{s:4:\"name\";s:8:\"our_team\";s:5:\"label\";s:8:\"Our Team\";s:14:\"singular_label\";s:8:\"Our Team\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"20\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:8:\"Our Team\";s:9:\"all_items\";s:12:\"All Our Team\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:16:\"Add new Our Team\";s:9:\"edit_item\";s:13:\"Edit Our Team\";s:8:\"new_item\";s:12:\"New Our Team\";s:9:\"view_item\";s:13:\"View Our Team\";s:10:\"view_items\";s:13:\"View Our Team\";s:12:\"search_items\";s:15:\"Search Our Team\";s:9:\"not_found\";s:17:\"No Our Team found\";s:18:\"not_found_in_trash\";s:26:\"No Our Team found in trash\";s:6:\"parent\";s:16:\"Parent Our Team:\";s:14:\"featured_image\";s:32:\"Featured image for this Our Team\";s:18:\"set_featured_image\";s:36:\"Set featured image for this Our Team\";s:21:\"remove_featured_image\";s:39:\"Remove featured image for this Our Team\";s:18:\"use_featured_image\";s:39:\"Use as featured image for this Our Team\";s:8:\"archives\";s:17:\"Our Team archives\";s:16:\"insert_into_item\";s:20:\"Insert into Our Team\";s:21:\"uploaded_to_this_item\";s:23:\"Upload to this Our Team\";s:17:\"filter_items_list\";s:20:\"Filter Our Team list\";s:21:\"items_list_navigation\";s:24:\"Our Team list navigation\";s:10:\"items_list\";s:13:\"Our Team list\";s:10:\"attributes\";s:19:\"Our Team attributes\";s:14:\"name_admin_bar\";s:8:\"Our Team\";s:14:\"item_published\";s:18:\"Our Team published\";s:24:\"item_published_privately\";s:29:\"Our Team published privately.\";s:22:\"item_reverted_to_draft\";s:27:\"Our Team reverted to draft.\";s:14:\"item_scheduled\";s:18:\"Our Team scheduled\";s:12:\"item_updated\";s:17:\"Our Team updated.\";s:17:\"parent_item_colon\";s:16:\"Parent Our Team:\";}s:15:\"custom_supports\";s:0:\"\";}s:11:\"testimonial\";a:30:{s:4:\"name\";s:11:\"testimonial\";s:5:\"label\";s:11:\"Testimonial\";s:14:\"singular_label\";s:12:\"Testimonials\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"21\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:6:\"editor\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:11:\"Testimonial\";s:9:\"all_items\";s:15:\"All Testimonial\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:20:\"Add new Testimonials\";s:9:\"edit_item\";s:17:\"Edit Testimonials\";s:8:\"new_item\";s:16:\"New Testimonials\";s:9:\"view_item\";s:17:\"View Testimonials\";s:10:\"view_items\";s:16:\"View Testimonial\";s:12:\"search_items\";s:18:\"Search Testimonial\";s:9:\"not_found\";s:20:\"No Testimonial found\";s:18:\"not_found_in_trash\";s:29:\"No Testimonial found in trash\";s:6:\"parent\";s:20:\"Parent Testimonials:\";s:14:\"featured_image\";s:36:\"Featured image for this Testimonials\";s:18:\"set_featured_image\";s:40:\"Set featured image for this Testimonials\";s:21:\"remove_featured_image\";s:43:\"Remove featured image for this Testimonials\";s:18:\"use_featured_image\";s:43:\"Use as featured image for this Testimonials\";s:8:\"archives\";s:21:\"Testimonials archives\";s:16:\"insert_into_item\";s:24:\"Insert into Testimonials\";s:21:\"uploaded_to_this_item\";s:27:\"Upload to this Testimonials\";s:17:\"filter_items_list\";s:23:\"Filter Testimonial list\";s:21:\"items_list_navigation\";s:27:\"Testimonial list navigation\";s:10:\"items_list\";s:16:\"Testimonial list\";s:10:\"attributes\";s:22:\"Testimonial attributes\";s:14:\"name_admin_bar\";s:12:\"Testimonials\";s:14:\"item_published\";s:22:\"Testimonials published\";s:24:\"item_published_privately\";s:33:\"Testimonials published privately.\";s:22:\"item_reverted_to_draft\";s:31:\"Testimonials reverted to draft.\";s:14:\"item_scheduled\";s:22:\"Testimonials scheduled\";s:12:\"item_updated\";s:21:\"Testimonials updated.\";s:17:\"parent_item_colon\";s:20:\"Parent Testimonials:\";}s:15:\"custom_supports\";s:0:\"\";}s:7:\"partner\";a:30:{s:4:\"name\";s:7:\"partner\";s:5:\"label\";s:7:\"Partner\";s:14:\"singular_label\";s:9:\"Partnerek\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"22\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:7:\"Partner\";s:9:\"all_items\";s:11:\"All Partner\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:17:\"Add new Partnerek\";s:9:\"edit_item\";s:14:\"Edit Partnerek\";s:8:\"new_item\";s:13:\"New Partnerek\";s:9:\"view_item\";s:14:\"View Partnerek\";s:10:\"view_items\";s:12:\"View Partner\";s:12:\"search_items\";s:14:\"Search Partner\";s:9:\"not_found\";s:16:\"No Partner found\";s:18:\"not_found_in_trash\";s:25:\"No Partner found in trash\";s:6:\"parent\";s:17:\"Parent Partnerek:\";s:14:\"featured_image\";s:33:\"Featured image for this Partnerek\";s:18:\"set_featured_image\";s:37:\"Set featured image for this Partnerek\";s:21:\"remove_featured_image\";s:40:\"Remove featured image for this Partnerek\";s:18:\"use_featured_image\";s:40:\"Use as featured image for this Partnerek\";s:8:\"archives\";s:18:\"Partnerek archives\";s:16:\"insert_into_item\";s:21:\"Insert into Partnerek\";s:21:\"uploaded_to_this_item\";s:24:\"Upload to this Partnerek\";s:17:\"filter_items_list\";s:19:\"Filter Partner list\";s:21:\"items_list_navigation\";s:23:\"Partner list navigation\";s:10:\"items_list\";s:12:\"Partner list\";s:10:\"attributes\";s:18:\"Partner attributes\";s:14:\"name_admin_bar\";s:9:\"Partnerek\";s:14:\"item_published\";s:19:\"Partnerek published\";s:24:\"item_published_privately\";s:30:\"Partnerek published privately.\";s:22:\"item_reverted_to_draft\";s:28:\"Partnerek reverted to draft.\";s:14:\"item_scheduled\";s:19:\"Partnerek scheduled\";s:12:\"item_updated\";s:18:\"Partnerek updated.\";s:17:\"parent_item_colon\";s:17:\"Parent Partnerek:\";}s:15:\"custom_supports\";s:0:\"\";}s:4:\"gyik\";a:30:{s:4:\"name\";s:4:\"gyik\";s:5:\"label\";s:4:\"GYIK\";s:14:\"singular_label\";s:4:\"GYIK\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"23\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:6:\"editor\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:4:\"GYIK\";s:9:\"all_items\";s:8:\"All GYIK\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:12:\"Add new GYIK\";s:9:\"edit_item\";s:9:\"Edit GYIK\";s:8:\"new_item\";s:8:\"New GYIK\";s:9:\"view_item\";s:9:\"View GYIK\";s:10:\"view_items\";s:9:\"View GYIK\";s:12:\"search_items\";s:11:\"Search GYIK\";s:9:\"not_found\";s:13:\"No GYIK found\";s:18:\"not_found_in_trash\";s:22:\"No GYIK found in trash\";s:6:\"parent\";s:12:\"Parent GYIK:\";s:14:\"featured_image\";s:28:\"Featured image for this GYIK\";s:18:\"set_featured_image\";s:32:\"Set featured image for this GYIK\";s:21:\"remove_featured_image\";s:35:\"Remove featured image for this GYIK\";s:18:\"use_featured_image\";s:35:\"Use as featured image for this GYIK\";s:8:\"archives\";s:13:\"GYIK archives\";s:16:\"insert_into_item\";s:16:\"Insert into GYIK\";s:21:\"uploaded_to_this_item\";s:19:\"Upload to this GYIK\";s:17:\"filter_items_list\";s:16:\"Filter GYIK list\";s:21:\"items_list_navigation\";s:20:\"GYIK list navigation\";s:10:\"items_list\";s:9:\"GYIK list\";s:10:\"attributes\";s:15:\"GYIK attributes\";s:14:\"name_admin_bar\";s:4:\"GYIK\";s:14:\"item_published\";s:14:\"GYIK published\";s:24:\"item_published_privately\";s:25:\"GYIK published privately.\";s:22:\"item_reverted_to_draft\";s:23:\"GYIK reverted to draft.\";s:14:\"item_scheduled\";s:14:\"GYIK scheduled\";s:12:\"item_updated\";s:13:\"GYIK updated.\";s:17:\"parent_item_colon\";s:12:\"Parent GYIK:\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(1826, 'wpmudev_recommended_plugins_registered', 'a:1:{s:23:\"wp-smushit/wp-smush.php\";a:1:{s:13:\"registered_at\";i:1585057547;}}', 'no'),
(2324, 'cfdb7_view_ignore_notice', 'true', 'yes'),
(2375, 'wpmdb_usage', 'a:2:{s:6:\"action\";s:8:\"savefile\";s:4:\"time\";i:1585729079;}', 'no'),
(2376, 'wpmdb_state_timeout_5e844e37c93ed', '1585815486', 'no'),
(2377, 'wpmdb_state_5e844e37c93ed', 'a:22:{s:6:\"action\";s:19:\"wpmdb_migrate_table\";s:6:\"intent\";s:8:\"savefile\";s:3:\"url\";s:0:\"\";s:9:\"form_data\";s:401:\"action=savefile&save_computer=1&gzip_file=1&connection_info=&replace_old%5B%5D=&replace_new%5B%5D=&replace_old%5B%5D=%2F%2Flocalhost%2Fdxengine&replace_new%5B%5D=%2F%2Flocalhost%2Fdxlanding&replace_old%5B%5D=C%3A%5Cxampp%5Chtdocs%5Cdxengine&replace_new%5B%5D=C%3A%5Cxampp%5Chtdocs%5Cdxlanding&replace_guids=1&exclude_transients=1&save_migration_profile_option=new&create_new_profile=&remote_json_data=\";s:5:\"stage\";s:7:\"migrate\";s:5:\"nonce\";s:10:\"ba4bedc83b\";s:12:\"site_details\";a:1:{s:5:\"local\";a:10:{s:12:\"is_multisite\";s:5:\"false\";s:8:\"site_url\";s:26:\"https://localhost/dxengine\";s:8:\"home_url\";s:25:\"http://localhost/dxengine\";s:6:\"prefix\";s:3:\"wp_\";s:15:\"uploads_baseurl\";s:45:\"http://localhost/dxengine/wp-content/uploads/\";s:7:\"uploads\";a:6:{s:4:\"path\";s:51:\"C:\\xampp\\htdocs\\dxengine/wp-content/uploads/2020/04\";s:3:\"url\";s:52:\"http://localhost/dxengine/wp-content/uploads/2020/04\";s:6:\"subdir\";s:8:\"/2020/04\";s:7:\"basedir\";s:43:\"C:\\xampp\\htdocs\\dxengine/wp-content/uploads\";s:7:\"baseurl\";s:44:\"http://localhost/dxengine/wp-content/uploads\";s:5:\"error\";b:0;}s:11:\"uploads_dir\";s:33:\"wp-content/uploads/wp-migrate-db/\";s:8:\"subsites\";a:0:{}s:13:\"subsites_info\";a:0:{}s:20:\"is_subdomain_install\";s:5:\"false\";}}s:4:\"code\";i:200;s:7:\"message\";s:2:\"OK\";s:4:\"body\";s:11:\"{\"error\":0}\";s:9:\"dump_path\";s:102:\"C:\\xampp\\htdocs\\dxengine\\wp-content\\uploads\\wp-migrate-db\\dxengine-migrate-20200401081759-kccid.sql.gz\";s:13:\"dump_filename\";s:37:\"dxengine-migrate-20200401081759-kccid\";s:8:\"dump_url\";s:103:\"http://localhost/dxengine/wp-content/uploads/wp-migrate-db/dxengine-migrate-20200401081759-wpflo.sql.gz\";s:10:\"db_version\";s:5:\"5.5.5\";s:8:\"site_url\";s:26:\"https://localhost/dxengine\";s:18:\"find_replace_pairs\";a:2:{s:11:\"replace_old\";a:2:{i:1;s:20:\"//localhost/dxengine\";i:2;s:24:\"C:\\xampp\\htdocs\\dxengine\";}s:11:\"replace_new\";a:2:{i:1;s:21:\"//localhost/dxlanding\";i:2;s:25:\"C:\\xampp\\htdocs\\dxlanding\";}}s:18:\"migration_state_id\";s:13:\"5e844e37c93ed\";s:5:\"table\";s:17:\"wp_yoast_seo_meta\";s:11:\"current_row\";s:0:\"\";s:10:\"last_table\";s:1:\"1\";s:12:\"primary_keys\";s:0:\"\";s:4:\"gzip\";s:1:\"0\";}', 'no'),
(2479, 'disallowed_keys', '', 'no'),
(2480, 'comment_previously_approved', '1', 'yes'),
(2481, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(2482, 'finished_updating_comment_type', '1', 'yes'),
(2485, 'can_compress_scripts', '1', 'no'),
(2492, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:6:\"15.1.1\";}', 'yes'),
(2526, 'jvcf7_show_label_error', 'errorMsgshow', 'yes'),
(2527, 'jvcf7_invalid_field_design', 'theme_1', 'yes'),
(2533, 'email-log-db', '0.3', 'yes'),
(2541, '_transient_health-check-site-status-result', '{\"good\":\"11\",\"recommended\":\"9\",\"critical\":\"0\"}', 'yes'),
(2637, 'bodhi_svgs_plugin_version', '2.3.18', 'yes'),
(2644, 'wpa_installed_date', '20201006', 'yes'),
(2645, 'wpa_field_name', 'field982', 'yes'),
(2646, 'wpa_error_message', ' Spamming or your Javascript is disabled !!', 'yes'),
(2647, 'wpa_stats', '{\"total\":{\"today\":{\"date\":\"20201006\",\"count\":0},\"week\":{\"date\":\"20201006\",\"count\":0},\"month\":{\"date\":\"20201006\",\"count\":0},\"all_time\":0}}', 'yes'),
(2666, 'jvcf7_hide_pro_notice', 'yes_7', 'yes'),
(2680, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/hu_HU/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"hu_HU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/hu_HU/wordpress-5.5.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1603358075;s:15:\"version_checked\";s:5:\"5.5.1\";s:12:\"translations\";a:0:{}}', 'no'),
(2682, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1603358084;s:7:\"checked\";a:4:{s:16:\"themeplate-child\";s:5:\"0.1.0\";s:10:\"themeplate\";s:5:\"0.1.0\";s:14:\"twentynineteen\";s:3:\"1.7\";s:12:\"twentytwenty\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:2:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(2892, '_transient_timeout_tnp_extensions_json', '1603456511', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2893, '_transient_tnp_extensions_json', '[\n    {\n        \"id\": \"85\",\n        \"children_fileid\": null,\n        \"version\": \"1.1.3\",\n        \"title\": \"Addons Manager\",\n        \"description\": \"\",\n        \"slug\": \"newsletter-extensions\",\n        \"type\": \"manager\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"\",\n        \"status\": \"4\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=85\",\n        \"wp_slug\": \"newsletter-extensions\\/extensions.php\"\n    },\n    {\n        \"id\": \"87\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.3\",\n        \"title\": \"Speed and Delivery Hours Control\",\n        \"description\": \"Configure a different delivery speed for each newsletter and the delivery hours window. Only for regular newsletters.\",\n        \"slug\": \"newsletter-speedcontrol\",\n        \"type\": \"legacy\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/12\\/speedcontrol.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=87\",\n        \"wp_slug\": \"newsletter-speedcontrol\\/speedcontrol.php\"\n    },\n    {\n        \"id\": \"90\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.2\",\n        \"title\": \"Sendinblue\",\n        \"description\": \"Integration with Sendinblue mailing service.\",\n        \"slug\": \"newsletter-sendinblue\",\n        \"type\": \"delivery\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/documentation\\/?p=198432\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=90\",\n        \"wp_slug\": \"newsletter-sendinblue\\/sendinblue.php\"\n    },\n    {\n        \"id\": \"92\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.1\",\n        \"title\": \"Popup Maker Integration (ALPHA version)\",\n        \"description\": \"Use Newsletter forms (via shortcode) with Popup Maker. \",\n        \"slug\": \"newsletter-popupmaker\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2020\\/07\\/popupmaker-32x32-1.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=92\",\n        \"wp_slug\": \"newsletter-popupmaker\\/popupmaker.php\"\n    },\n    {\n        \"id\": \"91\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.2\",\n        \"title\": \"Instasend\",\n        \"description\": \"Quickly create a newsletter from a post (free for limited time)\",\n        \"slug\": \"newsletter-instasend\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2020\\/05\\/instasend-32.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=91\",\n        \"wp_slug\": \"newsletter-instasend\\/instasend.php\"\n    },\n    {\n        \"id\": \"61\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.1.6\",\n        \"title\": \"Contact Form 7\",\n        \"description\": \"Adds the newsletter subscription feature to your Contact Form 7 forms.\",\n        \"slug\": \"newsletter-cf7\",\n        \"type\": \"integration\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/contact-form-7-extension\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/users-32px-outline_badge-13.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=61\",\n        \"wp_slug\": \"newsletter-cf7\\/cf7.php\"\n    },\n    {\n        \"id\": \"83\",\n        \"children_fileid\": null,\n        \"version\": \"1.0.3\",\n        \"title\": \"Ninja Forms Integration\",\n        \"description\": \"Integrate Ninja Forms with Newsletter collecting subscription from your contact form.\",\n        \"slug\": \"newsletter-ninjaforms\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/10\\/forms-integration.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=83\",\n        \"wp_slug\": \"newsletter-ninjaforms\\/ninjaforms.php\"\n    },\n    {\n        \"id\": \"84\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.3\",\n        \"title\": \"WP Forms Integration\",\n        \"description\": \"Integration with WP-Forms plugin. You can add a subscription option to your contact forms.\",\n        \"slug\": \"newsletter-wpforms\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/10\\/forms-integration.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=84\",\n        \"wp_slug\": \"newsletter-wpforms\\/wpforms.php\"\n    },\n    {\n        \"id\": \"50\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.3.7\",\n        \"title\": \"Reports\",\n        \"description\": \"Shows tables and diagrams of the collected data (opens, clicks, ...).\",\n        \"slug\": \"newsletter-reports\",\n        \"type\": \"extension\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/reports\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/business-32px-outline_chart-bar-33.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=50\",\n        \"wp_slug\": \"newsletter-reports\\/reports.php\"\n    },\n    {\n        \"id\": \"62\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.4.7\",\n        \"title\": \"Automated\",\n        \"description\": \"Automatically creates periodic newsletters with your blog contents. Multichannel.\",\n        \"slug\": \"newsletter-automated\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/automated\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/ui-32px-outline-2_time-countdown.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=62\",\n        \"wp_slug\": \"newsletter-automated\\/automated.php\"\n    },\n    {\n        \"id\": \"63\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.5.2\",\n        \"title\": \"WooCommerce\",\n        \"description\": \"The Newsletter Plugin integration for WooCommerce\\u2122. Unleash your marketing powers.\",\n        \"slug\": \"newsletter-woocommerce\",\n        \"type\": \"integration\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/woocommerce\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/03\\/woocommerce-extension-icon.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=63\",\n        \"wp_slug\": \"newsletter-woocommerce\\/woocommerce.php\"\n    },\n    {\n        \"id\": \"67\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.1.8\",\n        \"title\": \"Leads\",\n        \"description\": \"Add a popup or a fixed subscription bar to your website and offer your visitors a simple way to subscribe.\",\n        \"slug\": \"newsletter-leads\",\n        \"type\": \"extension\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/leads-extension\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/ui-32px-outline-3_widget.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=67\",\n        \"wp_slug\": \"newsletter-leads\\/leads.php\"\n    },\n    {\n        \"id\": \"68\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.1.2\",\n        \"title\": \"Google Analytics\",\n        \"description\": \"Automatically add Google Analytics UTM campaign tracking to links\",\n        \"slug\": \"newsletter-analytics\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/google-analytics\",\n        \"image\": \" https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/analytics.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=68\",\n        \"wp_slug\": \"newsletter-analytics\\/analytics.php\"\n    },\n    {\n        \"id\": \"70\",\n        \"children_fileid\": null,\n        \"version\": \"1.0.7\",\n        \"title\": \"Subscribe on Comment\",\n        \"description\": \"Adds the subscription option to your blog comment form\",\n        \"slug\": \"newsletter-comments\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/02\\/comment-notification.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=70\",\n        \"wp_slug\": \"newsletter-comments\\/comments.php\"\n    },\n    {\n        \"id\": \"72\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.2.8\",\n        \"title\": \"Autoresponder\",\n        \"description\": \"Create unlimited email series to follow-up your subscribers. Lessons, up-sells, conversations.\",\n        \"slug\": \"newsletter-autoresponder\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/autoresponder\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/emoticons-32px-outline_robot.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=72\",\n        \"wp_slug\": \"newsletter-autoresponder\\/autoresponder.php\"\n    },\n    {\n        \"id\": \"74\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.2.8\",\n        \"title\": \"Extended Composer Blocks\",\n        \"description\": \"Adds new blocks to the newsletter composer: list, video, gallery, full post.\",\n        \"slug\": \"newsletter-blocks\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/04\\/ui-32px-outline-3_widget.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=74\",\n        \"wp_slug\": \"newsletter-blocks\\/blocks.php\"\n    },\n    {\n        \"id\": \"75\",\n        \"children_fileid\": null,\n        \"version\": \"1.1.0\",\n        \"title\": \"Geolocation\",\n        \"description\": \"Geolocate the subscribers and target them by geolocation in your campaign.\",\n        \"slug\": \"newsletter-geo\",\n        \"type\": \"extension\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/03\\/geo-extension-icon.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=75\",\n        \"wp_slug\": \"newsletter-geo\\/geo.php\"\n    },\n    {\n        \"id\": \"77\",\n        \"children_fileid\": \"\",\n        \"version\": \"2.1.0\",\n        \"title\": \"Newsletter API\",\n        \"description\": \"Access programmatically to The Newsletter Plugin via REST calls.\",\n        \"slug\": \"newsletter-api\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/10\\/bold-direction@2x-1.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=77\",\n        \"wp_slug\": \"newsletter-api\\/api.php\"\n    },\n    {\n        \"id\": \"55\",\n        \"children_fileid\": null,\n        \"version\": \"4.0.9\",\n        \"title\": \"Facebook\",\n        \"description\": \"One click subscription and confirmation with Facebook Connect.\",\n        \"slug\": \"newsletter-facebook\",\n        \"type\": \"integration\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/facebook-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/Facebook.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=55\",\n        \"wp_slug\": \"newsletter-facebook\\/facebook.php\"\n    },\n    {\n        \"id\": \"76\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.1.2\",\n        \"title\": \"Bounce Management\",\n        \"description\": \"This experimental extension manages the bounces and keeps the list clean of invalid addresses.\",\n        \"slug\": \"newsletter-bounce\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/10\\/ic_settings_backup_restore_32px.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=76\",\n        \"wp_slug\": \"newsletter-bounce\\/bounce.php\"\n    },\n    {\n        \"id\": \"79\",\n        \"children_fileid\": null,\n        \"version\": \"1.0.9\",\n        \"title\": \"Events Manager Integration\",\n        \"description\": \"Integrates with Events Manager plugin to add events in your regular and automated newsletters.\",\n        \"slug\": \"newsletter-events\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2019\\/02\\/events-manager-icon.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=79\",\n        \"wp_slug\": \"newsletter-events\\/events.php\"\n    },\n    {\n        \"id\": \"82\",\n        \"children_fileid\": null,\n        \"version\": \"1.0.0\",\n        \"title\": \"Translatepress Bridge\",\n        \"description\": \"Enables few multilanguage Newsletter features for who is using Translatepress.\",\n        \"slug\": \"newsletter-translatepress\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2018\\/09\\/translatepress.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=82\",\n        \"wp_slug\": \"newsletter-translatepress\\/translatepress.php\"\n    },\n    {\n        \"id\": \"86\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.1.0\",\n        \"title\": \"Advanced Import\",\n        \"description\": \"An advanced import system with extended profile fields and mapping (beta version).\",\n        \"slug\": \"newsletter-import\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/file-upload-88.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=86\",\n        \"wp_slug\": \"newsletter-import\\/import.php\"\n    },\n    {\n        \"id\": \"88\",\n        \"children_fileid\": null,\n        \"version\": \"1.1.1\",\n        \"title\": \"The Events Calendar (by Tribe)\",\n        \"description\": \"Adds a composer block which extracts the events managed by The Events Calendar plugin.\",\n        \"slug\": \"newsletter-tribeevents\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/documentation\\/tribeevents-extension\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2019\\/02\\/tribe-event-calendar-icon.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=88\",\n        \"wp_slug\": \"newsletter-tribeevents\\/tribeevents.php\"\n    },\n    {\n        \"id\": \"58\",\n        \"children_fileid\": null,\n        \"version\": \"4.0.3\",\n        \"title\": \"Archive\",\n        \"description\": \"Generates a public archive of the sent newsletters for your blog.\",\n        \"slug\": \"newsletter-archive\",\n        \"type\": \"extension\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/newsletter-archive-extension\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/files-32px-outline_archive-3d-content.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=58\",\n        \"wp_slug\": \"newsletter-archive\\/archive.php\"\n    },\n    {\n        \"id\": \"71\",\n        \"children_fileid\": null,\n        \"version\": \"1.0.7\",\n        \"title\": \"Locked Content\",\n        \"description\": \"Boost your subscription rate locking out your premium contents with a subscription form.\",\n        \"slug\": \"newsletter-lock\",\n        \"type\": \"extension\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/04\\/ui-32px-outline-1_lock-open.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=71\",\n        \"wp_slug\": \"newsletter-lock\\/lock.php\"\n    },\n    {\n        \"id\": \"73\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.2.4\",\n        \"title\": \"WP Users Integration\",\n        \"description\": \"Connects the WordPress user registration with Newsletter subscription. Optionally imports all WP users as subscribers.\",\n        \"slug\": \"newsletter-wpusers\",\n        \"type\": \"integration\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/account\",\n        \"image\": \"https:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/uploads\\/2017\\/04\\/media-32px-outline-2_speaker-01.png\",\n        \"status\": \"3\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=73\",\n        \"wp_slug\": \"newsletter-wpusers\\/wpusers.php\"\n    },\n    {\n        \"id\": \"51\",\n        \"children_fileid\": null,\n        \"version\": \"4.1.1\",\n        \"title\": \"Feed by Mail\",\n        \"description\": \"Automatically creates and sends newsletters with the latest blog posts.\",\n        \"slug\": \"newsletter-feed\",\n        \"type\": \"legacy\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/feed-by-mail-extension\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/ui-32px-outline-3_playlist.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=51\",\n        \"wp_slug\": \"newsletter-feed\\/feed.php\"\n    },\n    {\n        \"id\": \"53\",\n        \"children_fileid\": null,\n        \"version\": \"2.2.0\",\n        \"title\": \"Popup\",\n        \"description\": \"Configurable popup system to increase the subscription rate.\",\n        \"slug\": \"newsletter-popup\",\n        \"type\": \"legacy\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/popup-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/ui-32px-outline-3_widget.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=53\",\n        \"wp_slug\": \"newsletter-popup\\/popup.php\"\n    },\n    {\n        \"id\": \"54\",\n        \"children_fileid\": null,\n        \"version\": \"4.1.1\",\n        \"title\": \"Followup\",\n        \"description\": \"Automated email series sent upon subscription at defined intervals.\",\n        \"slug\": \"newsletter-followup\",\n        \"type\": \"legacy\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/follow-up-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/ui-32px-outline-2_time-countdown.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=54\",\n        \"wp_slug\": \"newsletter-followup\\/followup.php\"\n    },\n    {\n        \"id\": \"48\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.2.1\",\n        \"title\": \"SendGrid\",\n        \"description\": \"Integrates the SendGrid delivery system and bounce detection.\",\n        \"slug\": \"newsletter-sendgrid\",\n        \"type\": \"delivery\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/sendgrid-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=48\",\n        \"wp_slug\": \"newsletter-sendgrid\\/sendgrid.php\"\n    },\n    {\n        \"id\": \"49\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.0.0\",\n        \"title\": \"Mandrill\",\n        \"description\": \"Integrates the Mandrill delivery system and bounce detection.\",\n        \"slug\": \"newsletter-mandrill\",\n        \"type\": \"legacy\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/mandrill-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=49\",\n        \"wp_slug\": \"newsletter-mandrill\\/mandrill.php\"\n    },\n    {\n        \"id\": \"52\",\n        \"children_fileid\": \"\",\n        \"version\": \"4.0.8\",\n        \"title\": \"Mailjet\",\n        \"description\": \"Integrates the Mailjet delivery system and bounce detection.\",\n        \"slug\": \"newsletter-mailjet\",\n        \"type\": \"delivery\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/mailjet-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=52\",\n        \"wp_slug\": \"newsletter-mailjet\\/mailjet.php\"\n    },\n    {\n        \"id\": \"60\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.2.8\",\n        \"title\": \"Amazon SES\",\n        \"description\": \"Integrates Newsletter with Amazon SES service for sending emails and processing bounces.\",\n        \"slug\": \"newsletter-amazon\",\n        \"type\": \"delivery\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/amazon-ses-extension\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=60\",\n        \"wp_slug\": \"newsletter-amazon\\/amazon.php\"\n    },\n    {\n        \"id\": \"65\",\n        \"children_fileid\": null,\n        \"version\": \"4.1.0\",\n        \"title\": \"Mailgun\",\n        \"description\": \"Integrates the Mailgun delivery system and bounce detection.\",\n        \"slug\": \"newsletter-mailgun\",\n        \"type\": \"delivery\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/mailgun-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=65\",\n        \"wp_slug\": \"newsletter-mailgun\\/mailgun.php\"\n    },\n    {\n        \"id\": \"66\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.0.9\",\n        \"title\": \"ElasticEmail\",\n        \"description\": \"ElasticEmail integration\",\n        \"slug\": \"newsletter-elasticemail\",\n        \"type\": \"delivery\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=66\",\n        \"wp_slug\": \"newsletter-elasticemail\\/elasticemail.php\"\n    },\n    {\n        \"id\": \"69\",\n        \"children_fileid\": \"\",\n        \"version\": \"1.1.7\",\n        \"title\": \"SparkPost\",\n        \"description\": \"Integrates Newsletter with the SparkPost mail delivery service and bounce detection.\",\n        \"slug\": \"newsletter-sparkpost\",\n        \"type\": \"delivery\",\n        \"url\": \"https:\\/\\/www.thenewsletterplugin.com\\/premium\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/design-32px-outline_newsletter-dev.png\",\n        \"status\": \"2\",\n        \"free\": false,\n        \"downloadable\": false,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=69\",\n        \"wp_slug\": \"newsletter-sparkpost\\/sparkpost.php\"\n    },\n    {\n        \"id\": \"56\",\n        \"children_fileid\": \"\",\n        \"version\": \"2.2.0\",\n        \"title\": \"Grabber\",\n        \"description\": \"Experimental! General subscription grabber from other forms. Requires technical skills.\",\n        \"slug\": \"newsletter-grabber\",\n        \"type\": \"legacy\",\n        \"url\": \"http:\\/\\/www.thenewsletterplugin.com\\/plugins\\/newsletter\\/grabber-module\",\n        \"image\": \"https:\\/\\/cdn.thenewsletterplugin.com\\/extensions\\/placeholder.png\",\n        \"status\": \"4\",\n        \"free\": true,\n        \"downloadable\": true,\n        \"download_url\": \"http:\\/\\/www.thenewsletterplugin.com\\/wp-content\\/plugins\\/file-commerce-pro\\/get.php?f=56\",\n        \"wp_slug\": \"newsletter-grabber\\/grabber.php\"\n    }\n]', 'no'),
(2895, '_site_transient_timeout_php_check_a4e7a3af7060c530d791075f6e3eb5fa', '1603802115', 'no'),
(2896, '_site_transient_php_check_a4e7a3af7060c530d791075f6e3eb5fa', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2899, '_site_transient_timeout_browser_44a5e524f134e3228c7b0b16c2224ffc', '1603802227', 'no'),
(2900, '_site_transient_browser_44a5e524f134e3228c7b0b16c2224ffc', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"86.0.4240.75\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(2914, '_transient_timeout_wp-smush-conflict_check', '1603363061', 'no'),
(2915, '_transient_wp-smush-conflict_check', 'a:0:{}', 'no'),
(2959, 'emr_news', '1', 'yes'),
(2989, '_transient_timeout_el_addon_data', '1603401130', 'no'),
(2990, '_transient_el_addon_data', 'a:2:{s:8:\"products\";a:5:{i:0;a:3:{s:4:\"info\";a:14:{s:2:\"id\";i:18;s:4:\"slug\";s:11:\"more-fields\";s:5:\"title\";s:11:\"More Fields\";s:11:\"create_date\";s:19:\"2017-03-07 11:50:43\";s:13:\"modified_date\";s:19:\"2020-07-05 11:31:48\";s:6:\"status\";s:7:\"publish\";s:4:\"link\";s:46:\"http://wpemaillog.com/?post_type=download&p=18\";s:7:\"content\";s:409:\"More Fields add-on shows additional fields about the email in the email log list page. The following are the additional fields that are added by this add-on.\r\n<ul>\r\n 	<li>From</li>\r\n 	<li>CC</li>\r\n 	<li>BCC</li>\r\n 	<li>Reply To</li>\r\n 	<li>Attachment</li>\r\n</ul>\r\nThese additional fields will allow you to see more information about the logged email.\r\n<h3>Screenshots</h3>\r\n[gallery columns=\"2\" ids=\"974,975\"]\";s:7:\"excerpt\";s:110:\"More Fields add-on shows additional fields in the email log page like From, CC, BCC, Reply To, Attachment etc.\";s:9:\"thumbnail\";s:75:\"https://wpemaillog.com/wp-content/uploads/edd/2016/11/more-fields-addon.png\";s:8:\"category\";a:1:{i:0;a:10:{s:7:\"term_id\";i:4;s:4:\"name\";s:5:\"Addon\";s:4:\"slug\";s:5:\"addon\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:8:\"taxonomy\";s:17:\"download_category\";s:11:\"description\";s:0:\"\";s:6:\"parent\";i:0;s:5:\"count\";i:5;s:6:\"filter\";s:3:\"raw\";}}s:4:\"tags\";b:0;s:9:\"permalink\";s:42:\"https://wpemaillog.com/addons/more-fields/\";s:7:\"version\";s:5:\"2.1.0\";}s:7:\"pricing\";a:3:{s:10:\"singlesite\";s:5:\"19.00\";s:8:\"2-5sites\";s:5:\"37.00\";s:14:\"unlimitedsites\";s:5:\"49.00\";}s:9:\"licensing\";a:4:{s:7:\"enabled\";b:1;s:7:\"version\";s:5:\"2.1.0\";s:8:\"exp_unit\";s:5:\"years\";s:10:\"exp_length\";s:1:\"1\";}}i:1;a:3:{s:4:\"info\";a:14:{s:2:\"id\";i:16;s:4:\"slug\";s:12:\"resend-email\";s:5:\"title\";s:12:\"Resend Email\";s:11:\"create_date\";s:19:\"2017-03-07 11:40:25\";s:13:\"modified_date\";s:19:\"2020-05-22 13:44:15\";s:6:\"status\";s:7:\"publish\";s:4:\"link\";s:46:\"http://wpemaillog.com/?post_type=download&p=16\";s:7:\"content\";s:369:\"Resend Email add-on allows you to resend the entire email directly from the email log. Before re-sending the email, this add-on allows you to modify the different fields before re-sending the email.\r\n\r\nYou can also resend all emails or only selected emails in bulk in addition to individually re-sending them.\r\n<h3>Screenshots</h3>\r\n[gallery ids=\"980,981,982,983,984\"]\";s:7:\"excerpt\";s:156:\"Resend Email add-on allows you to resend the entire email directly from the email log. You can also modify the different fields before re-sending the email.\";s:9:\"thumbnail\";s:76:\"https://wpemaillog.com/wp-content/uploads/edd/2016/11/resend-email-addon.png\";s:8:\"category\";a:1:{i:0;a:10:{s:7:\"term_id\";i:4;s:4:\"name\";s:5:\"Addon\";s:4:\"slug\";s:5:\"addon\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:8:\"taxonomy\";s:17:\"download_category\";s:11:\"description\";s:0:\"\";s:6:\"parent\";i:0;s:5:\"count\";i:5;s:6:\"filter\";s:3:\"raw\";}}s:4:\"tags\";b:0;s:9:\"permalink\";s:43:\"https://wpemaillog.com/addons/resend-email/\";s:7:\"version\";s:5:\"2.2.0\";}s:7:\"pricing\";a:3:{s:10:\"singlesite\";s:5:\"19.00\";s:8:\"2-5sites\";s:5:\"37.00\";s:14:\"unlimitedsites\";s:5:\"49.00\";}s:9:\"licensing\";a:4:{s:7:\"enabled\";b:1;s:7:\"version\";s:5:\"2.2.0\";s:8:\"exp_unit\";s:5:\"years\";s:10:\"exp_length\";s:1:\"1\";}}i:2;a:3:{s:4:\"info\";a:14:{s:2:\"id\";i:311;s:4:\"slug\";s:16:\"auto-delete-logs\";s:5:\"title\";s:16:\"Auto Delete Logs\";s:11:\"create_date\";s:19:\"2017-03-07 11:30:56\";s:13:\"modified_date\";s:19:\"2020-07-05 11:33:41\";s:6:\"status\";s:7:\"publish\";s:4:\"link\";s:48:\"https://wpemaillog.com/?post_type=download&p=311\";s:7:\"content\";s:154:\"The Auto Delete Logs add-on allows you to automatically delete logs based on a schedule.\r\n<h3>Screenshots</h3>\r\n[gallery columns=\"2\" ids=\"791182, 791183\"]\";s:7:\"excerpt\";s:88:\"The Auto Delete Logs add-on allows you to automatically delete logs based on a schedule.\";s:9:\"thumbnail\";s:75:\"https://wpemaillog.com/wp-content/uploads/edd/2017/03/delete-logs-addon.png\";s:8:\"category\";a:1:{i:0;a:10:{s:7:\"term_id\";i:4;s:4:\"name\";s:5:\"Addon\";s:4:\"slug\";s:5:\"addon\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:8:\"taxonomy\";s:17:\"download_category\";s:11:\"description\";s:0:\"\";s:6:\"parent\";i:0;s:5:\"count\";i:5;s:6:\"filter\";s:3:\"raw\";}}s:4:\"tags\";b:0;s:9:\"permalink\";s:47:\"https://wpemaillog.com/addons/auto-delete-logs/\";s:7:\"version\";s:5:\"1.1.1\";}s:7:\"pricing\";a:3:{s:10:\"singlesite\";s:5:\"19.00\";s:8:\"2-5sites\";s:5:\"37.00\";s:14:\"unlimitedsites\";s:5:\"49.00\";}s:9:\"licensing\";a:4:{s:7:\"enabled\";b:1;s:7:\"version\";s:5:\"1.1.1\";s:8:\"exp_unit\";s:5:\"years\";s:10:\"exp_length\";s:1:\"1\";}}i:3;a:3:{s:4:\"info\";a:14:{s:2:\"id\";i:20;s:4:\"slug\";s:13:\"forward-email\";s:5:\"title\";s:13:\"Forward Email\";s:11:\"create_date\";s:19:\"2017-03-07 11:30:56\";s:13:\"modified_date\";s:19:\"2020-05-27 13:21:45\";s:6:\"status\";s:7:\"publish\";s:4:\"link\";s:46:\"http://wpemaillog.com/?post_type=download&p=20\";s:7:\"content\";s:358:\"Forward Email add-on allows you to send a copy of all the emails send from WordPress, to another email address. The add-on allows you to choose whether you want to forward through to, cc or bcc fields. This can be extremely useful when you want to debug by analyzing the emails that are sent from WordPress.\r\n<h3>Screenshots</h3>\r\n[gallery ids=\"987,988,989\"]\";s:7:\"excerpt\";s:110:\"Forward Email add-on allows you to send a copy of all the emails send from WordPress, to another email address\";s:9:\"thumbnail\";s:77:\"https://wpemaillog.com/wp-content/uploads/edd/2016/11/forward-email-addon.png\";s:8:\"category\";a:1:{i:0;a:10:{s:7:\"term_id\";i:4;s:4:\"name\";s:5:\"Addon\";s:4:\"slug\";s:5:\"addon\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:8:\"taxonomy\";s:17:\"download_category\";s:11:\"description\";s:0:\"\";s:6:\"parent\";i:0;s:5:\"count\";i:5;s:6:\"filter\";s:3:\"raw\";}}s:4:\"tags\";b:0;s:9:\"permalink\";s:44:\"https://wpemaillog.com/addons/forward-email/\";s:7:\"version\";s:5:\"2.0.2\";}s:7:\"pricing\";a:3:{s:10:\"singlesite\";s:5:\"19.00\";s:8:\"2-5sites\";s:5:\"37.00\";s:14:\"unlimitedsites\";s:5:\"49.00\";}s:9:\"licensing\";a:4:{s:7:\"enabled\";b:1;s:7:\"version\";s:5:\"2.0.2\";s:8:\"exp_unit\";s:5:\"years\";s:10:\"exp_length\";s:1:\"1\";}}i:4;a:3:{s:4:\"info\";a:14:{s:2:\"id\";i:308;s:4:\"slug\";s:11:\"export-logs\";s:5:\"title\";s:11:\"Export Logs\";s:11:\"create_date\";s:19:\"2017-03-07 11:20:12\";s:13:\"modified_date\";s:19:\"2020-07-05 11:34:53\";s:6:\"status\";s:7:\"publish\";s:4:\"link\";s:48:\"https://wpemaillog.com/?post_type=download&p=308\";s:7:\"content\";s:215:\"Export Logs add-on allows you to export the logged email logs as a csv file. The exported logs can be used for further processing or for record keeping.\r\n<h3>Screenshots</h3>\r\n[gallery ids=\"965,966,967,968, 791221\"]\";s:7:\"excerpt\";s:103:\"Export Logs add-on allows you to export the logged email logs for further processing or record keeping.\";s:9:\"thumbnail\";s:75:\"https://wpemaillog.com/wp-content/uploads/edd/2017/03/export-logs-addon.png\";s:8:\"category\";a:1:{i:0;a:10:{s:7:\"term_id\";i:4;s:4:\"name\";s:5:\"Addon\";s:4:\"slug\";s:5:\"addon\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:8:\"taxonomy\";s:17:\"download_category\";s:11:\"description\";s:0:\"\";s:6:\"parent\";i:0;s:5:\"count\";i:5;s:6:\"filter\";s:3:\"raw\";}}s:4:\"tags\";b:0;s:9:\"permalink\";s:42:\"https://wpemaillog.com/addons/export-logs/\";s:7:\"version\";s:5:\"1.3.0\";}s:7:\"pricing\";a:3:{s:10:\"singlesite\";s:5:\"19.00\";s:8:\"2-5sites\";s:5:\"37.00\";s:14:\"unlimitedsites\";s:5:\"49.00\";}s:9:\"licensing\";a:4:{s:7:\"enabled\";b:1;s:7:\"version\";s:5:\"1.3.0\";s:8:\"exp_unit\";s:5:\"years\";s:10:\"exp_length\";s:1:\"1\";}}}s:13:\"request_speed\";d:0.0055980682373046875;}', 'no'),
(2994, '_site_transient_timeout_theme_roots', '1603359742', 'no'),
(2995, '_site_transient_theme_roots', 'a:4:{s:16:\"themeplate-child\";s:7:\"/themes\";s:10:\"themeplate\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no'),
(2998, '_transient_timeout_wpseo_total_unindexed_posts', '1603444390', 'no'),
(2999, '_transient_wpseo_total_unindexed_posts', '38', 'no'),
(3000, '_transient_timeout_wpseo_total_unindexed_terms', '1603444390', 'no'),
(3001, '_transient_wpseo_total_unindexed_terms', '1', 'no'),
(3002, '_transient_timeout_wpseo_total_unindexed_post_type_archives', '1603444390', 'no'),
(3003, '_transient_wpseo_total_unindexed_post_type_archives', '1', 'no'),
(3004, '_transient_timeout_wpseo_unindexed_post_link_count', '1603444390', 'no'),
(3005, '_transient_wpseo_unindexed_post_link_count', '16', 'no'),
(3006, '_transient_timeout_wpseo_unindexed_term_link_count', '1603444390', 'no'),
(3007, '_transient_wpseo_unindexed_term_link_count', '1', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(3009, 'rewrite_rules', 'a:151:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:11:\"our_team/?$\";s:28:\"index.php?post_type=our_team\";s:41:\"our_team/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=our_team&feed=$matches[1]\";s:36:\"our_team/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=our_team&feed=$matches[1]\";s:28:\"our_team/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=our_team&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:34:\"our_team/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"our_team/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"our_team/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"our_team/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"our_team/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"our_team/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"our_team/(.+?)/embed/?$\";s:41:\"index.php?our_team=$matches[1]&embed=true\";s:27:\"our_team/(.+?)/trackback/?$\";s:35:\"index.php?our_team=$matches[1]&tb=1\";s:47:\"our_team/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?our_team=$matches[1]&feed=$matches[2]\";s:42:\"our_team/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?our_team=$matches[1]&feed=$matches[2]\";s:35:\"our_team/(.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?our_team=$matches[1]&paged=$matches[2]\";s:42:\"our_team/(.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?our_team=$matches[1]&cpage=$matches[2]\";s:31:\"our_team/(.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?our_team=$matches[1]&page=$matches[2]\";s:37:\"testimonial/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"testimonial/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"testimonial/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"testimonial/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"testimonial/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"testimonial/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"testimonial/(.+?)/embed/?$\";s:44:\"index.php?testimonial=$matches[1]&embed=true\";s:30:\"testimonial/(.+?)/trackback/?$\";s:38:\"index.php?testimonial=$matches[1]&tb=1\";s:38:\"testimonial/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?testimonial=$matches[1]&paged=$matches[2]\";s:45:\"testimonial/(.+?)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?testimonial=$matches[1]&cpage=$matches[2]\";s:34:\"testimonial/(.+?)(?:/([0-9]+))?/?$\";s:50:\"index.php?testimonial=$matches[1]&page=$matches[2]\";s:33:\"partner/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"partner/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"partner/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"partner/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"partner/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"partner/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"partner/(.+?)/embed/?$\";s:40:\"index.php?partner=$matches[1]&embed=true\";s:26:\"partner/(.+?)/trackback/?$\";s:34:\"index.php?partner=$matches[1]&tb=1\";s:34:\"partner/(.+?)/page/?([0-9]{1,})/?$\";s:47:\"index.php?partner=$matches[1]&paged=$matches[2]\";s:41:\"partner/(.+?)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?partner=$matches[1]&cpage=$matches[2]\";s:30:\"partner/(.+?)(?:/([0-9]+))?/?$\";s:46:\"index.php?partner=$matches[1]&page=$matches[2]\";s:32:\"gyik/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"gyik/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"gyik/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"gyik/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"gyik/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"gyik/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"gyik/([^/]+)/embed/?$\";s:37:\"index.php?gyik=$matches[1]&embed=true\";s:25:\"gyik/([^/]+)/trackback/?$\";s:31:\"index.php?gyik=$matches[1]&tb=1\";s:33:\"gyik/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?gyik=$matches[1]&paged=$matches[2]\";s:40:\"gyik/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?gyik=$matches[1]&cpage=$matches[2]\";s:29:\"gyik/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?gyik=$matches[1]&page=$matches[2]\";s:21:\"gyik/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"gyik/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"gyik/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"gyik/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"gyik/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"gyik/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(3015, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1603358082;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:35:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"animate-it/edsanimate.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/animate-it\";s:4:\"slug\";s:10:\"animate-it\";s:6:\"plugin\";s:25:\"animate-it/edsanimate.php\";s:11:\"new_version\";s:5:\"2.3.7\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/animate-it/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/animate-it.2.3.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/animate-it/assets/icon-256x256.png?rev=989356\";s:2:\"1x\";s:62:\"https://ps.w.org/animate-it/assets/icon-128x128.png?rev=989356\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/animate-it/assets/banner-1544x500.png?rev=988616\";s:2:\"1x\";s:64:\"https://ps.w.org/animate-it/assets/banner-772x250.png?rev=988616\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"wp-asset-clean-up/wpacu.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/wp-asset-clean-up\";s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:6:\"plugin\";s:27:\"wp-asset-clean-up/wpacu.php\";s:11:\"new_version\";s:7:\"1.3.7.1\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/wp-asset-clean-up/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wp-asset-clean-up.1.3.7.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-asset-clean-up/assets/icon-256x256.png?rev=1981952\";s:2:\"1x\";s:70:\"https://ps.w.org/wp-asset-clean-up/assets/icon-128x128.png?rev=1981952\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wp-asset-clean-up/assets/banner-772x250.png?rev=1986594\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"asset-queue-manager/asset-queue-manager.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/asset-queue-manager\";s:4:\"slug\";s:19:\"asset-queue-manager\";s:6:\"plugin\";s:43:\"asset-queue-manager/asset-queue-manager.php\";s:11:\"new_version\";s:5:\"1.0.3\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/asset-queue-manager/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/asset-queue-manager.1.0.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://s.w.org/plugins/geopattern-icon/asset-queue-manager.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:21:\"backwpup/backwpup.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/backwpup\";s:4:\"slug\";s:8:\"backwpup\";s:6:\"plugin\";s:21:\"backwpup/backwpup.php\";s:11:\"new_version\";s:5:\"3.8.0\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/backwpup/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/backwpup.3.8.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/backwpup/assets/icon-256x256.png?rev=1422084\";s:2:\"1x\";s:61:\"https://ps.w.org/backwpup/assets/icon-128x128.png?rev=1422084\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/backwpup/assets/banner-1544x500.png?rev=2340186\";s:2:\"1x\";s:63:\"https://ps.w.org/backwpup/assets/banner-772x250.png?rev=2340186\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"5.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.5.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.3\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"debug-bar/debug-bar.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/debug-bar\";s:4:\"slug\";s:9:\"debug-bar\";s:6:\"plugin\";s:23:\"debug-bar/debug-bar.php\";s:11:\"new_version\";s:5:\"1.0.1\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/debug-bar/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/debug-bar.1.0.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/debug-bar/assets/icon-256x256.png?rev=1908362\";s:2:\"1x\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";s:3:\"svg\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/debug-bar/assets/banner-1544x500.png?rev=1365496\";s:2:\"1x\";s:64:\"https://ps.w.org/debug-bar/assets/banner-772x250.png?rev=1365496\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"disable-comments/disable-comments.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/disable-comments\";s:4:\"slug\";s:16:\"disable-comments\";s:6:\"plugin\";s:37:\"disable-comments/disable-comments.php\";s:11:\"new_version\";s:6:\"1.11.0\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/disable-comments/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/disable-comments.1.11.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/disable-comments/assets/icon-256x256.png?rev=2365558\";s:2:\"1x\";s:69:\"https://ps.w.org/disable-comments/assets/icon-128x128.png?rev=2365562\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/disable-comments/assets/banner-1544x500.png?rev=2365680\";s:2:\"1x\";s:71:\"https://ps.w.org/disable-comments/assets/banner-772x250.png?rev=2365680\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"duplicate-page/duplicatepage.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-page\";s:4:\"slug\";s:14:\"duplicate-page\";s:6:\"plugin\";s:32:\"duplicate-page/duplicatepage.php\";s:11:\"new_version\";s:3:\"4.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-page/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/duplicate-page.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-page/assets/icon-128x128.jpg?rev=1412874\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-page/assets/banner-772x250.jpg?rev=1410328\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"easy-wp-smtp/easy-wp-smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/easy-wp-smtp\";s:4:\"slug\";s:12:\"easy-wp-smtp\";s:6:\"plugin\";s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:11:\"new_version\";s:5:\"1.4.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/easy-wp-smtp/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/easy-wp-smtp.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/easy-wp-smtp/assets/icon-128x128.png?rev=1242044\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/easy-wp-smtp/assets/banner-772x250.png?rev=1650323\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"email-log/email-log.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/email-log\";s:4:\"slug\";s:9:\"email-log\";s:6:\"plugin\";s:23:\"email-log/email-log.php\";s:11:\"new_version\";s:5:\"2.4.3\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/email-log/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/email-log.2.4.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/email-log/assets/icon-256x256.png?rev=1710920\";s:2:\"1x\";s:62:\"https://ps.w.org/email-log/assets/icon-128x128.png?rev=1710920\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/email-log/assets/banner-1544x500.png?rev=1708230\";s:2:\"1x\";s:64:\"https://ps.w.org/email-log/assets/banner-772x250.png?rev=1708230\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/email-log/assets/banner-1544x500-rtl.png?rev=1708230\";s:2:\"1x\";s:68:\"https://ps.w.org/email-log/assets/banner-772x250-rtl.png?rev=1708230\";}}s:45:\"enable-media-replace/enable-media-replace.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/enable-media-replace\";s:4:\"slug\";s:20:\"enable-media-replace\";s:6:\"plugin\";s:45:\"enable-media-replace/enable-media-replace.php\";s:11:\"new_version\";s:5:\"3.4.2\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/enable-media-replace/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/enable-media-replace.3.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/enable-media-replace/assets/icon-256x256.png?rev=1940728\";s:2:\"1x\";s:73:\"https://ps.w.org/enable-media-replace/assets/icon-128x128.png?rev=1940728\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/enable-media-replace/assets/banner-1544x500.png?rev=2322194\";s:2:\"1x\";s:75:\"https://ps.w.org/enable-media-replace/assets/banner-772x250.png?rev=2322194\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"fast-velocity-minify/fvm.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/fast-velocity-minify\";s:4:\"slug\";s:20:\"fast-velocity-minify\";s:6:\"plugin\";s:28:\"fast-velocity-minify/fvm.php\";s:11:\"new_version\";s:5:\"2.8.9\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/fast-velocity-minify/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/fast-velocity-minify.2.8.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/fast-velocity-minify/assets/icon-128x128.jpg?rev=1440946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:75:\"https://ps.w.org/fast-velocity-minify/assets/banner-772x250.jpg?rev=1440936\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"gdpr-cookie-compliance/moove-gdpr.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/gdpr-cookie-compliance\";s:4:\"slug\";s:22:\"gdpr-cookie-compliance\";s:6:\"plugin\";s:37:\"gdpr-cookie-compliance/moove-gdpr.php\";s:11:\"new_version\";s:5:\"4.3.5\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/gdpr-cookie-compliance/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/gdpr-cookie-compliance.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/gdpr-cookie-compliance/assets/icon-256x256.png?rev=2376316\";s:2:\"1x\";s:75:\"https://ps.w.org/gdpr-cookie-compliance/assets/icon-128x128.png?rev=2376316\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/gdpr-cookie-compliance/assets/banner-1544x500.png?rev=2376316\";s:2:\"1x\";s:77:\"https://ps.w.org/gdpr-cookie-compliance/assets/banner-772x250.png?rev=2376316\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"health-check/health-check.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/health-check\";s:4:\"slug\";s:12:\"health-check\";s:6:\"plugin\";s:29:\"health-check/health-check.php\";s:11:\"new_version\";s:5:\"1.4.5\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/health-check/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/health-check.1.4.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:65:\"https://ps.w.org/health-check/assets/icon-256x256.png?rev=1823210\";s:2:\"1x\";s:57:\"https://ps.w.org/health-check/assets/icon.svg?rev=1828244\";s:3:\"svg\";s:57:\"https://ps.w.org/health-check/assets/icon.svg?rev=1828244\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/health-check/assets/banner-1544x500.png?rev=1823210\";s:2:\"1x\";s:67:\"https://ps.w.org/health-check/assets/banner-772x250.png?rev=1823210\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"hello-dolly/hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:21:\"hello-dolly/hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:77:\"jquery-validation-for-contact-form-7/jquery-validation-for-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:50:\"w.org/plugins/jquery-validation-for-contact-form-7\";s:4:\"slug\";s:36:\"jquery-validation-for-contact-form-7\";s:6:\"plugin\";s:77:\"jquery-validation-for-contact-form-7/jquery-validation-for-contact-form-7.php\";s:11:\"new_version\";s:3:\"5.0\";s:3:\"url\";s:67:\"https://wordpress.org/plugins/jquery-validation-for-contact-form-7/\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/plugin/jquery-validation-for-contact-form-7.5.0.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:89:\"https://ps.w.org/jquery-validation-for-contact-form-7/assets/icon-128x128.png?rev=1113600\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:91:\"https://ps.w.org/jquery-validation-for-contact-form-7/assets/banner-772x250.png?rev=1113600\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"rocket-lazy-load/rocket-lazy-load.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/rocket-lazy-load\";s:4:\"slug\";s:16:\"rocket-lazy-load\";s:6:\"plugin\";s:37:\"rocket-lazy-load/rocket-lazy-load.php\";s:11:\"new_version\";s:5:\"2.3.4\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/rocket-lazy-load/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/rocket-lazy-load.2.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/rocket-lazy-load/assets/icon-256x256.png?rev=1776193\";s:2:\"1x\";s:69:\"https://ps.w.org/rocket-lazy-load/assets/icon-128x128.png?rev=1776193\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:71:\"https://ps.w.org/rocket-lazy-load/assets/banner-772x250.png?rev=1776193\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.4.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.4.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"newsletter/plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:6:\"plugin\";s:21:\"newsletter/plugin.php\";s:11:\"new_version\";s:5:\"6.9.3\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/newsletter/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/newsletter.6.9.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/newsletter/assets/icon-256x256.png?rev=1052028\";s:2:\"1x\";s:63:\"https://ps.w.org/newsletter/assets/icon-128x128.png?rev=1160467\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/newsletter/assets/banner-1544x500.png?rev=1052027\";s:2:\"1x\";s:65:\"https://ps.w.org/newsletter/assets/banner-772x250.png?rev=1052027\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"query-monitor/query-monitor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/query-monitor\";s:4:\"slug\";s:13:\"query-monitor\";s:6:\"plugin\";s:31:\"query-monitor/query-monitor.php\";s:11:\"new_version\";s:5:\"3.6.4\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/query-monitor/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/query-monitor.3.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/query-monitor/assets/icon-256x256.png?rev=2301273\";s:2:\"1x\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";s:3:\"svg\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/query-monitor/assets/banner-1544x500.png?rev=1629576\";s:2:\"1x\";s:68:\"https://ps.w.org/query-monitor/assets/banner-772x250.png?rev=2301273\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"show-current-template/show-current-template.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/show-current-template\";s:4:\"slug\";s:21:\"show-current-template\";s:6:\"plugin\";s:47:\"show-current-template/show-current-template.php\";s:11:\"new_version\";s:5:\"0.3.4\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/show-current-template/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/show-current-template.0.3.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:73:\"https://ps.w.org/show-current-template/assets/icon-256x256.png?rev=976031\";s:2:\"1x\";s:65:\"https://ps.w.org/show-current-template/assets/icon.svg?rev=976031\";s:3:\"svg\";s:65:\"https://ps.w.org/show-current-template/assets/icon.svg?rev=976031\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:35:\"simply-show-ids/simply-show-ids.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/simply-show-ids\";s:4:\"slug\";s:15:\"simply-show-ids\";s:6:\"plugin\";s:35:\"simply-show-ids/simply-show-ids.php\";s:11:\"new_version\";s:5:\"1.3.3\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/simply-show-ids/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/simply-show-ids.1.3.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:59:\"https://s.w.org/plugins/geopattern-icon/simply-show-ids.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:5:\"3.7.1\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/wp-smushit.3.7.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.gif?rev=2263432\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.gif?rev=2263431\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"svg-support/svg-support.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/svg-support\";s:4:\"slug\";s:11:\"svg-support\";s:6:\"plugin\";s:27:\"svg-support/svg-support.php\";s:11:\"new_version\";s:6:\"2.3.18\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/svg-support/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/svg-support.2.3.18.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:64:\"https://ps.w.org/svg-support/assets/icon-256x256.png?rev=1417738\";s:2:\"1x\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";s:3:\"svg\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/svg-support/assets/banner-1544x500.jpg?rev=1215377\";s:2:\"1x\";s:66:\"https://ps.w.org/svg-support/assets/banner-772x250.jpg?rev=1215377\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"theme-check/theme-check.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/theme-check\";s:4:\"slug\";s:11:\"theme-check\";s:6:\"plugin\";s:27:\"theme-check/theme-check.php\";s:11:\"new_version\";s:10:\"20200922.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/theme-check/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/theme-check.20200922.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/theme-check/assets/icon-128x128.png?rev=972579\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/theme-check/assets/banner-1544x500.png?rev=904294\";s:2:\"1x\";s:65:\"https://ps.w.org/theme-check/assets/banner-772x250.png?rev=904294\";}s:11:\"banners_rtl\";a:0:{}}s:53:\"webp-converter-for-media/webp-converter-for-media.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:38:\"w.org/plugins/webp-converter-for-media\";s:4:\"slug\";s:24:\"webp-converter-for-media\";s:6:\"plugin\";s:53:\"webp-converter-for-media/webp-converter-for-media.php\";s:11:\"new_version\";s:5:\"1.4.5\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/webp-converter-for-media/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/webp-converter-for-media.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/webp-converter-for-media/assets/icon-256x256.png?rev=2107059\";s:2:\"1x\";s:77:\"https://ps.w.org/webp-converter-for-media/assets/icon-128x128.png?rev=2107059\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/webp-converter-for-media/assets/banner-1544x500.png?rev=2107032\";s:2:\"1x\";s:79:\"https://ps.w.org/webp-converter-for-media/assets/banner-772x250.png?rev=2107032\";}s:11:\"banners_rtl\";a:0:{}}s:22:\"honeypot/wp-armour.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/honeypot\";s:4:\"slug\";s:8:\"honeypot\";s:6:\"plugin\";s:22:\"honeypot/wp-armour.php\";s:11:\"new_version\";s:5:\"1.5.4\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/honeypot/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/honeypot.1.5.4.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/honeypot/assets/icon-128x128.png?rev=2328451\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/honeypot/assets/banner-772x250.png?rev=2325223\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-cerber/wp-cerber.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/wp-cerber\";s:4:\"slug\";s:9:\"wp-cerber\";s:6:\"plugin\";s:23:\"wp-cerber/wp-cerber.php\";s:11:\"new_version\";s:5:\"8.6.7\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/wp-cerber/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wp-cerber.8.6.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/wp-cerber/assets/icon-256x256.png?rev=2344645\";s:2:\"1x\";s:62:\"https://ps.w.org/wp-cerber/assets/icon-128x128.png?rev=2359065\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-cerber/assets/banner-1544x500.png?rev=2344628\";s:2:\"1x\";s:64:\"https://ps.w.org/wp-cerber/assets/banner-772x250.png?rev=2344628\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"wp-migrate-db/wp-migrate-db.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wp-migrate-db\";s:4:\"slug\";s:13:\"wp-migrate-db\";s:6:\"plugin\";s:31:\"wp-migrate-db/wp-migrate-db.php\";s:11:\"new_version\";s:6:\"1.0.16\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wp-migrate-db/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-migrate-db.1.0.16.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-256x256.jpg?rev=1809889\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-128x128.jpg?rev=1809889\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wp-migrate-db/assets/banner-1544x500.jpg?rev=1809889\";s:2:\"1x\";s:68:\"https://ps.w.org/wp-migrate-db/assets/banner-772x250.jpg?rev=1809889\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:6:\"15.1.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wordpress-seo.15.1.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(3016, '_transient_timeout_yoast_i18n_wordpress-seo_hu_HU', '1603445181', 'no'),
(3017, '_transient_yoast_i18n_wordpress-seo_hu_HU', 'O:8:\"stdClass\":14:{s:2:\"id\";i:396164;s:4:\"name\";s:9:\"Hungarian\";s:4:\"slug\";s:7:\"default\";s:10:\"project_id\";i:3158;s:6:\"locale\";s:2:\"hu\";s:13:\"current_count\";i:1046;s:18:\"untranslated_count\";i:107;s:13:\"waiting_count\";i:71;s:11:\"fuzzy_count\";i:16;s:9:\"all_count\";i:1233;s:14:\"warnings_count\";i:0;s:18:\"percent_translated\";i:84;s:9:\"wp_locale\";s:5:\"hu_HU\";s:13:\"last_modified\";s:19:\"2020-08-19 18:02:52\";}', 'no');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(4, 5, '_form', '<div class=\"row contact-form\">\n\n <div class=\"col-lg-12 name\"> \n  <label> Telefonszám:\n  [tel* tel]</label>\n</div>\n\n<div class=\"col-lg-12 email\">\n <label> E-mail cím:\n  [email* email]</label>\n</div>\n\n<div class=\"col-lg-12 location\">\n  <label> Település:\n  [text* location]</label>\n</div>\n\n<div class=\"col-lg-12 height\">\n  <label> Kerítésmagasság:\n  [text* height]</label>\n</div>\n\n<div class=\"col-lg-12 length\">\n  <label> Kerítendő szakasz hossza:\n  [text* length]</label>\n</div>\n\n<div class=\"col-lg-12 image-of-location\">\n  <label> Kép csatolása a területről\n  [file image-of-location limit:10]</label>\n</div>\n\n<div class=\"col-lg-12 built-in\">\n   Beépítést kér?\n  [radio built-in default:1 \"Kérek\" \"Nem kérek\"]\n</div>\n\n<div class=\"col-lg-12 soil\">\n  <label> Talaj jellemzői:\n  [text* soil]</label>\n</div>\n\n<div class=\"col-lg-12 message\">\n  <label> Egyéb megjegyzés:\n  [textarea message placeholder \"Egyéb kiegészítő információk\"]</label>\n</div>\n\n<div class=\"col-lg-12 image-of-element\">\n  <label> Kép csatolása a kedvelt elemekről\n  [file image-of-item limit:10]</label>\n</div>\n\n<div class=\"col-lg-12 privacy\">\n  [acceptance privacy] Elolvastam és elfogadom az <a href=\"#\" target=\"_blank\">Adatvédelmi Tájékoztatóban</a> leírtakat! [/acceptance]\n</div>\n\n<div class=\"col-lg-12 submit\">\n  [submit \"Üzenet küldése\"]\n</div>\n\n</div>'),
(5, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:34:\"Érdeklődés - Árajánlatkérés\";s:6:\"sender\";s:49:\"Fix Beton - Árajánlat <info@fixbetonkerites.hu>\";s:9:\"recipient\";s:16:\"gefusz@gmail.com\";s:4:\"body\";s:469:\"<h2>Fix Beton Árajánlatkérés</h2>\n\n<h4>Az érdeklődő elérhetőségei:</h4>\n<strong>Telefonszám:</strong> [tel]\n<strong>Email:</strong> [email]\n\n<h4>Az éppítendő kerítés adatai:</h4>\n<strong>Település:</strong> [location]\n<strong>Kerítésmagasság:</strong> [height]\n<strong>Kerítendő szakasz hossza:</strong> [length]\n\n<strong>Beépítést kér:</strong> [built-in]\n<strong>Talaj jellemzők:</strong> [soil]\n\n<h4>Leírás a projektről:</h4>\n[message]\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:34:\"[image-of-location][image-of-item]\";s:8:\"use_html\";b:1;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:18:\"Sikeres Beküldés\";s:6:\"sender\";s:15:\"admin@email.com\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:18:\"Sikeres Beküldés\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(7, 5, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:35:\"Nem megfelelő a dátum formátuma.\";s:14:\"date_too_early\";s:63:\"A dátum korábbi, mint az engedélyezett legkorábbi időpont.\";s:13:\"date_too_late\";s:43:\"A dátum az engedélyezett időpont utáni.\";s:13:\"upload_failed\";s:57:\"Ismeretlen eredetű hiba történt a fájlfeltöltéskor.\";s:24:\"upload_file_type_invalid\";s:56:\"Nem engedélyezett az ilyen típusú fájl feltöltése.\";s:21:\"upload_file_too_large\";s:26:\"Túl nagyméretű a fájl.\";s:23:\"upload_failed_php_error\";s:42:\"Hiba történt a fájlfeltöltés közben.\";s:14:\"invalid_number\";s:33:\"A szám formátuma érvénytelen.\";s:16:\"number_too_small\";s:53:\"A szám kisebb az engedélyezett legkisebb számnál.\";s:16:\"number_too_large\";s:55:\"A szám nagyobb az engedélyezett legnagyobb számnál.\";s:23:\"quiz_answer_not_correct\";s:39:\"A quiz-kérdésre adott válasz hibás.\";s:13:\"invalid_email\";s:37:\"A megadott e-mail cím érvénytelen.\";s:11:\"invalid_url\";s:25:\"Az URL Cím érvénytelen\";s:11:\"invalid_tel\";s:29:\"A telefonszám érvénytelen.\";}'),
(8, 5, '_additional_settings', ''),
(9, 5, '_locale', 'hu_HU'),
(37, 2, '_edit_lock', '1585059287:1'),
(38, 2, '_edit_last', '1'),
(39, 2, 'themeplate_field_range', '50'),
(41, 12, '_edit_lock', '1583846100:1'),
(42, 11, '_edit_lock', '1583846115:1'),
(43, 1, '_edit_lock', '1583846127:1'),
(44, 13, '_wp_attached_file', '2020/03/placeholder-img-3.jpg'),
(45, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:600;s:4:\"file\";s:29:\"2020/03/placeholder-img-3.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(46, 13, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:22.951845933345414;s:5:\"bytes\";i:27864;s:11:\"size_before\";i:121402;s:10:\"size_after\";i:93538;s:4:\"time\";d:0.06;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:3:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.2;s:5:\"bytes\";i:3486;s:11:\"size_before\";i:14405;s:10:\"size_after\";i:10919;s:4:\"time\";d:0.01;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:19.53;s:5:\"bytes\";i:554;s:11:\"size_before\";i:2837;s:10:\"size_after\";i:2283;s:4:\"time\";d:0.02;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:22.87;s:5:\"bytes\";i:23824;s:11:\"size_before\";i:104160;s:10:\"size_after\";i:80336;s:4:\"time\";d:0.03;}}}'),
(47, 12, '_edit_last', '1'),
(48, 12, '_thumbnail_id', '13'),
(50, 14, 'themeplate_field_range', '50'),
(51, 12, '_yoast_wpseo_content_score', '90'),
(52, 12, 'themeplate_field_range', '50'),
(53, 11, '_edit_last', '1'),
(54, 11, '_thumbnail_id', '13'),
(56, 15, 'themeplate_field_range', '50'),
(57, 11, '_yoast_wpseo_content_score', '90'),
(58, 11, 'themeplate_field_range', '50'),
(59, 1, '_edit_last', '1'),
(60, 1, '_thumbnail_id', '13'),
(62, 16, 'themeplate_field_range', '50'),
(63, 1, '_yoast_wpseo_content_score', '90'),
(64, 1, 'themeplate_field_range', '50'),
(65, 17, '_edit_last', '1'),
(66, 19, 'themeplate_field_range', '50'),
(67, 17, 'themeplate_field_range', '50'),
(68, 17, '_edit_lock', '1584007452:1'),
(69, 20, '_edit_last', '1'),
(70, 20, '_edit_lock', '1584006650:1'),
(71, 22, '_wp_attached_file', '2020/03/unknown-person.png'),
(72, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:450;s:6:\"height\";i:450;s:4:\"file\";s:26:\"2020/03/unknown-person.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"unknown-person-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"unknown-person-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(73, 22, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:25.739626227431106;s:5:\"bytes\";i:8126;s:11:\"size_before\";i:31570;s:10:\"size_after\";i:23444;s:4:\"time\";d:0.25;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.19;s:5:\"bytes\";i:5495;s:11:\"size_before\";i:22715;s:10:\"size_after\";i:17220;s:4:\"time\";d:0.16;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.71;s:5:\"bytes\";i:2631;s:11:\"size_before\";i:8855;s:10:\"size_after\";i:6224;s:4:\"time\";d:0.09;}}}'),
(74, 20, '_thumbnail_id', '22'),
(75, 20, 'titulus', 'Manager'),
(76, 20, '_titulus', 'field_5e6a023c16dca'),
(77, 20, 'themeplate_field_range', '50'),
(78, 20, '_yoast_wpseo_content_score', '60'),
(79, 23, '_edit_last', '1'),
(80, 23, '_edit_lock', '1584006072:1'),
(81, 23, '_thumbnail_id', '22'),
(82, 23, 'titulus', 'Manager'),
(83, 23, '_titulus', 'field_5e6a023c16dca'),
(84, 23, 'themeplate_field_range', '50'),
(85, 23, '_yoast_wpseo_content_score', '60'),
(86, 24, '_edit_last', '1'),
(87, 24, '_edit_lock', '1584006076:1'),
(88, 24, '_thumbnail_id', '22'),
(89, 24, 'titulus', 'Manager'),
(90, 24, '_titulus', 'field_5e6a023c16dca'),
(91, 24, 'themeplate_field_range', '50'),
(92, 24, '_yoast_wpseo_content_score', '60'),
(93, 25, '_edit_last', '1'),
(94, 25, '_edit_lock', '1584006084:1'),
(95, 25, '_thumbnail_id', '22'),
(96, 25, 'titulus', 'Manager'),
(97, 25, '_titulus', 'field_5e6a023c16dca'),
(98, 25, 'themeplate_field_range', '50'),
(99, 25, '_yoast_wpseo_content_score', '60'),
(100, 26, 'themeplate_field_range', '50'),
(101, 27, '_edit_last', '1'),
(102, 29, 'themeplate_field_range', '50'),
(103, 27, 'themeplate_field_range', '50'),
(104, 27, '_edit_lock', '1584007428:1'),
(105, 30, '_edit_last', '1'),
(106, 30, '_edit_lock', '1584007647:1'),
(107, 30, 'testimonial_titulus', 'CEO'),
(108, 30, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(109, 30, 'themeplate_field_range', '50'),
(110, 30, '_yoast_wpseo_content_score', '60'),
(111, 32, '_edit_last', '1'),
(112, 32, '_edit_lock', '1584007663:1'),
(113, 32, 'testimonial_titulus', 'CEO'),
(114, 32, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(115, 32, 'themeplate_field_range', '50'),
(116, 32, '_yoast_wpseo_content_score', '60'),
(118, 33, '_edit_last', '1'),
(119, 33, '_edit_lock', '1584007668:1'),
(120, 33, 'testimonial_titulus', 'CEO'),
(121, 33, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(122, 33, 'themeplate_field_range', '50'),
(123, 33, '_yoast_wpseo_content_score', '60'),
(125, 34, '_edit_last', '1'),
(126, 34, '_edit_lock', '1584007673:1'),
(127, 34, 'testimonial_titulus', 'CEO'),
(128, 34, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(129, 34, 'themeplate_field_range', '50'),
(130, 34, '_yoast_wpseo_content_score', '60'),
(141, 37, '_wp_attached_file', '2020/03/partner-logo.png'),
(142, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:624;s:4:\"file\";s:24:\"2020/03/partner-logo.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"partner-logo-300x195.png\";s:5:\"width\";i:300;s:6:\"height\";i:195;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"partner-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"partner-logo-768x499.png\";s:5:\"width\";i:768;s:6:\"height\";i:499;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(143, 37, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:20.910518778583636;s:5:\"bytes\";i:9053;s:11:\"size_before\";i:43294;s:10:\"size_after\";i:34241;s:4:\"time\";d:0.38;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:3:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:19.5;s:5:\"bytes\";i:1393;s:11:\"size_before\";i:7144;s:10:\"size_after\";i:5751;s:4:\"time\";d:0.07;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:16.11;s:5:\"bytes\";i:768;s:11:\"size_before\";i:4766;s:10:\"size_after\";i:3998;s:4:\"time\";d:0.09;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:21.96;s:5:\"bytes\";i:6892;s:11:\"size_before\";i:31384;s:10:\"size_after\";i:24492;s:4:\"time\";d:0.22;}}}'),
(144, 35, '_edit_last', '1'),
(145, 35, '_edit_lock', '1584019414:1'),
(146, 35, '_thumbnail_id', '37'),
(147, 35, 'themeplate_field_range', '50'),
(148, 35, '_yoast_wpseo_content_score', '30'),
(149, 38, '_edit_last', '1'),
(150, 38, '_edit_lock', '1584019433:1'),
(151, 38, '_thumbnail_id', '37'),
(152, 38, 'themeplate_field_range', '50'),
(153, 38, '_yoast_wpseo_content_score', '30'),
(156, 39, '_edit_last', '1'),
(157, 39, '_edit_lock', '1584019437:1'),
(158, 39, '_thumbnail_id', '37'),
(159, 39, 'themeplate_field_range', '50'),
(160, 39, '_yoast_wpseo_content_score', '30'),
(163, 40, '_edit_last', '1'),
(164, 40, '_edit_lock', '1584019442:1'),
(165, 40, '_thumbnail_id', '37'),
(166, 40, 'themeplate_field_range', '50'),
(167, 40, '_yoast_wpseo_content_score', '30'),
(168, 42, '_edit_last', '1'),
(169, 42, '_wp_page_template', 'default'),
(170, 44, 'themeplate_field_range', '50'),
(171, 42, 'themeplate_field_range', '50'),
(172, 42, '_edit_lock', '1585233611:1'),
(173, 46, '_menu_item_type', 'post_type'),
(174, 46, '_menu_item_menu_item_parent', '0'),
(175, 46, '_menu_item_object_id', '42'),
(176, 46, '_menu_item_object', 'page'),
(177, 46, '_menu_item_target', ''),
(178, 46, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(179, 46, '_menu_item_xfn', ''),
(180, 46, '_menu_item_url', ''),
(196, 51, '_edit_lock', '1585227863:1'),
(197, 51, '_edit_last', '1'),
(198, 51, '_thumbnail_id', '13'),
(199, 51, '_yoast_wpseo_content_score', '90'),
(200, 51, 'themeplate_field_range', '50'),
(203, 52, '_edit_lock', '1585227868:1'),
(204, 52, '_edit_last', '1'),
(205, 52, '_thumbnail_id', '13'),
(206, 52, '_yoast_wpseo_content_score', '90'),
(207, 52, 'themeplate_field_range', '50'),
(210, 53, '_edit_lock', '1585227881:1'),
(211, 53, '_edit_last', '1'),
(212, 53, '_thumbnail_id', '13'),
(213, 53, '_yoast_wpseo_content_score', '90'),
(214, 53, 'themeplate_field_range', '50'),
(232, 64, '_edit_last', '1'),
(233, 64, '_wp_page_template', 'archive-our_team.php'),
(234, 66, 'themeplate_field_range', '50'),
(235, 64, 'themeplate_field_range', '50'),
(236, 64, '_edit_lock', '1585560970:1'),
(237, 67, '_menu_item_type', 'post_type'),
(238, 67, '_menu_item_menu_item_parent', '0'),
(239, 67, '_menu_item_object_id', '64'),
(240, 67, '_menu_item_object', 'page'),
(241, 67, '_menu_item_target', ''),
(242, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(243, 67, '_menu_item_xfn', ''),
(244, 67, '_menu_item_url', ''),
(249, 71, '_wp_attached_file', '2020/03/dx-engine-fav.png'),
(250, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:178;s:6:\"height\";i:178;s:4:\"file\";s:25:\"2020/03/dx-engine-fav.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"dx-engine-fav-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(251, 71, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21129;s:10:\"size_after\";i:21129;s:4:\"time\";d:0.07;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21129;s:10:\"size_after\";i:21129;s:4:\"time\";d:0.07;}}}'),
(252, 72, '_wp_attached_file', '2020/03/cropped-dx-engine-fav.png'),
(253, 72, '_wp_attachment_context', 'site-icon'),
(254, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:33:\"2020/03/cropped-dx-engine-fav.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:31:\"cropped-dx-engine-fav-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(255, 72, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:0.060350333687053344;s:5:\"bytes\";i:120;s:11:\"size_before\";i:198839;s:10:\"size_after\";i:198719;s:4:\"time\";d:0.44;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:62929;s:10:\"size_after\";i:62929;s:4:\"time\";d:0.12;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21778;s:10:\"size_after\";i:21778;s:4:\"time\";d:0.13;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:53126;s:10:\"size_after\";i:53126;s:4:\"time\";d:0.08;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:30083;s:10:\"size_after\";i:30083;s:4:\"time\";d:0.04;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:28956;s:10:\"size_after\";i:28956;s:4:\"time\";d:0.05;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.1;s:5:\"bytes\";i:120;s:11:\"size_before\";i:1967;s:10:\"size_after\";i:1847;s:4:\"time\";d:0.02;}}}'),
(256, 73, '_wp_attached_file', '2020/03/dx-engine-fav-1.png'),
(257, 73, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:142;s:6:\"height\";i:128;s:4:\"file\";s:27:\"2020/03/dx-engine-fav-1.png\";s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(258, 73, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:17500;s:10:\"size_after\";i:17500;s:4:\"time\";d:0.05;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:17500;s:10:\"size_after\";i:17500;s:4:\"time\";d:0.05;}}}'),
(259, 74, '_wp_attached_file', '2020/03/cropped-dx-engine-fav-1.png'),
(260, 74, '_wp_attachment_context', 'site-icon'),
(261, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:35:\"2020/03/cropped-dx-engine-fav-1.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-1-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(262, 75, '_edit_lock', '1585653787:1'),
(263, 77, '_edit_last', '1'),
(264, 77, '_edit_lock', '1601988858:1'),
(265, 77, '_yoast_wpseo_content_score', '60'),
(266, 78, '_edit_last', '1'),
(267, 78, '_edit_lock', '1601989011:1'),
(268, 78, '_yoast_wpseo_content_score', '60'),
(269, 79, '_edit_last', '1'),
(270, 79, '_edit_lock', '1601989019:1'),
(271, 79, '_yoast_wpseo_content_score', '60'),
(272, 80, 'inline_featured_image', '0'),
(276, 82, '_wp_attached_file', '2020/10/fixbeton-logo-only.png'),
(277, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:457;s:6:\"height\";i:457;s:4:\"file\";s:30:\"2020/10/fixbeton-logo-only.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"fixbeton-logo-only-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"fixbeton-logo-only-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(278, 82, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:31.19774770268532;s:5:\"bytes\";i:21830;s:11:\"size_before\";i:69973;s:10:\"size_after\";i:48143;s:4:\"time\";d:0.30000000000000004;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.45;s:5:\"bytes\";i:15720;s:11:\"size_before\";i:51628;s:10:\"size_after\";i:35908;s:4:\"time\";d:0.14;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.31;s:5:\"bytes\";i:6110;s:11:\"size_before\";i:18345;s:10:\"size_after\";i:12235;s:4:\"time\";d:0.16;}}}'),
(279, 83, '_wp_attached_file', '2020/10/cropped-fixbeton-logo-only.png'),
(280, 83, '_wp_attachment_context', 'site-icon'),
(281, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:38:\"2020/10/cropped-fixbeton-logo-only.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:36:\"cropped-fixbeton-logo-only-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(282, 83, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.263604682334016;s:5:\"bytes\";i:50802;s:11:\"size_before\";i:167865;s:10:\"size_after\";i:117063;s:4:\"time\";d:0.8699999999999999;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:28.2;s:5:\"bytes\";i:15399;s:11:\"size_before\";i:54616;s:10:\"size_after\";i:39217;s:4:\"time\";d:0.21;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.85;s:5:\"bytes\";i:6266;s:11:\"size_before\";i:19077;s:10:\"size_after\";i:12811;s:4:\"time\";d:0.09;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.66;s:5:\"bytes\";i:13542;s:11:\"size_before\";i:45663;s:10:\"size_after\";i:32121;s:4:\"time\";d:0.35;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.06;s:5:\"bytes\";i:6868;s:11:\"size_before\";i:22112;s:10:\"size_after\";i:15244;s:4:\"time\";d:0.05;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.7;s:5:\"bytes\";i:8292;s:11:\"size_before\";i:24603;s:10:\"size_after\";i:16311;s:4:\"time\";d:0.1;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.25;s:5:\"bytes\";i:435;s:11:\"size_before\";i:1794;s:10:\"size_after\";i:1359;s:4:\"time\";d:0.07;}}}'),
(283, 85, '_edit_lock', '1603200176:1'),
(284, 86, '_wp_attached_file', '2020/10/fixbeton-logo-allo.png'),
(285, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:648;s:6:\"height\";i:648;s:4:\"file\";s:30:\"2020/10/fixbeton-logo-allo.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"fixbeton-logo-allo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"fixbeton-logo-allo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(286, 86, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:32.82371358285726;s:5:\"bytes\";i:16145;s:11:\"size_before\";i:49187;s:10:\"size_after\";i:33042;s:4:\"time\";d:0.29;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.03;s:5:\"bytes\";i:11746;s:11:\"size_before\";i:35561;s:10:\"size_after\";i:23815;s:4:\"time\";d:0.18;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.28;s:5:\"bytes\";i:4399;s:11:\"size_before\";i:13626;s:10:\"size_after\";i:9227;s:4:\"time\";d:0.11;}}}'),
(287, 87, '_wp_attached_file', '2020/10/cropped-fixbeton-logo-allo.png'),
(288, 87, '_wp_attachment_context', 'site-icon'),
(289, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:38:\"2020/10/cropped-fixbeton-logo-allo.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-allo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-allo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-allo-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-allo-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-allo-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:36:\"cropped-fixbeton-logo-allo-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(290, 87, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:29.133281942270706;s:5:\"bytes\";i:37415;s:11:\"size_before\";i:128427;s:10:\"size_after\";i:91012;s:4:\"time\";d:0.55;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";i:28;s:5:\"bytes\";i:11643;s:11:\"size_before\";i:41578;s:10:\"size_after\";i:29935;s:4:\"time\";d:0.14;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:28.13;s:5:\"bytes\";i:4099;s:11:\"size_before\";i:14574;s:10:\"size_after\";i:10475;s:4:\"time\";d:0.09;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";d:28.71;s:5:\"bytes\";i:9945;s:11:\"size_before\";i:34643;s:10:\"size_after\";i:24698;s:4:\"time\";d:0.12;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.6;s:5:\"bytes\";i:5368;s:11:\"size_before\";i:16985;s:10:\"size_after\";i:11617;s:4:\"time\";d:0.13;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.91;s:5:\"bytes\";i:5828;s:11:\"size_before\";i:18856;s:10:\"size_after\";i:13028;s:4:\"time\";d:0.06;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.7;s:5:\"bytes\";i:532;s:11:\"size_before\";i:1791;s:10:\"size_after\";i:1259;s:4:\"time\";d:0.01;}}}'),
(291, 89, '_wp_attached_file', '2020/10/cropped-fixbeton-logo-only-1.png'),
(292, 89, '_wp_attachment_context', 'site-icon'),
(293, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:40:\"2020/10/cropped-fixbeton-logo-only-1.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"cropped-fixbeton-logo-only-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"cropped-fixbeton-logo-only-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:40:\"cropped-fixbeton-logo-only-1-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:40:\"cropped-fixbeton-logo-only-1-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:40:\"cropped-fixbeton-logo-only-1-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:38:\"cropped-fixbeton-logo-only-1-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(294, 89, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.263604682334016;s:5:\"bytes\";i:50802;s:11:\"size_before\";i:167865;s:10:\"size_after\";i:117063;s:4:\"time\";d:1.56;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:28.2;s:5:\"bytes\";i:15399;s:11:\"size_before\";i:54616;s:10:\"size_after\";i:39217;s:4:\"time\";d:0.7;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.85;s:5:\"bytes\";i:6266;s:11:\"size_before\";i:19077;s:10:\"size_after\";i:12811;s:4:\"time\";d:0.04;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.66;s:5:\"bytes\";i:13542;s:11:\"size_before\";i:45663;s:10:\"size_after\";i:32121;s:4:\"time\";d:0.53;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.06;s:5:\"bytes\";i:6868;s:11:\"size_before\";i:22112;s:10:\"size_after\";i:15244;s:4:\"time\";d:0.16;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.7;s:5:\"bytes\";i:8292;s:11:\"size_before\";i:24603;s:10:\"size_after\";i:16311;s:4:\"time\";d:0.11;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.25;s:5:\"bytes\";i:435;s:11:\"size_before\";i:1794;s:10:\"size_after\";i:1359;s:4:\"time\";d:0.02;}}}');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-10-31 08:28:29', '2017-10-31 08:28:29', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 1', '', 'publish', 'open', 'open', '', 'hello-vilag', '', '', '2020-03-10 13:15:25', '2020-03-10 13:15:25', '', 0, 'http://localhost/dxengine/?p=1', 0, 'post', '', 1),
(2, 1, '2017-10-31 08:28:29', '2017-10-31 08:28:29', '', 'Főoldal', '', 'publish', 'closed', 'open', '', 'ez-egy-minta-oldal', '', '', '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 0, 'http://localhost/dxengine/?page_id=2', 0, 'page', '', 0),
(5, 1, '2018-06-02 09:12:39', '2018-06-02 09:12:39', '<div class=\"row contact-form\">\r\n\r\n <div class=\"col-lg-12 name\"> \r\n  <label> Telefonszám:\r\n  [tel* tel]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 email\">\r\n <label> E-mail cím:\r\n  [email* email]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 location\">\r\n  <label> Település:\r\n  [text* location]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 height\">\r\n  <label> Kerítésmagasság:\r\n  [text* height]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 length\">\r\n  <label> Kerítendő szakasz hossza:\r\n  [text* length]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 image-of-location\">\r\n  <label> Kép csatolása a területről\r\n  [file image-of-location limit:10]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 built-in\">\r\n   Beépítést kér?\r\n  [radio built-in default:1 \"Kérek\" \"Nem kérek\"]\r\n</div>\r\n\r\n<div class=\"col-lg-12 soil\">\r\n  <label> Talaj jellemzői:\r\n  [text* soil]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 message\">\r\n  <label> Egyéb megjegyzés:\r\n  [textarea message placeholder \"Egyéb kiegészítő információk\"]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 image-of-element\">\r\n  <label> Kép csatolása a kedvelt elemekről\r\n  [file image-of-item limit:10]</label>\r\n</div>\r\n\r\n<div class=\"col-lg-12 privacy\">\r\n  [acceptance privacy] Elolvastam és elfogadom az <a href=\"#\" target=\"_blank\" rel=\"noopener noreferrer\">Adatvédelmi Tájékoztatóban</a> leírtakat! [/acceptance]\r\n</div>\r\n\r\n<div class=\"col-lg-12 submit\">\r\n  [submit \"Üzenet küldése\"]\r\n</div>\r\n\r\n</div>\n1\nÉrdeklődés - Árajánlatkérés\nFix Beton - Árajánlat <info@fixbetonkerites.hu>\ngefusz@gmail.com\n<h2>Fix Beton Árajánlatkérés</h2>\r\n\r\n<h4>Az érdeklődő elérhetőségei:</h4>\r\n<strong>Telefonszám:</strong> [tel]\r\n<strong>Email:</strong> [email]\r\n\r\n<h4>Az éppítendő kerítés adatai:</h4>\r\n<strong>Település:</strong> [location]\r\n<strong>Kerítésmagasság:</strong> [height]\r\n<strong>Kerítendő szakasz hossza:</strong> [length]\r\n\r\n<strong>Beépítést kér:</strong> [built-in]\r\n<strong>Talaj jellemzők:</strong> [soil]\r\n\r\n<h4>Leírás a projektről:</h4>\r\n[message]\n\n[image-of-location][image-of-item]\n1\n\n\nSikeres Beküldés\nadmin@email.com\n[your-email]\nSikeres Beküldés\n\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nNem megfelelő a dátum formátuma.\nA dátum korábbi, mint az engedélyezett legkorábbi időpont.\nA dátum az engedélyezett időpont utáni.\nIsmeretlen eredetű hiba történt a fájlfeltöltéskor.\nNem engedélyezett az ilyen típusú fájl feltöltése.\nTúl nagyméretű a fájl.\nHiba történt a fájlfeltöltés közben.\nA szám formátuma érvénytelen.\nA szám kisebb az engedélyezett legkisebb számnál.\nA szám nagyobb az engedélyezett legnagyobb számnál.\nA quiz-kérdésre adott válasz hibás.\nA megadott e-mail cím érvénytelen.\nAz URL Cím érvénytelen\nA telefonszám érvénytelen.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2020-10-22 11:37:37', '2020-10-22 09:37:37', '', 0, 'http://localhost/dxengine/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(9, 1, '2020-03-10 09:54:33', '2020-03-10 09:54:33', '[newsletter]', 'Newsletter', '', 'publish', 'closed', 'closed', '', 'newsletter', '', '', '2020-03-10 09:54:33', '2020-03-10 09:54:33', '', 0, 'http://localhost/dxengine/newsletter/', 0, 'page', '', 0),
(10, 1, '2020-03-10 10:33:22', '2020-03-10 10:33:22', 'Ez egy mintaoldal, egy WordPress-alapú honlap egy oldalának létrehozásához. Az oldal különbözik a bejegyzéstől, mert az oldal állandóan látszik a honlap menüjében, a navigáció során (a legtöbb sablonban). Az emberek többsége használja a Névjegy oldalt, amely arra szolgál, hogy a lehetséges látogatói számára elmondjanak magukról, vállalkozásukról, a honlapról néhány jellemző mondatot. Egyik lehetséges megoldás erre pl.:\n<blockquote>Helló Emberek!Ezt a fordítást a WordPress Magyarország Fordítói csapata végezte. Sok száz munkaórát fektettünk abba, hogy ezt a remek tartalomkezelő rendszert anyanyelvünkön, magyarul használhassa mindenki.</blockquote>\n.... vagy egy másik példa:\n<blockquote>A<strong> WordPress Magyarország</strong> tulajdonképpen a kezdetektől létezik anélkül, hogy kihirdette volna létét. <strong>A WordPress Magyarország egy Közösség!</strong> - a nyílt forráskódú, zseniális WordPress tartalomkezelő rendszer alkalmazására, megismerésére, tanulására szövetkeztünk.</blockquote>\nÚj WordPress-felhasználóként a <a href=\"http://localhost/dxengine/wp-admin/\">Vezérlőpult</a>ba lépve ez az oldal törölhető, módosítható, és új oldalak hozhatóak létre saját tartalommal. Jó szórakozást kíván a WordPress Nemzeközi Közössége!', 'Ez egy minta oldal', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-03-10 10:33:22', '2020-03-10 10:33:22', '', 2, 'https://localhost/wordpress-bootstrap-4-boilerplate/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-03-10 13:15:13', '2020-03-10 13:15:13', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 3', '', 'publish', 'open', 'open', '', 'blog-post-3', '', '', '2020-03-10 13:15:13', '2020-03-10 13:15:13', '', 0, 'http://localhost/dxengine/?p=11', 0, 'post', '', 0),
(12, 1, '2020-03-10 13:14:59', '2020-03-10 13:14:59', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 2', '', 'publish', 'open', 'open', '', 'blog-post-2', '', '', '2020-03-10 13:14:59', '2020-03-10 13:14:59', '', 0, 'http://localhost/dxengine/?p=12', 0, 'post', '', 0),
(13, 1, '2020-03-10 13:14:40', '2020-03-10 13:14:40', '', 'placeholder-img-3', '', 'inherit', 'open', 'closed', '', 'placeholder-img-3', '', '', '2020-03-10 13:14:40', '2020-03-10 13:14:40', '', 12, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2020-03-10 13:14:59', '2020-03-10 13:14:59', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 2', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-03-10 13:14:59', '2020-03-10 13:14:59', '', 12, 'http://localhost/dxengine/12-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2020-03-10 13:15:13', '2020-03-10 13:15:13', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 3', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2020-03-10 13:15:13', '2020-03-10 13:15:13', '', 11, 'http://localhost/dxengine/11-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2020-03-10 13:15:25', '2020-03-10 13:15:25', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 1', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-03-10 13:15:25', '2020-03-10 13:15:25', '', 1, 'http://localhost/dxengine/1-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2020-03-12 09:35:02', '2020-03-12 09:35:02', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"our_team\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Our Team', 'our-team', 'publish', 'closed', 'closed', '', 'group_5e6a022d542c0', '', '', '2020-03-12 10:06:24', '2020-03-12 10:06:24', '', 0, 'http://localhost/dxengine/?post_type=acf-field-group&#038;p=17', 0, 'acf-field-group', '', 0),
(19, 1, '2020-03-12 09:35:02', '2020-03-12 09:35:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titulus', 'member_titulus', 'publish', 'closed', 'closed', '', 'field_5e6a023c16dca', '', '', '2020-03-12 10:06:24', '2020-03-12 10:06:24', '', 17, 'http://localhost/dxengine/?post_type=acf-field&#038;p=19', 0, 'acf-field', '', 0),
(20, 1, '2020-03-12 09:39:05', '2020-03-12 09:39:05', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 1', '', 'publish', 'closed', 'closed', '', 'team-member-1', '', '', '2020-03-12 09:39:05', '2020-03-12 09:39:05', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=20', 0, 'our_team', '', 0),
(22, 1, '2020-03-12 09:38:58', '2020-03-12 09:38:58', '', 'unknown-person', '', 'inherit', 'open', 'closed', '', 'unknown-person', '', '', '2020-03-12 09:38:58', '2020-03-12 09:38:58', '', 20, 'http://localhost/dxengine/wp-content/uploads/2020/03/unknown-person.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2020-03-12 09:39:12', '2020-03-12 09:39:12', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 2', '', 'publish', 'closed', 'closed', '', 'team-member-2', '', '', '2020-03-12 09:41:12', '2020-03-12 09:41:12', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=23', 0, 'our_team', '', 0),
(24, 1, '2020-03-12 09:39:14', '2020-03-12 09:39:14', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 3', '', 'publish', 'closed', 'closed', '', 'team-member-3', '', '', '2020-03-12 09:41:16', '2020-03-12 09:41:16', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=24', 0, 'our_team', '', 0),
(25, 1, '2020-03-12 09:39:17', '2020-03-12 09:39:17', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 4', '', 'publish', 'closed', 'closed', '', 'team-member-4', '', '', '2020-03-12 09:41:24', '2020-03-12 09:41:24', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=25', 0, 'our_team', '', 0),
(26, 1, '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 'Főoldal', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 2, 'http://localhost/dxengine/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-03-12 10:06:08', '2020-03-12 10:06:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"testimonial\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Testimonial', 'testimonial', 'publish', 'closed', 'closed', '', 'group_5e6a096c84b2f', '', '', '2020-03-12 10:06:08', '2020-03-12 10:06:08', '', 0, 'http://localhost/dxengine/?post_type=acf-field-group&#038;p=27', 0, 'acf-field-group', '', 0),
(29, 1, '2020-03-12 10:06:08', '2020-03-12 10:06:08', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titulus', 'testimonial_titulus', 'publish', 'closed', 'closed', '', 'field_5e6a0977edbdf', '', '', '2020-03-12 10:06:08', '2020-03-12 10:06:08', '', 27, 'http://localhost/dxengine/?post_type=acf-field&p=29', 0, 'acf-field', '', 0),
(30, 1, '2020-03-12 10:07:25', '2020-03-12 10:07:25', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 1', '', 'publish', 'closed', 'closed', '', 'testimonial-1', '', '', '2020-03-12 10:07:25', '2020-03-12 10:07:25', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=30', 0, 'testimonial', '', 0),
(32, 1, '2020-03-12 10:07:32', '2020-03-12 10:07:32', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 2', '', 'publish', 'closed', 'closed', '', 'testimonial-2', '', '', '2020-03-12 10:07:43', '2020-03-12 10:07:43', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=32', 0, 'testimonial', '', 0),
(33, 1, '2020-03-12 10:07:35', '2020-03-12 10:07:35', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 3', '', 'publish', 'closed', 'closed', '', 'testimonial-3', '', '', '2020-03-12 10:07:48', '2020-03-12 10:07:48', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=33', 0, 'testimonial', '', 0),
(34, 1, '2020-03-12 10:07:37', '2020-03-12 10:07:37', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 4', '', 'publish', 'closed', 'closed', '', 'testimonial-4', '', '', '2020-03-12 10:07:53', '2020-03-12 10:07:53', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=34', 0, 'testimonial', '', 0),
(35, 1, '2020-03-12 13:23:33', '2020-03-12 13:23:33', '', 'Partner 1', '', 'publish', 'closed', 'closed', '', 'partner-1', '', '', '2020-03-12 13:23:33', '2020-03-12 13:23:33', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=35', 0, 'partner', '', 0),
(37, 1, '2020-03-12 13:23:21', '2020-03-12 13:23:21', '', 'partner-logo', '', 'inherit', 'open', 'closed', '', 'partner-logo', '', '', '2020-03-12 13:23:21', '2020-03-12 13:23:21', '', 35, 'http://localhost/dxengine/wp-content/uploads/2020/03/partner-logo.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2020-03-12 13:23:38', '2020-03-12 13:23:38', '', 'Partner 2', '', 'publish', 'closed', 'closed', '', 'partner-2', '', '', '2020-03-12 13:23:53', '2020-03-12 13:23:53', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=38', 0, 'partner', '', 0),
(39, 1, '2020-03-12 13:23:41', '2020-03-12 13:23:41', '', 'Partner 3', '', 'publish', 'closed', 'closed', '', 'partner-3', '', '', '2020-03-12 13:23:57', '2020-03-12 13:23:57', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=39', 0, 'partner', '', 0),
(40, 1, '2020-03-12 13:23:44', '2020-03-12 13:23:44', '', 'Partner 4', '', 'publish', 'closed', 'closed', '', 'partner-4', '', '', '2020-03-12 13:24:02', '2020-03-12 13:24:02', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=40', 0, 'partner', '', 0),
(42, 1, '2020-03-24 14:14:36', '2020-03-24 14:14:36', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2020-03-26 14:40:09', '2020-03-26 14:40:09', '', 0, 'http://localhost/dxengine/?page_id=42', 0, 'page', '', 0),
(44, 1, '2020-03-24 14:14:36', '2020-03-24 14:14:36', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2020-03-24 14:14:36', '2020-03-24 14:14:36', '', 42, 'http://localhost/dxengine/42-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-03-24 14:15:11', '2020-03-24 14:15:11', '{\n    \"page_for_posts\": {\n        \"value\": \"42\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-24 14:15:11\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'b6c38f01-2be6-4993-8cd2-4dd5c3b4182a', '', '', '2020-03-24 14:15:11', '2020-03-24 14:15:11', '', 0, 'http://localhost/dxengine/b6c38f01-2be6-4993-8cd2-4dd5c3b4182a/', 0, 'customize_changeset', '', 0),
(46, 1, '2020-03-24 14:16:16', '2020-03-24 14:16:16', ' ', '', '', 'publish', 'closed', 'closed', '', '46', '', '', '2020-03-30 09:39:55', '2020-03-30 09:39:55', '', 0, 'http://localhost/dxengine/?p=46', 2, 'nav_menu_item', '', 0),
(51, 1, '2020-03-26 13:04:03', '2020-03-26 13:04:03', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 4', '', 'publish', 'open', 'open', '', 'blog-post-4', '', '', '2020-03-26 13:04:23', '2020-03-26 13:04:23', '', 0, 'http://localhost/dxengine/?p=51', 0, 'post', '', 0),
(52, 1, '2020-03-26 13:04:06', '2020-03-26 13:04:06', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 5', '', 'publish', 'open', 'open', '', 'blog-post-5', '', '', '2020-03-26 13:04:28', '2020-03-26 13:04:28', '', 0, 'http://localhost/dxengine/?p=52', 0, 'post', '', 0),
(53, 1, '2020-03-26 13:04:11', '2020-03-26 13:04:11', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 6', '', 'publish', 'open', 'open', '', 'blog-post-6', '', '', '2020-03-26 13:04:41', '2020-03-26 13:04:41', '', 0, 'http://localhost/dxengine/?p=53', 0, 'post', '', 0),
(54, 1, '2020-03-26 13:04:19', '2020-03-26 13:04:19', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 4', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-03-26 13:04:19', '2020-03-26 13:04:19', '', 51, 'http://localhost/dxengine/51-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-03-26 13:04:28', '2020-03-26 13:04:28', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 5', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2020-03-26 13:04:28', '2020-03-26 13:04:28', '', 52, 'http://localhost/dxengine/52-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-03-26 13:04:33', '2020-03-26 13:04:33', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 6', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2020-03-26 13:04:33', '2020-03-26 13:04:33', '', 53, 'http://localhost/dxengine/53-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 'CPT', '', 'publish', 'closed', 'closed', '', 'cpt', '', '', '2020-03-30 09:38:32', '2020-03-30 09:38:32', '', 0, 'http://localhost/dxengine/?page_id=64', 0, 'page', '', 0),
(66, 1, '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 'Portfolio', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 64, 'http://localhost/dxengine/64-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2020-03-30 09:22:23', '2020-03-30 09:22:23', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2020-03-30 09:39:55', '2020-03-30 09:39:55', '', 0, 'http://localhost/dxengine/?p=67', 1, 'nav_menu_item', '', 0),
(68, 1, '2020-03-30 09:35:25', '2020-03-30 09:35:25', '', 'CPT', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2020-03-30 09:35:25', '2020-03-30 09:35:25', '', 64, 'http://localhost/dxengine/64-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-03-31 11:22:00', '2020-03-31 11:22:00', '', 'dx-engine-fav', '', 'inherit', 'open', 'closed', '', 'dx-engine-fav', '', '', '2020-03-31 11:22:00', '2020-03-31 11:22:00', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/dx-engine-fav.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2020-03-31 11:22:05', '2020-03-31 11:22:05', 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', 'cropped-dx-engine-fav.png', '', 'inherit', 'open', 'closed', '', 'cropped-dx-engine-fav-png', '', '', '2020-03-31 11:22:05', '2020-03-31 11:22:05', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2020-03-31 11:22:59', '2020-03-31 11:22:59', '', 'dx-engine-fav', '', 'inherit', 'open', 'closed', '', 'dx-engine-fav-2', '', '', '2020-03-31 11:22:59', '2020-03-31 11:22:59', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/dx-engine-fav-1.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2020-03-31 11:23:04', '2020-03-31 11:23:04', 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', 'cropped-dx-engine-fav-1.png', '', 'inherit', 'open', 'closed', '', 'cropped-dx-engine-fav-1-png', '', '', '2020-03-31 11:23:04', '2020-03-31 11:23:04', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2020-03-31 11:23:18', '2020-03-31 11:23:18', '{\n    \"site_icon\": {\n        \"value\": 74,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-31 11:23:07\"\n    },\n    \"blogdescription\": {\n        \"value\": \"powered by dxlabz\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-31 11:23:18\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', '00fe786f-e330-4df2-9d6b-363f500833e5', '', '', '2020-03-31 11:23:18', '2020-03-31 11:23:18', '', 0, 'http://localhost/dxengine/?p=75', 0, 'customize_changeset', '', 0),
(77, 1, '2020-10-06 12:56:38', '2020-10-06 12:56:38', 'Azért', 'Miért 1', '', 'publish', 'closed', 'closed', '', 'miert-1', '', '', '2020-10-06 12:56:38', '2020-10-06 12:56:38', '', 0, 'http://localhost/dxengine/?post_type=gyik&#038;p=77', 0, 'gyik', '', 0),
(78, 1, '2020-10-06 12:56:44', '2020-10-06 12:56:44', 'Azért', 'Miért 2', '', 'publish', 'closed', 'closed', '', 'miert-2', '', '', '2020-10-06 12:56:51', '2020-10-06 12:56:51', '', 0, 'http://localhost/dxengine/?post_type=gyik&#038;p=78', 0, 'gyik', '', 0),
(79, 1, '2020-10-06 12:56:53', '2020-10-06 12:56:53', 'Azért', 'Miért 3', '', 'publish', 'closed', 'closed', '', 'miert-3', '', '', '2020-10-06 12:56:59', '2020-10-06 12:56:59', '', 0, 'http://localhost/dxengine/?post_type=gyik&#038;p=79', 0, 'gyik', '', 0),
(80, 1, '2020-10-20 12:37:08', '0000-00-00 00:00:00', '', 'Automatikus vázlat', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-10-20 12:37:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/dxengine/?p=80', 0, 'post', '', 0),
(82, 1, '2020-10-20 15:20:39', '2020-10-20 13:20:39', '', 'fixbeton-logo-only', '', 'inherit', 'open', 'closed', '', 'fixbeton-logo-only', '', '', '2020-10-20 15:20:39', '2020-10-20 13:20:39', '', 0, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/fixbeton-logo-only.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2020-10-20 15:20:43', '2020-10-20 13:20:43', 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only.png', 'cropped-fixbeton-logo-only.png', '', 'inherit', 'open', 'closed', '', 'cropped-fixbeton-logo-only-png', '', '', '2020-10-20 15:20:43', '2020-10-20 13:20:43', '', 0, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2020-10-20 15:20:43', '2020-10-20 13:20:43', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', 'f989020eba67179b523ef97f87c85d76', '', '', '2020-10-20 15:20:43', '2020-10-20 13:20:43', '', 0, 'http://localhost/fixbetonkerites.hu/f989020eba67179b523ef97f87c85d76/', 0, 'oembed_cache', '', 0),
(85, 1, '2020-10-20 15:22:57', '2020-10-20 13:22:57', '{\n    \"site_icon\": {\n        \"value\": 89,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-10-20 13:22:57\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'e051a989-9700-49d9-b403-f4d5ca3308cf', '', '', '2020-10-20 15:22:57', '2020-10-20 13:22:57', '', 0, 'http://localhost/fixbetonkerites.hu/?p=85', 0, 'customize_changeset', '', 0),
(86, 1, '2020-10-20 15:22:36', '2020-10-20 13:22:36', '', 'fixbeton-logo-allo', '', 'inherit', 'open', 'closed', '', 'fixbeton-logo-allo', '', '', '2020-10-20 15:22:36', '2020-10-20 13:22:36', '', 0, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/fixbeton-logo-allo.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2020-10-20 15:22:41', '2020-10-20 13:22:41', 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-allo.png', 'cropped-fixbeton-logo-allo.png', '', 'inherit', 'open', 'closed', '', 'cropped-fixbeton-logo-allo-png', '', '', '2020-10-20 15:22:41', '2020-10-20 13:22:41', '', 0, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-allo.png', 0, 'attachment', 'image/png', 0),
(88, 1, '2020-10-20 15:22:41', '2020-10-20 13:22:41', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', 'feea9ff3f9c52120d31b3191630cb2dc', '', '', '2020-10-20 15:22:41', '2020-10-20 13:22:41', '', 0, 'http://localhost/fixbetonkerites.hu/feea9ff3f9c52120d31b3191630cb2dc/', 0, 'oembed_cache', '', 0),
(89, 1, '2020-10-20 15:22:52', '2020-10-20 13:22:52', 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only-1.png', 'cropped-fixbeton-logo-only-1.png', '', 'inherit', 'open', 'closed', '', 'cropped-fixbeton-logo-only-1-png', '', '', '2020-10-20 15:22:52', '2020-10-20 13:22:52', '', 0, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only-1.png', 0, 'attachment', 'image/png', 0),
(90, 1, '2020-10-20 15:22:52', '2020-10-20 13:22:52', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', 'a2d8a29fb1ede7b581d71c452f579248', '', '', '2020-10-20 15:22:52', '2020-10-20 13:22:52', '', 0, 'http://localhost/fixbetonkerites.hu/a2d8a29fb1ede7b581d71c452f579248/', 0, 'oembed_cache', '', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_smush_dir_images`
--

CREATE TABLE `wp_smush_dir_images` (
  `id` mediumint(9) NOT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_hash` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resize` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lossy` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_size` int(10) UNSIGNED DEFAULT NULL,
  `orig_size` int(10) UNSIGNED DEFAULT NULL,
  `file_time` int(10) UNSIGNED DEFAULT NULL,
  `last_scan` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Egyéb', 'egyeb', 0),
(2, 'Main menu', 'main-menu', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(11, 1, 0),
(12, 1, 0),
(46, 2, 0),
(51, 1, 0),
(52, 1, 0),
(53, 1, 0),
(67, 2, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 6),
(2, 2, 'nav_menu', '', 0, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(14, 1, 'show_welcome_panel', '0'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '80'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, '_yoast_wpseo_profile_updated', '1548620464'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:23:\"add-post-type-portfolio\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:17:\"add-portfolio-cat\";i:4;s:17:\"add-portfolio-tag\";}'),
(22, 1, 'wbcr_clearfy_hidden_notices', 'a:2:{s:65:\"35cc128cded936867f1217fc156cadf6_60cff2f1de6f2d36f6a38932b8d702f6\";s:137:\"Az új ThemePlate verzió már elérhető.  A legfrissebb 3.2.1 változat részletei itt tekinthetőek meg, vagy automatikus frissítés.\";s:65:\"73c6ed5c28b46cd800ea1cb82de8949b_cb1ec3fc7d15a1e7b961766658c9f956\";s:216:\"The following recommended plugin is currently inactive: W3 Total Cache.\nThere are updates available for the following plugins: Enable Media Replace and W3 Total Cache.\nBegin updating plugins | Begin activating plugin\";}'),
(23, 1, 'session_tokens', 'a:3:{s:64:\"8bf28ec84a8cfdf1a01f4ab130d7b2d4c7237761507fbf7af1a012f6b57b6b3f\";a:4:{s:10:\"expiration\";i:1603370225;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36\";s:5:\"login\";i:1603197425;}s:64:\"3a7830a14147696dcfc148408b0dee40de0bb958f8cc597e53651084756ad6b5\";a:4:{s:10:\"expiration\";i:1603372588;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36\";s:5:\"login\";i:1603199788;}s:64:\"95a5df90490932be3dc7e346a1b063fccc68816f83c358b306ec94eb85ed8f01\";a:4:{s:10:\"expiration\";i:1603530789;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36\";s:5:\"login\";i:1603357989;}}'),
(24, 1, 'wp_yoast_notifications', 'a:3:{i:0;a:2:{s:7:\"message\";s:621:\"<p>As you can see, there is a translation of this plugin in Hungarian. This translation is currently 84% complete. We need your help to make it complete and to fix any errors. Please register at <a href=\"https://translate.wordpress.org/projects/wp-plugins/wordpress-seo/\">Translating WordPress</a> to help complete the translation to Hungarian!</p><p><a href=\"https://translate.wordpress.org/projects/wp-plugins/wordpress-seo/\">Register now &raquo;</a></p><a class=\"button\" href=\"/fixbetonkerites.hu/wp-admin/admin.php?page=wpseo_dashboard&#038;remove_i18n_promo=1\">Please don&#039;t show me this notification anymore</a>\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:31:\"i18nModuleTranslationAssistance\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:5:\"admin\";s:9:\"user_pass\";s:34:\"$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0\";s:13:\"user_nicename\";s:5:\"admin\";s:10:\"user_email\";s:18:\"doxa84@hotmail.com\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2017-10-31 08:28:28\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:5:\"admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:65:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:17:\"manage_email_logs\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:20:\"wpseo_manage_options\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:420:\"<strong>New in Yoast SEO 15.1: </strong>Easily find related keyphrases and their actual Google search volume with the SEMRush integration; Farsi keyphrase recognition. <a href=\"https://yoa.st/yoast15-1?php_version=7.3&#038;platform=wordpress&#038;platform_version=5.5.1&#038;software=free&#038;software_version=15.1.1&#038;days_active=30plus&#038;user_language=hu_HU\" target=\"_blank\">Read all about version 15.1 here</a>\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"updated\";s:2:\"id\";s:20:\"wpseo-plugin-updated\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:5:\"admin\";s:9:\"user_pass\";s:34:\"$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0\";s:13:\"user_nicename\";s:5:\"admin\";s:10:\"user_email\";s:18:\"doxa84@hotmail.com\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2017-10-31 08:28:28\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:5:\"admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:65:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:17:\"manage_email_logs\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:1:{s:13:\"dismiss_value\";s:6:\"15.1.1\";}s:13:\"dismissal_key\";s:20:\"wpseo-plugin-updated\";s:12:\"capabilities\";a:1:{i:0;s:20:\"wpseo_manage_options\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:2;a:2:{s:7:\"message\";O:61:\"Yoast\\WP\\SEO\\Presenters\\Admin\\Indexing_Notification_Presenter\":3:{s:18:\"\0*\0total_unindexed\";i:57;s:9:\"\0*\0reason\";s:23:\"home_url_option_changed\";s:20:\"\0*\0short_link_helper\";O:38:\"Yoast\\WP\\SEO\\Helpers\\Short_Link_Helper\":2:{s:17:\"\0*\0options_helper\";O:35:\"Yoast\\WP\\SEO\\Helpers\\Options_Helper\":0:{}s:17:\"\0*\0product_helper\";O:35:\"Yoast\\WP\\SEO\\Helpers\\Product_Helper\":0:{}}}s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:13:\"wpseo-reindex\";s:4:\"user\";r:106;s:5:\"nonce\";N;s:8:\"priority\";d:0.8;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(25, 1, 'nav_menu_recently_edited', '2'),
(26, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(27, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:24:\"wpseo-dashboard-overview\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(28, 1, 'wp_user-settings', 'libraryContent=browse'),
(29, 1, 'wp_user-settings-time', '1583846095'),
(30, 1, 'closedpostboxes_our_team', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:27:\"themeplate_themeplate_field\";}'),
(31, 1, 'metaboxhidden_our_team', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(32, 1, 'meta-box-order_our_team', 'a:5:{s:15:\"acf_after_title\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:42:\"acf-group_5e6a022d542c0,wpseo_meta,slugdiv\";s:8:\"advanced\";s:27:\"themeplate_themeplate_field\";}'),
(33, 1, 'screen_layout_our_team', '2'),
(34, 1, 'closedpostboxes_testimonial', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:27:\"themeplate_themeplate_field\";}'),
(35, 1, 'metaboxhidden_testimonial', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(36, 1, 'meta-box-order_testimonial', 'a:5:{s:15:\"acf_after_title\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:42:\"acf-group_5e6a096c84b2f,wpseo_meta,slugdiv\";s:8:\"advanced\";s:27:\"themeplate_themeplate_field\";}'),
(37, 1, 'screen_layout_testimonial', '2'),
(38, 1, 'closedpostboxes_partner', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:27:\"themeplate_themeplate_field\";}'),
(39, 1, 'metaboxhidden_partner', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(40, 1, 'meta-box-order_partner', 'a:5:{s:15:\"acf_after_title\";s:0:\"\";s:11:\"after_title\";s:12:\"postimagediv\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:18:\"wpseo_meta,slugdiv\";s:8:\"advanced\";s:27:\"themeplate_themeplate_field\";}'),
(41, 1, 'screen_layout_partner', '2'),
(42, 1, 'wp_i18nModuleTranslationAssistance', 'seen'),
(44, 1, 'wp_wpseo-plugin-updated', '15.1.1');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0', 'admin', 'doxa84@hotmail.com', '', '2017-10-31 08:28:28', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_indexable`
--

CREATE TABLE `wp_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` int(11) UNSIGNED DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` int(11) UNSIGNED DEFAULT NULL,
  `post_parent` int(11) UNSIGNED DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT 0,
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT 0,
  `is_robots_noindex` tinyint(1) DEFAULT 0,
  `is_robots_nofollow` tinyint(1) DEFAULT 0,
  `is_robots_noarchive` tinyint(1) DEFAULT 0,
  `is_robots_noimageindex` tinyint(1) DEFAULT 0,
  `is_robots_nosnippet` tinyint(1) DEFAULT 0,
  `twitter_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1,
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_indexable`
--

INSERT INTO `wp_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`) VALUES
(1, 'http://localhost/fixbetonkerites.hu/author/admin/', '49:ce81379cbb161eed3df0f8536263f6f2', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/5cfb74386c805526089e57863c520eb7?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/5cfb74386c805526089e57863c520eb7?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-10-06 11:40:17', '2020-10-22 07:37:37', 1, NULL, NULL, NULL, NULL, 0),
(2, 'http://localhost/fixbetonkerites.hu/blog-post-6/', '48:ee76072ace99505b2e5c949b19f81cdb', 53, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 6', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\n    \"width\": 800,\n    \"height\": 600,\n    \"url\": \"http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"path\": \"C:\\\\xampp\\\\htdocs\\\\dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"size\": \"full\",\n    \"id\": 13,\n    \"alt\": \"\",\n    \"pixels\": 480000,\n    \"type\": \"image/jpeg\"\n}', 0, NULL, NULL, '2020-10-06 11:41:17', '2020-10-22 07:13:13', 1, NULL, NULL, NULL, NULL, 0),
(3, 'http://localhost/fixbetonkerites.hu/blog-post-5/', '48:7ffe074a205b1e53393e411479ee31ae', 52, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 5', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\n    \"width\": 800,\n    \"height\": 600,\n    \"url\": \"http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"path\": \"C:\\\\xampp\\\\htdocs\\\\dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"size\": \"full\",\n    \"id\": 13,\n    \"alt\": \"\",\n    \"pixels\": 480000,\n    \"type\": \"image/jpeg\"\n}', 0, NULL, NULL, '2020-10-06 11:41:17', '2020-10-22 07:13:13', 1, NULL, NULL, NULL, NULL, 0),
(4, 'http://localhost/fixbetonkerites.hu/blog-post-4/', '48:053820d43a81a46f15ef748be08f7103', 51, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\n    \"width\": 800,\n    \"height\": 600,\n    \"url\": \"http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"path\": \"C:\\\\xampp\\\\htdocs\\\\dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\n    \"size\": \"full\",\n    \"id\": 13,\n    \"alt\": \"\",\n    \"pixels\": 480000,\n    \"type\": \"image/jpeg\"\n}', 0, NULL, NULL, '2020-10-06 11:41:17', '2020-10-22 07:13:13', 1, NULL, NULL, NULL, NULL, 0),
(5, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-06 11:42:15', '2020-10-06 09:42:15', 1, NULL, NULL, NULL, NULL, 0),
(6, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'Keresési eredmény a következőre: %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-06 11:42:15', '2020-10-06 09:42:15', 1, NULL, NULL, NULL, NULL, 0),
(7, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Az oldal nem található %%sep%% %%sitename%%', NULL, 'HIBA 404: A keresett oldal nem található', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-06 11:42:15', '2020-10-06 09:42:15', 1, NULL, NULL, NULL, NULL, 0),
(8, NULL, NULL, NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', 'Biztonságos, Gyors, Esztétikus megoldások Olcsón, a Legmodernebb Technológiákkal', 'Főoldal', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2020-10-06 11:42:15', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(9, 'https://localhost/fixbetonkerites.hu/', '37:412e8686341e9b296a84179cb71e5e32', 2, 'post', 'page', 1, 0, NULL, NULL, 'Főoldal', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 11:42:18', '2020-10-22 07:14:58', 1, NULL, NULL, NULL, NULL, 0),
(10, NULL, NULL, 42, 'post', 'page', 1, 0, NULL, NULL, 'Blog', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 11:42:29', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(11, 'http://localhost/fixbetonkerites.hu/?post_type=wpcf7_contact_form&p=5', '69:f61d5f8516575c13302ccdbf6c044022', 5, 'post', 'wpcf7_contact_form', 1, 0, NULL, NULL, 'Contact form 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-06 11:48:13', '2020-10-22 07:37:37', 1, NULL, NULL, NULL, NULL, 0),
(12, NULL, NULL, 64, 'post', 'page', 1, 0, NULL, NULL, 'CPT', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 12:40:32', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(13, NULL, NULL, 77, 'post', 'gyik', 1, 0, NULL, NULL, 'Miért 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 12:56:27', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(14, NULL, NULL, 78, 'post', 'gyik', 1, 0, NULL, NULL, 'Miért 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 12:56:45', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(15, NULL, NULL, 79, 'post', 'gyik', 1, 0, NULL, NULL, 'Miért 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-06 12:56:53', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(16, NULL, NULL, 80, 'post', 'post', 1, 0, NULL, NULL, 'Automatikus vázlat', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-20 12:37:08', '2020-10-20 12:39:31', 1, NULL, NULL, NULL, NULL, 0),
(18, NULL, NULL, 82, 'post', 'attachment', 1, 0, NULL, NULL, 'fixbeton-logo-only', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/fixbeton-logo-only.png', NULL, '82', 'attachment-image', NULL, NULL, NULL, '82', 'attachment-image', NULL, 0, NULL, NULL, '2020-10-20 13:20:39', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(19, NULL, NULL, 83, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-fixbeton-logo-only.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only.png', NULL, '83', 'attachment-image', NULL, NULL, NULL, '83', 'attachment-image', NULL, 0, NULL, NULL, '2020-10-20 13:20:43', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(20, 'http://localhost/fixbetonkerites.hu/f989020eba67179b523ef97f87c85d76/', '69:175b42b42bbb163eb4da16d7f98d3cc4', 84, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-20 13:20:43', '2020-10-20 11:20:43', 1, NULL, NULL, NULL, NULL, 0),
(21, 'http://localhost/fixbetonkerites.hu/e051a989-9700-49d9-b403-f4d5ca3308cf/', '73:db560ccab758b39b98448fa4dbadd67f', 85, 'post', 'customize_changeset', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-20 13:20:54', '2020-10-20 11:22:57', 1, NULL, NULL, NULL, NULL, 0),
(22, NULL, NULL, 86, 'post', 'attachment', 1, 0, NULL, NULL, 'fixbeton-logo-allo', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/fixbeton-logo-allo.png', NULL, '86', 'attachment-image', NULL, NULL, NULL, '86', 'attachment-image', NULL, 0, NULL, NULL, '2020-10-20 13:22:36', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(23, NULL, NULL, 87, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-fixbeton-logo-allo.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-allo.png', NULL, '87', 'attachment-image', NULL, NULL, NULL, '87', 'attachment-image', NULL, 0, NULL, NULL, '2020-10-20 13:22:41', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(24, 'http://localhost/fixbetonkerites.hu/feea9ff3f9c52120d31b3191630cb2dc/', '69:5c64cfebb454b8aa4c5d5eaa55adfd97', 88, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-20 13:22:41', '2020-10-20 11:22:41', 1, NULL, NULL, NULL, NULL, 0),
(25, NULL, NULL, 89, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-fixbeton-logo-only-1.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/fixbetonkerites.hu/wp-content/uploads/2020/10/cropped-fixbeton-logo-only-1.png', NULL, '89', 'attachment-image', NULL, NULL, NULL, '89', 'attachment-image', NULL, 0, NULL, NULL, '2020-10-20 13:22:52', '2020-10-22 09:12:28', 1, NULL, NULL, NULL, NULL, 0),
(26, 'http://localhost/fixbetonkerites.hu/a2d8a29fb1ede7b581d71c452f579248/', '69:6806a4899b12834ee623eec8321e9064', 90, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-10-20 13:22:52', '2020-10-20 11:22:52', 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_indexable_hierarchy`
--

CREATE TABLE `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_migrations`
--

CREATE TABLE `wp_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_migrations`
--

INSERT INTO `wp_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_primary_term`
--

CREATE TABLE `wp_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `term_id` int(11) UNSIGNED NOT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`, `indexable_id`, `target_indexable_id`, `height`, `width`, `size`, `language`, `region`) VALUES
(25, '#', 5, 0, 'internal', 11, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_seo_meta`
--

CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(11, 0, 0),
(12, 0, 0),
(18, 0, 0),
(20, 0, 0),
(21, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(28, 0, 0),
(30, 0, 0),
(31, 0, 0),
(32, 0, 0),
(33, 0, 0),
(34, 0, 0),
(35, 0, 0),
(36, 0, 0),
(38, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(43, 0, 0),
(47, 0, 0),
(48, 0, 0),
(49, 0, 0),
(50, 0, 0),
(51, 0, 0),
(52, 0, 0),
(53, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(69, 0, 0),
(70, 0, 0);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- A tábla indexei `wp_db7_forms`
--
ALTER TABLE `wp_db7_forms`
  ADD PRIMARY KEY (`form_id`);

--
-- A tábla indexei `wp_email_log`
--
ALTER TABLE `wp_email_log`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- A tábla indexei `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `wp_user_id` (`wp_user_id`);

--
-- A tábla indexei `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `wp_newsletter_sent`
--
ALTER TABLE `wp_newsletter_sent`
  ADD PRIMARY KEY (`email_id`,`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `email_id` (`email_id`);

--
-- A tábla indexei `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id` (`email_id`),
  ADD KEY `user_id` (`user_id`);

--
-- A tábla indexei `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- A tábla indexei `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- A tábla indexei `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `path_hash` (`path_hash`),
  ADD KEY `image_size` (`image_size`);

--
-- A tábla indexei `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- A tábla indexei `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- A tábla indexei `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- A tábla indexei `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- A tábla indexei `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`),
  ADD KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  ADD KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  ADD KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`);

--
-- A tábla indexei `wp_yoast_indexable_hierarchy`
--
ALTER TABLE `wp_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- A tábla indexei `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wp_yoast_migrations_version` (`version`);

--
-- A tábla indexei `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- A tábla indexei `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`),
  ADD KEY `indexable_link_direction` (`indexable_id`,`type`);

--
-- A tábla indexei `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_db7_forms`
--
ALTER TABLE `wp_db7_forms`
  MODIFY `form_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_email_log`
--
ALTER TABLE `wp_email_log`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3024;

--
-- AUTO_INCREMENT a táblához `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT a táblához `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT a táblához `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT a táblához `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT a táblához `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT a táblához `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
